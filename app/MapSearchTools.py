import copy
import json
import os
import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import shortest_path
from skimage.draw import line
from .SemanticMap import SemanticMap


class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)

class MapSearchTools(object):

    def __init__(self):
        self.semanticMap = None
        self.basemap = {}
        self.graphs = {}
        self.movementGridAtTime = {}
        self.visibilityGridAtTime = {}
        self.pointIndicies = [[]]
        self.victims = {}
        self.rubble = {}
        self.xRange = [99999, -99999]
        self.zRange = [99999, -99999]
        self.minX = self.xRange[0]
        self.minZ = self.zRange[0]

    def initialize(self, basemap, rubble, victims):
        self.setBasemap(basemap)
        self.setRubbleAndVictims(rubble, victims)
        self.generateTimeGrids()

    def setSemanticMap(self, semanticMap):
        self.semanticMap = semanticMap

    def load(self, filename):
        if os.path.exists(filename):
            with open(filename) as f:
                info = json.load(f)
                self.walls = info['walls']
                self.victims = info['victims']
                self.rubble = info['rubble']
                self.xRange = info['xRange']
                self.zRange = info['zRange']

                self.minX = self.xRange[0]
                self.minZ = self.zRange[0]
                self.width = self.xRange[1] - self.xRange[0] + 1
                self.height = self.zRange[1] - self.zRange[0] + 1

                self.setRubbleAndVictims(info['rubble'], info['victims'])
                self.generateTimeGrids()

    def save(self, filename):
        info = {
            "walls": self.walls,
            "victims": self.victims,
            "rubble": self.rubble,
            "xRange": self.xRange,
            "zRange": self.zRange
        }

        f = open(filename, "w")
        f.write(json.dumps(info, separators=(',', ': '), cls=NpEncoder))
        f.close()

    # given a starting location (ps[x, y]) a victim id and a time determine if the victim can be triaged or not.
    def isVictimTriagable(self, ps, vid, time):
        info = {'vid': vid, 'triagable-from': []}
        # first get the victims location at the given time
        victim = self.getVictimInfo(vid, time)

        # Unknown Victim!!!
        if victim is None:
            return info

        info['triaged'] = victim['triaged']
        info['unlocked'] = victim['unlocked']
        info['is-critical'] = victim['is-critical']
        info['x'] = victim['x']
        info['z'] = victim['z']

        # Victim has already been triaged or is a locked critical victim
        if victim['triaged'] or not victim['unlocked']:
            return info

        # print("Victim Info:")
        # print(victim)

        vpt = [info['x'], info['z']]
        # Now determine the open blocks around the victim in a 5 block range
        blocks = self.getOpenLocations(vpt, 5, time)

        #for each open block, see if the block is reachable from the starting point
        for block in blocks:
            # Is the block reachable
            dist, path = self.shortestPath(block, ps, time)
            if dist == np.Infinity:
                continue

            # print('   Checking: ' + str(block))
            # Can the victim be seen from that block?
            if not self.isPartOfBlockVisibleFrom(block, vpt, time):
                # print("   - Can't see the victim from here!!!")
                continue

            # if yes, then add that block location to the triageLocations list.
            info['triagable-from'].append(block)
            # print("   - YES i can get here and I can see the victim from here!!")

        return info

    def world2Grid(self, pt):
        return [pt[0]-self.minX, pt[1]-self.minZ]

    def grid2World(self, pt):
        return [pt[0]+self.minX, pt[1]+self.minZ]

    def getTimeKeyAt(self, time):
        key = 0
        for t in self.changeTimes:
            if time < t:
                break
            key = t
        return key

    def getOpenLocations(self, pt, radius, time):
        blocks = []
        tk = self.getTimeKeyAt(time)
        ptG = self.world2Grid(pt)
        x1 = ptG[0] - radius if ptG[0] - radius >= 0 else 0
        z1 = ptG[1] - radius if ptG[1] - radius >= 0 else 0
        x2 = ptG[0] + radius if ptG[0] + radius < self.width else self.width-1
        z2 = ptG[1] + radius if ptG[1] + radius < self.height else self.height-1
        for x in range(x1, x2+1):
            for z in range(z1, z2+1):
                if self.movementGridAtTime[tk][x][z] == 0:
                    blocks.append(self.grid2World([x, z]))
        return blocks

    # compute line of sight blocks between the two points by scaling up and
    # computing the bresenham lines between the opposite corners of the blocks
    # then return the normal scale block which fall on the bresenham lines.
    def lineOfSightBlocks(self, pt1, pt2):
        # print ('      -  lineOfSightBetween: ' + str(pt1) + " and " + str(pt2))

        scale = 4
        width = abs((pt2[0]-pt1[0] + 1) * scale)
        height = abs((pt2[1]-pt1[1] + 1) * scale)

        ax1 = 0 if pt2[1] < pt1[1] else scale-1
        az1 = 0
        ax2 = width-scale if pt2[1] < pt1[1] else width-1
        az2 = -(height-scale) if pt2[1] < pt1[1] else height-scale 

        blockLines = []
        cc, rr = line(ax1, az1, ax2, az2)
        blockLine = []

        # print ('        - scaled between [' + str(ax1) + ', ' + str(az1) + '] and [' + str(ax2) + ', ' + str(az2) + ']')
        # print ('        -   ' + str(cc))
        # print ('        -   ' + str(rr))

        # Need to convert the returned blocks back to the correct scale and translate!!
        for i in range(len(rr)):
            block = [int(cc[i]/scale)+pt1[0], int(rr[i]/scale)+pt1[1]]
            if block not in blockLine:
                blockLine.append(block)
        blockLines.append(blockLine)

        bx1 = scale-1 if pt2[1] < pt1[1] else 0
        bz1 = scale-1
        bx2 = width-1 if pt2[1] < pt1[1] else width-scale
        bz2 = -(height-scale-1) if pt2[1] < pt1[1] else height-1

        cc, rr = line(bx1, bz1, bx2, bz2)
        blockLine = []

        # print ('        - scaled between [' + str(bx1) + ', ' + str(bz1) + '] and [' + str(bx2) + ', ' + str(bz2) + ']')
        # print ('        -   ' + str(cc))
        # print ('        -   ' + str(rr))

        # Need to convert the returned blocks back to the correct scale and translation!!
        for i in range(len(rr)):
            block = [int(cc[i]/scale)+pt1[0], int(rr[i]/scale)+pt1[1]]
            if block not in blockLine:
                blockLine.append(block)
        if pt1 not in blockLine:
            blockLine.append(pt1)
        if pt2 not in blockLine:
            blockLine.append(pt2)
        blockLines.append(blockLine)

        return blockLines

    def getBlockLinesBetween(self, pt1, pt2):
        # print("      - Looking for blocklines between: " + str(pt1) + " and " + str(pt2))
        blockLines = []
        if pt1[0] < pt2[0]:
            x1 = pt1[0]
            z1 = pt1[1]
            x2 = pt2[0]
            z2 = pt2[1]
        else:
            x1 = pt2[0]
            z1 = pt2[1]
            x2 = pt1[0]
            z2 = pt1[1]

        if x1 == x2:    # straight line
            blockLine = []
            if z1 <= z2:
                for z in range(z1, z2+1):
                    blockLine.append([x1,z2])
            else:
                for z in range(z2, z1+1):
                    blockLine.append([x1, z])
            blockLines.append(blockLine)
        elif z1 == z2:    # straight line
            blockLine = [[x1, z1]]
            for x in range(x1, x2+1):
                blockLine.append([x, z1])
            blockLine.append([x2,z2])
            blockLines.append(blockLine)
        else:
            # Not a straight line so need to compute each individually
            blockLines = self.lineOfSightBlocks([x1, z1], [x2, z2])

        return blockLines

    def isPartOfBlockVisibleFrom(self, pt1, pt2, time):
        # Just say no if the points are more that X blocks away
        if abs(pt1[0] - pt2[0]) > 15 or abs(pt1[1] - pt2[1]) > 15:
            return False

        # print("      Checking between " + str(pt1) + " and " + str(pt2))
        pt1G = self.world2Grid(pt1)
        pt2G = self.world2Grid(pt2)
        blockLines = self.getBlockLinesBetween(pt1G, pt2G)
        if len(blockLines) <= 0:
            # print("      - No Lines!!: " + str(len(blockLines)))
            return False
        # print("blockLines: " + str(blockLines))

        tk = self.getTimeKeyAt(time)
        visGrid = self.visibilityGridAtTime[tk]
        # print (visGrid)
        for blockLine in blockLines:
            if len(blockLine) <= 0:
                continue
            # print ("      - Checking: " + str(blockLine))
            blocked = False
            for pt in blockLine:
                if visGrid[pt[0]][pt[1]] > 0:
                    blocked = True
                    break
            if not blocked:
                return True

        return False

    def getVisibilityUnblockedLocations(self, pt1, pt2, time):
        blocks = []
        tk = self.getTimeKeyAt(time)
        pt1G = self.world2Grid(pt1)
        pt2G = self.world2Grid(pt2)

        x1 = pt1G[0] if pt1G[0] < pt2G[0] else pt2G[0]
        x2 = pt2G[0] if pt2G[0] > pt1G[0] else pt1G[0]
        z1 = pt1G[1] if pt1G[1] < pt2G[1] else pt2G[1]
        z2 = pt2G[1] if pt2G[1] > pt1G[1] else pt1G[1]
        for x in range(x1, x2+1):
            for z in range(z1, z2+1):
                if self.visibilityGridAtTime[tk][x][z] == 0:
                    blocks.append(self.grid2World([x, z]))
        return blocks

    def isVictimVisible(self, pt, yaw, vid, dist, time):
        # First get the victims location at the given time
        victim = self.getVictimInfo(vid, time)

        # Unknown Victim!!!
        if victim is None:
            return False

        vpt = [victim['x'], victim['z']]

        # TODO:: Is the yaw such that the player is facing this victim and 'could' see it?


        # Now get the list of locations within 10 blocks of the victim which do not have a block blocking the visiblity
        blocks = self.getVisibilityUnblockedLocations(pt, vpt, time)

        return False

    def getVictimInfo(self, vid, time):
        if vid in self.victims.keys():
            for info in self.victims[vid]:
                if info['start'] <= time < info['end']:
                    return info
        return None

    # Given two points p1[x,y] and p2[x,y] and a time, determine the shortest path in blocks from p1 to p2
    def shortestPath(self, p1, p2, time):
        # what is the shortest path between point p1 and point p2 at the given timestamp.
        dist = -1

        gridTime = 0
        for t in self.changeTimes:
            if time < t:
                break
            gridTime = t

        # use the grid at time 'gridTime' to compute the distance.
        ip1 = self.pointIndicies[p1[0]-self.minX][p1[1]-self.minZ]
        ip2 = self.pointIndicies[p2[0]-self.minX][p2[1]-self.minZ]
        if ip2 < ip1:
            tmp = ip2
            ip2 = ip1
            ip1 = tmp
        if gridTime not in self.graphs:
            self.computeDistMatrixAtTime(gridTime)

        dist_matrix, predecessors = shortest_path(csgraph=self.graphs[gridTime], directed=False, return_predecessors=True, indices=[ip1])
        # print (dist_matrix)
        # print (" dist from : " + str(ip1) + ' to ' + str(ip2) + ' is ' + str(dist))
        dist = dist_matrix[0][ip2]

        # now rebuild the path using the predicessors matrix
        # print(' - Building path from predecessors...')
        # print(self.predecessors[gridTime])
        i = ip2
        j = ip1
        # pathI = [i]
        path = [[int(i/self.height) + self.minX, i % self.height + self.minZ]]
        while predecessors[0][i] > 0 and i != j:
            i = predecessors[0][i]
            path.append([int(i/self.height) + self.minX, i % self.height + self.minZ])
            # pathI.append(i)
            # print(pathI)
        path.reverse()

        return dist, path

    def generateTimeGrids(self):
        # print (self.xRange)
        # print (self.zRange)
        # print ("size = " + str(self.width) + " x " + str(self.height))
        # print ("min = " + str(self.minX) + " x " + str(self.minZ))
        prevMovementGrid = [[0 for i in range(self.height)] for j in range(self.width)]
        prevVisibilityGrid = [[0 for i in range(self.height)] for j in range(self.width)]

        # use the semantic map to cut out areas which are not used.
        if self.semanticMap is not None:
            for x in range(self.width):
                for z in range(self.height):
                    if len(self.semanticMap.get_locations_containing(x+self.minX,z+self.minZ,False)) == 0 and \
                    len(self.semanticMap.get_connections_containing(x+self.minX,z+self.minZ,False)) == 0:
                        prevMovementGrid[x][z] = 3
                        prevVisibilityGrid[x][z] = 1

        for wall in self.walls:
            if wall[0] < self.xRange[0] or wall[0] > self.xRange[1] or \
               wall[1] < self.zRange[0] or wall[1] > self.zRange[1]:
               continue
            prevMovementGrid[wall[0]-self.minX][wall[1]-self.minZ] = 1          # cannot move here
            if wall[2] != 1:
                prevVisibilityGrid[wall[0]-self.minX][wall[1]-self.minZ] = 1    # cannot see here

        self.pointIndicies = [[0 for i in range(self.height)] for j in range(self.width)]
        cntr = 0
        for x in range(self.width):
            for z in range(self.height):
                self.pointIndicies[x][z] = cntr
                cntr = cntr + 1

        # print (self.pointIndicies)
        # print ('  - (1,2) = ' + str(self.pointIndicies[1][2]))

        self.movementGridAtTime = {}
        self.visibilityGridAtTime = {}
        self.changeTimes = list(self.blockages.keys())
        self.changeTimes.sort()
        # print (self.changeTimes)
        for time in self.changeTimes:
            # make a deep copy of the previous grids
            # print('Processing time: ' + str(time))
            # print('  - coping previous Grids...')
            self.movementGridAtTime[time] = copy.deepcopy(prevMovementGrid)
            self.visibilityGridAtTime[time] = copy.deepcopy(prevVisibilityGrid)
            # now update the grids with the blockage updates for this time
            # print('  - adding new blockages...')
            for blockage in self.blockages[time]:
                self.movementGridAtTime[time][blockage[0]-self.minX][blockage[1]-self.minZ] = 1 if blockage[2] != 0 else 0
                if blockage[2] > 1:
                    self.visibilityGridAtTime[time][blockage[0]-self.minX][blockage[1]-self.minZ] = 1    # cannot see here
                else:
                    self.visibilityGridAtTime[time][blockage[0]-self.minX][blockage[1]-self.minZ] = 0    # cannot see here

            # set the previous grids to point to this time's grids
            prevMovementGrid = self.movementGridAtTime[time]
            prevVisibilityGrid = self.visibilityGridAtTime[time]
            # print(prevMovementGrid)
            # print ('  - (3,2) = ' + str(prevMovementGrid[3][2]))

    def computeDistMatrixAtTime(self, time):
        if time not in self.changeTimes:
            return

        # now compute the shortest path graphs for the movement grid at this time
        # print('  - building the graph...')
        # graph = [[0 for i in range(self.width*self.height)] for j in range(self.width*self.height)]
        graph = np.zeros((self.width*self.height, self.width*self.height), dtype=np.uint8)
        # print('  - populating the graph...')
        for x in range(self.width):
            for z in range(self.height):
                pt1 = self.pointIndicies[x][z]
                # print('    checking [' + str(pt1) + '] (' + str(x) + ', ' + str(z) + ')')
                if self.movementGridAtTime[time][x][z] != 0:
                    # print('    - Skipping it!!')
                    continue
                a = False
                b = False
                if x+1 < self.width and self.movementGridAtTime[time][x+1][z] == 0:
                    # print('    - FOUND path to right : [' + str(self.pointIndicies[x+1][z]) + '] (' + str(x+1) + ', ' + str(z) + ')')
                    graph[pt1][self.pointIndicies[x+1][z]] = 100
                    # graph[self.pointIndicies[x+1][z]][pt1] = 100
                    a = True
                # else:
                #     print('    - no path to right : [' + str(self.pointIndicies[x+1][z]) + '] (' + str(x+1) + ', ' + str(z) + ')')

                if z+1 >= self.height:
                    # print ('    - last row so not checking down.')
                    continue
                if self.movementGridAtTime[time][x][z+1] == 0:
                    # print('    - FOUND path down to : [' + str(self.pointIndicies[x][z+1]) + '] (' + str(x) + ', ' + str(z+1) + ')')
                    graph[pt1][self.pointIndicies[x][z+1]] = 100
                    # graph[self.pointIndicies[x][z+1]][pt1] = 100
                    b = True
                # else:
                #     print('    - no path down to : [' + str(self.pointIndicies[x][z+1]) + '] (' + str(x) + ', ' + str(z+1) + ')')
                if (a or b) and self.movementGridAtTime[time][x+1][z+1] == 0:
                    graph[pt1][self.pointIndicies[x+1][z+1]] = 130
                    # graph[self.pointIndicies[x+1][z+1]][pt1] = 130

        # print(graph)

        # print('  - Compressing the graph...')
        graph = csr_matrix(graph)
        self.graphs[time] = graph
        # print(graph)

    def updateMinMaxRange(self, x, z):
        if self.xRange[0] > x:
            self.xRange[0] = x
        elif self.xRange[1] < x:
            self.xRange[1] = x

        if self.zRange[0] > z:
            self.zRange[0] = z
        elif self.zRange[1] < z:
            self.zRange[1] = z

        self.minX = self.xRange[0]
        self.minZ = self.zRange[0]
        self.width = self.xRange[1] - self.xRange[0] + 1
        self.height = self.zRange[1] - self.zRange[0] + 1

    def setBasemap(self, basemap):
        self.rubble = {}
        self.victims = {}
        self.xRange = [99999, -99999]
        self.zRange = [99999, -99999]

        self.walls = []
        walls = {}
        for data in basemap['data']:
            if data[1] in ['wall_sign', 'wall_banner', 'lever', 'water', 'flower_pot', 'barrier', \
                            'tripwire_hook', 'redstone_torch', 'ladder', 'cake', 'brewing_stand'] or \
               data[1].endswith('_button') or data[1].endswith('_door') or \
               data[0][1] == 62:
                continue

            x = data[0][0]
            z = data[0][2]
            y = data[0][1]
            key = str(x) + ',' + str(z)
            if key not in walls.keys():
                walls[key] = [x, z, 0]
                self.updateMinMaxRange(x, z)
            walls[key][2] += y - 59        # 1 = block at height 1, 2 = block at height 2, 3 = block at height 1 and 2

        self.walls = list(walls.values())

    def setRubbleAndVictims(self, rubble, victims):
        self.blockages = {0:[]}
        self.rubble = rubble
        self.victims = victims

        for rl in self.rubble.values():
            for rb in rl:
                x = rb['x']
                z = rb['z']
                if rb['start'] not in self.blockages.keys():
                    self.blockages[rb['start']] = []
                self.blockages[rb['start']].append([x, z, rb['total']])
            if rl[-1]['end'] != 9999999:    # rubble was completely cleared so need to mark this
                if rl[-1]['end'] not in self.blockages.keys():
                    self.blockages[rl[-1]['end']] = []
                self.blockages[rl[-1]['end']].append([x, z, 0])

        for vic in self.victims.values():
            for vevt in vic:
                x = vevt['x']
                z = vevt['z']
                if vevt['start'] not in self.blockages.keys():
                    self.blockages[vevt['start']] = []
                self.blockages[vevt['start']].append([x, z, 1])
                if vevt['end'] != 9999999:
                    if vevt['end'] not in self.blockages.keys():
                        self.blockages[vevt['end']] = []
                    self.blockages[vevt['end']].append([x, z, 0])




            

            



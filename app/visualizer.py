from flask import Flask, render_template, request, make_response, redirect
from app.timeline.css import generateDefaultTimelineCSS
from werkzeug.utils import secure_filename
from app.ASISTDataTools import ASISTDataTools
from app.MapSearchTools import MapSearchTools
from app.functions import write_json
from app import TimelineHelper
from . import socketio
from . import app

import json
import os

# print("app/visualizer.py starting ...")

staticFolder = 'static'
staticUrl = 'static'
mapsFolder = 'maps'
trialsHT = {}

if not os.path.exists(staticFolder):
    staticFolder = 'app/static'
    if not os.path.exists(staticFolder):
        print(
            'Unable to locate an existing static folder so assuming it will be in the CWD.')
        staticFolder = 'static'

if not os.path.exists(mapsFolder):
    mapsFolder = 'app/maps'
    if not os.path.exists(mapsFolder):
        print('Unable to locate an existing maps folder so assuming it will be in the CWD.')
        mapsFolder = 'maps'

dtools = ASISTDataTools(maps_folder=mapsFolder,
                        data_folder=os.path.join(staticFolder, 'videos'))
app.config['SECRET_KEY'] = 'a1d3952a87031056225c44465eaabef2ea47ef9aa46a1c19'


@app.route("/")
def visualizer():
    return render_template("visualizer.html")


@app.route("/review", methods=['GET'])
def general_review():
    message = ""
    error = ""

    if request.args.get('message'):
        message = request.args.get('message')

    if request.args.get('error'):
        error = request.args.get('error')

    return render_template(
        "review.html",
        message=message,
        error=error,
        count=[[index, interval]
               for index, interval in enumerate(range(2, 17, 3))]
    )


@app.route('/post_general_review', methods=['POST'])
def process_general_review():
    if request.method == 'POST':
        if request.form['trial_name'] != "":
            file = request.form['trial_name']
            items = 5
            survey_data = {
                "scores": [int(request.form[f'input-{i}']) for i in range(items)],
                "overall": int(request.form[f'input-{items}']),
                "name": request.form['name']
            }

            save_path = os.path.join(
                staticFolder, 'videos', secure_filename(file))

            file_name = f'{save_path}.general_review'

            write_json(survey_data, file_name)
            return redirect("review?message=Thank you for submitting your feedback.")
        else:
            return redirect("review?error=Please select a game file before giving feedback.")


@app.route('/upload', methods=['POST'])
def upload():
    # Route to deal with the uploaded chunks
    print(request.form)
    print(request.files)

    # Remember the paramName was set to 'file', we can use that here to grab it
    file = request.files['file']

    # secure_filename makes sure the filename isn't unsafe to save
    save_path = os.path.join(staticFolder, 'videos',
                             secure_filename(file.filename))

    open_format = 'ab' if int(request.form['dzchunkbyteoffset']) != 0 else 'wb'

    # We need to append to the file, and write as bytes
    with open(save_path, open_format) as f:
        # Goto the offset, aka after the chunks we already wrote
        f.seek(int(request.form['dzchunkbyteoffset']))
        f.write(file.stream.read())

    # Giving it a 200 means it knows everything is ok
    return make_response(('Uploaded Chunk', 200))


@socketio.on('connect', namespace='/ihmc_replay_visualizer')
def sio_connect():
    print('Client connected')


@socketio.on('get_trial_list', namespace='/ihmc_replay_visualizer')
def sio_get_trial_list(msg, namespace):
    print('Getting Trial List...')
    trials = dtools.getListOfTrials()
    for trial in trials:
        trialsHT[trial['name']] = trial

    trial_list = []
    for trial in trialsHT:
        trial_list.append(trial)
    msg = {'trial_list': trial_list}
    print("Returning the list of trials:")
    print(msg)
    socketio.emit('trial_list', msg, namespace='/ihmc_replay_visualizer')


@socketio.on('delete_trial_data', namespace='/ihmc_replay_visualizer')
def sio_delete_trial_data(msg, namespace):
    if msg['trial'] in trialsHT.keys():
        trial = trialsHT[msg['trial']]
        del trialsHT[msg['trial']]

        if os.path.exists(trial['metadata-filename']):
            os.remove(trial['metadata-filename'])

        if os.path.exists(trial['video-filename']):
            os.remove(trial['video-filename'])

    sio_get_trial_list({}, '/ihmc_replay_visualizer')


@socketio.on('get_trial_data', namespace='/ihmc_replay_visualizer')
def sio_get_trial_data(msg, namespace):
    trial = trialsHT[msg['trial']]
    regenerate = msg['regenerate'] if 'regenerate' in msg.keys() else False
    print("Loading Trial Data for: " + msg['trial'])
    width, height, xtrans, ztrans, svg, timings, timelineData, expVersion, trialLengthInMs = dtools.loadTrial(
        trial, regenerateData=regenerate)

    video_url = trial['video-filename'] if 'video-filename' in trial.keys() else staticUrl + \
        '/videos/' + \
        msg['trial'].replace('_TrialMessages_', '_OBVideo_') + '.mp4'

    data = {
        'name': trial['info']['experiment_mission'] if 'info' in trial.keys() and trial['info'] is not None and 'experiment_mission' in trial['info'].keys() else trial['trial-name'] + ' ' + trial['map'],
        'svg': svg,
        'width': width,
        'height': height,
        'xtrans': xtrans,
        'ztrans': ztrans,
        'expVersion': expVersion,
        'trialLengthInMs': trialLengthInMs,
        'timings': json.loads(json.dumps(timings)),
        'timeline': json.loads(json.dumps(timelineData)),
        'video_url': video_url
    }
    if not os.path.exists(data['video_url']):
        data['video_url'] = staticUrl + '/videos/30minutes.mp4'

    if data['video_url'].startswith("app/"):
        data['video_url'] = data['video_url'][4:]

    print("  video_url: " + data['video_url'])
    socketio.emit('trial_data', data,
                  namespace='/ihmc_replay_visualizer', room=request.sid)


@socketio.on('update_annotations', namespace='/ihmc_replay_visualizer')
def sio_update_annotations(msg, namespace):
    trial = trialsHT[msg['trial']]
    print("Saving Annotations for: " + msg['trial'])
    annotations = msg['annotations']
    updatedGroups = msg['updated']
    dtools.saveAnnotations(trial, annotations, updatedGroups)
    # print(json.dumps(msg))


@socketio.on('get_timeline_types', namespace='/ihmc_replay_visualizer')
def sio_get_timeline_types(msg, namespace):
    timelineTypes = None
    typesfile = os.path.join(staticFolder, 'TimelineCSS.json')
    if os.path.exists(typesfile):
        # load the timings
        with open(typesfile) as f:
            timelineTypes = json.load(f)
    defaultTypes = generateDefaultTimelineCSS()
    if timelineTypes is None:
        timelineTypes = defaultTypes
    else:
        timelineTypes['defaults'] = defaultTypes['defaults']
    print('Returning Timeline Types...')
    print(timelineTypes)
    socketio.emit('timeline_types', timelineTypes,
                  namespace='/ihmc_replay_visualizer')


@socketio.on('save_timeline_types', namespace='/ihmc_replay_visualizer')
def sio_save_timeline_types(msg, namespace):
    print('Saving New Timeline Types...')
    print(msg)
    if 'defaults' in msg.keys() and 'added' in msg.keys():
        typesfile = os.path.join(staticFolder, 'TimelineCSS.json')
        f = open(typesfile, "w")
        f.write(json.dumps(msg))
        f.close()

        # Now tell everyone about the changes...
        socketio.emit('timeline_types', msg,
                      namespace='/ihmc_replay_visualizer', broadcast=True)

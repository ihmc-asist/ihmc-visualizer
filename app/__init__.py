import flask
from flask_socketio import SocketIO

# print("app/__init__.py starting...")
app = flask.Flask(__name__)
socketio = SocketIO(app, async_mode=None, logger=False, engineio_logger=False)

# define for IIS module registration.
wsgi_app = app.wsgi_app
debug = app.debug

from app import visualizer
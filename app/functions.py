import json

def write_json(data, filename):
    with open(filename,'a') as file:
        file.write(json.dumps(data)+"\n")
from app.timeline.css import generateDefaultTimelineCSS
from .JAGHelper import JointActivityHelper

import json

sg1_order = 100
sg0_order = 200
sgc_order = 300
sg2_order = 0

def getPlayerColorFromCallsign(callsign):
    if callsign is not None:
        if callsign == 'Red' or callsign == 'Alpha':
            return 'rgba(255, 0, 0, 0.6)'
        if callsign == 'Green' or callsign == 'Bravo':
            return 'rgba(0, 255, 0, 0.6)'
        if callsign == 'Blue' or callsign == 'Delta':
            return 'rgba(0, 0, 255, 0.6)'
    return 'rgba(20, 20, 20, 0.6)'

def generateTimelineData(players, timeline, basemap, rubble, victims, expType, maxEndTime, jah, jagData, planning_stop_time, markers, bombs):
    """Generates timeline data that is used on the web visualizer."""

    # All timeline information is stored in this variable.
    timelineData = {}

    # Individual timeline items are stored in timelineData["Items"] and export to .timeline file.   
    timelineData["Items"] = []

    # Specific timeline items CSS is generated.
    timelineData["CSS"] = generateDefaultTimelineCSS()

    timelineData["Groups"] = [
        {
            "id": 0,
            "order": 99,
            "content": "Score",
            "subgroupStack": {"sg1": True, "sg0": False},
        }
    ]
    # timelineData['Items'].append({'start': -20, 'end': 1200000, 'group': 0, 'subgroup': 'sg1', 'type': 'background', 'style': 'background-color: rgba(255,255,255,0.0); height: 90px;'})

    endTime = 0
    if timeline["endTime"] > 0:
        endTime = timeline['endTime']
        style = (
            "color: black; background-color: gray; padding-top: 0px; font-size: 10px;"
        )
        timelineData["Items"].append(
            {
                "start": timeline["endTime"],
                "end": timeline["endTime"] + 60000,
                "content": "End",
                "style": style,
                "type": "background",
                "group": 0,
            }
        )

    #timeline['stageTransition'].append({'st': ts, 'stage': currentStage})
    lastTime = 0
    lastStage = 'SHOP_STAGE'
    store_style = (
        "color: black; background-color: rgba(200, 200, 200, 0.3); padding-top: 0px; font-size: 10px;"
    )
    recon_style = (
        "color: black; background-color: rgba(39, 245, 119, 0.3); padding-top: 0px; font-size: 10px;"
    )
    stageItems = []
    for stage in timeline["stageTransition"]:
        if stage['stage'] == 'FIELD_STAGE':
            continue;
        elif stage['stage'] == 'RECON_STAGE':
            timelineData["Items"].append(
                {
                    "start": stage['st'],
                    "end": stage['et'],
                    "content": stage['stage'],
                    "style": recon_style,
                    "type": "background",
                    "group": 0,
                })
            stageItems.append(
                {
                    "start": stage['st'],
                    "end": stage['et'],
                    "style": recon_style,
                    "type": "background",
                    "group": 0,
                })
        elif stage['stage'] == 'SHOP_STAGE':
            timelineData["Items"].append(
                {
                    "start": stage['st'],
                    "end": stage['et'],
                    "content": stage['stage'],
                    "style": store_style,
                    "type": "background",
                    "group": 0,
                })
            stageItems.append(
                {
                    "start": stage['st'],
                    "end": stage['et'],
                    "style": store_style,
                    "type": "background",
                    "group": 0,
                })

    for item in timeline["unlocked"]:
        timelineData["Items"].append(
            {
                "start": item["st"],
                "title": "Unlocked cv-" + str(item["vid"]),
                "group": 0,
                "type": "point",
                "editable": False,
                "className": "def_unlocked-victim",
                "subgroup": "sg0",
                "subgroupOrder": sg0_order,
            }
        )

    for item in timeline["score"]:
        timelineData["Items"].append(
            {
                "start": item["st"],
                "content": str(item["score"]),
                "group": 0,
                "type": "point",
                "editable": False,
                "className": "def_score",
                "subgroup": "sg0",
                "subgroupOrder": sg0_order,
            }
        )

    if "perturbation" in timeline:
        style = (
            "color: black; background-color: pink; padding-top: 0px; font-size: 10px;"
        )
        for item in timeline["perturbation"]:
            if item["type"] == "blackout":
                timelineData["Items"].append(
                    {
                        "start": item["start"],
                        "end": item["end"],
                        "content": "Comms Black Out",
                        "style": style,
                        "type": "background",
                        "group": 0,
                    }
                )
            elif item["type"] == "rubble":
                timelineData["Items"].append(
                    {
                        "start": item["start"],
                        "end": item["end"],
                        "content": "Rubble",
                        "style": style,
                        "type": "background",
                        "group": 0,
                    }
                )

    # process the merged jag messages:
    # team = jah.get_player_by_callsign('black')
    # if team is not None:
    #     jags = team.joint_activity_model.jag_instances

    #     for jag in jags:
    #         print("Handling a JAG: " + jag.short_string())
    #         processJag(jag, players)
    # else:
    #     print("**** No Team JAGS!!!!! ****")

    tansparency = 0.8
    max = 255.0  # was 200.0
    min = 150.0  # was 100.0
    colorRange = max - min

    groups = {"Red": 1, "Green": 2, "Blue": 3}
    if expType == 4:
        groups = {"Alpha": 1, "Bravo": 2, "Delta": 3}

    marker_title_map = {
        "novictim": "Marker Block: No Victim",
        "regularvictim": "Marker Block: Regular Victim",
        "criticalvictim": "Marker Block: Critical Victim/Type C",
        "abrasion": "Marker Block: Victim Type A (Abrasion)",
        "bonedamage": "Marker Block: Victim Type B (Bone Damage)",
        "threat": "Marker Block: Threat",
        "sos": "Marker Block: SOS",
        "rubble": "Marker Block: Rubble",
    }

    bombJags = {}
    if expType == 4:
        for jagId in jagData.keys():
            for jag in jagData[jagId]:
                bombId = jag['jag']['inputs']['bomb_id']
                if bombId not in bombJags:
                    bombJags[bombId] = {'bombId': bombId, 'participants': []}

                if jag['jag']['urn'] == 'urn:asist:defuse-bomb':
                    if jag['status'] == 'start':
                        bombJags[bombId]['start'] = jag['elapsed_milliseconds']
                    if jag['status'] == 'success':
                        bombJags[bombId]['success'] = jag['elapsed_milliseconds']
                    if jag['status'] == 'failure':
                        bombJags[bombId]['failure'] = jag['elapsed_milliseconds']
                    bombJags[bombId]['sequence'] = jag['jag']['inputs']['sequence']
                    
                if jag['jag']['urn'] == 'urn:asist:cut-wire':
                    if jag['status'] not in ['start']:
                        if jag['participant']['id'] not in bombJags[bombId]['participants']:
                            bombJags[bombId]['participants'].append(jag['participant']['id'])
                        bombJags[bombId][jag['jag']['inputs']['wire']] = {'status': jag['status'], 'pid': jag['participant']['id'], 'wire': jag['jag']['inputs']['wire'], 'ts': jag['elapsed_milliseconds']}
                    # bombJags[bombId][jag['elapsed_milliseconds']] = {'status': jag['status'], 'pid': jag['participant']['id'], 'wire': jag['jag']['inputs']['wire']}
                    if 'start' not in bombJags[bombId] or jag['elapsed_milliseconds'] < bombJags[bombId]['start']:
                        bombJags[bombId]['start'] = jag['elapsed_milliseconds']

    # for bomb in bombJags:
    #     print(bombJags[bomb])

    for key in players.keys():
        player = players[key]
        if player['callsign'] == 'server':
            if expType != 4:
                continue
            #  Show timeline Info about bombs which exploded because of their fuse going off automatically and the players not handling them.

        if expType == 4 and player['callsign'] != 'server':
            timelineData["Groups"].append(
                {
                    "id": player["id"],
                    "order": player["id"],
                    "content": player["callsign"],
                    "title": "Id: " + player["participant_id"],
                    "style": "color: white; background-color: " + getPlayerColorFromCallsign(player["callsign"]) + ";",
                    # "subgroupStack": {"sg3": True, "sg1": True, "sg0": False, "sgc": False},
                    "subgroupStack": {"sg3": True, "sg1": False, "sg0": False, "sgc": False},
                    "subgroupOrder": "subgroupOrder",
                    "callsign": player["callsign"],
                    "markerblocklegend": None,
                    "staticmapversion": None,
                }
            )

            # show the stages on all group timelines.
            for item in stageItems:
                newItem = item.copy()
                newItem['group'] = player['id']
                timelineData["Items"].append(newItem)

            groups[player["callsign"]] = player["id"]
            print("Processing Jags for: " + player['participant_id'])
            jagItems = processJagExp4(bombJags, player["callsign"], player['participant_id'], player["id"])
            if len(jagItems) > 0:
                jagGroupIndex = 2
                subGrp = "sg" + str(jagGroupIndex)
                timelineData["Groups"][-1]["subgroupStack"][subGrp] = False
                for item in jagItems:
                    if item["end"] > endTime:
                        item["end"] = endTime
                    item["subgroup"] = subGrp
                    item["subgroupOrder"] = jagGroupIndex
                    timelineData["Items"].append(item)


        if expType == 3:
            timelineData["Groups"].append(
                {
                    "id": player["id"],
                    "order": player["id"],
                    "content": player["callsign"],
                    "title": "Id: " + player["participant_id"],
                    "style": "color: white; background-color: "
                    + player["callsign"]
                    + ";",
                    "subgroupStack": {"sg3": True, "sg1": True, "sg0": False, "sgc": False},
                    "subgroupOrder": "subgroupOrder",
                    "callsign": player["callsign"],
                    "markerblocklegend": None,
                    "staticmapversion": None,
                }
            )

            groups[player["callsign"]] = player["id"]

            # timelineData['Items'].append({'start': 0, 'end': 1200000, 'group': player['id'], 'subgroup': 'sg2', 'type': 'background', 'style': 'background-color: rgba(255, 255, 255, 0.0); z-index:0'})
            # timelineData['Items'].append({'start': 1, 'end': 1200000, 'group': player['id'], 'subgroup': 'sg2.1', 'type': 'background', 'style': 'background-color: rgba(255, 255, 255, 0.0); z-index:0'})
            # timelineData['Items'].append({'start': 2, 'end': 1200000, 'group': player['id'], 'subgroup': 'sg2.1', 'editable': False, 'style': 'background-color: rgba(255, 255, 255, 0.0); z-index:0, height:5px'})
            # timelineData['Items'].append({'start': 3, 'end': 1200000, 'group': player['id'], 'subgroup': 'sg1', 'type': 'background', 'style': 'background-color: rgba(255, 255, 255, 0.0); z-index:0'})
            # timelineData['Items'].append({'start': 4, 'end': 1200000, 'group': player['id'], 'subgroup': 'sg0', 'type': 'background', 'style': 'background-color: rgba(255, 255, 255, 0.0); z-index:0'})
            # timelineData['Items'].append({'start': 5, 'end': 1200000, 'group': player['id'], 'subgroup': 'sgc', 'type': 'background', 'style': 'background-color: rgba(255, 255, 255, 0.0); z-index:0'})

            # handle jag messages
            jagP = jah.get_player_by_callsign(player["callsign"])
            jagGroups = []
            if jagP is not None:
                jags = jagP.joint_activity_model.get_known_victims()
                if jags is not None:
                    for jag in jags:
                        if jag.is_complete():
                            print("COMPLETED JAG!!")
                        print(
                            player["callsign"]
                            + " - Handling a JAG: "
                            + jag.short_string()
                        )
                        processJag(
                            jag,
                            jagGroups,
                            player["callsign"].lower(),
                            player["id"],
                            planning_stop_time,
                        )

            # For each jag Group, create a subgroup and then add all the jags in each jag Group to it's subgroup on the timeline.
            # print("*** There are " + str(len(jagGroups)) + " Jag Groups")
            jagGroupIndex = 2
            for jg in jagGroups:
                subGrp = "sg" + str(jagGroupIndex)
                timelineData["Groups"][-1]["subgroupStack"][subGrp] = False
                for ja in jg:
                    for item in ja["items"]:
                        item["subgroup"] = subGrp
                        item["subgroupOrder"] = jagGroupIndex
                        timelineData["Items"].append(item)
                for ja in jg:
                    if ja["main"] is not None:
                        ja["main"]["subgroup"] = subGrp
                        ja["main"]["subgroupOrder"] = jagGroupIndex
                        if ja["main"]["start"] == ja["main"]["end"]:
                            ja["main"]["end"] += 100
                        timelineData["Items"].append(ja["main"])

                jagGroupIndex += 1

            print(timelineData["Groups"][-1]["subgroupStack"])

            # Hard coding the planning stage background to be 2:03 long.
            toolLevelColor = "rgba(120, 120, 120, " + str(tansparency) + ")"
            role_name = "UNKNOWN"
            if len(player["role_changes"]) > 0:
                rc = player["role_changes"][-1]
                rc["level"] = 100
                role_name = rc["role"]
                if rc["role"].lower() == "transporter":
                    percentColor = str(
                        round(colorRange * (100.0 - rc["level"]) / 100.0 + min)
                    )
                    # toolLevelColor = 'rgba(255, ' + percentColor + ', ' + percentColor + ', ' + str(tansparency) + ')'
                    toolLevelColor = (
                        "rgba(255, "
                        + percentColor
                        + ", "
                        + "0"
                        + ", "
                        + str(tansparency)
                        + ")"
                    )
                elif rc["role"].lower() == "medic":
                    percentColor = round(
                        colorRange * (100.0 - rc["level"]) / 100.0 + min
                    )
                    # toolLevelColor = 'rgba(' + percentColor + ', 255, ' + percentColor + ', ' + str(tansparency) + ')'
                    # toolLevelColor = 'rgba(' + '0' + ', 255, ' + percentColor + ', ' + str(tansparency) + ')'
                    toolLevelColor = (
                        "rgba("
                        + str(percentColor)
                        + ", "
                        + str(round(percentColor / 2))
                        + ", 255, "
                        + str(tansparency)
                        + ")"
                    )
                elif rc["role"].lower() == "rubbler":
                    percentColor = str(
                        round(colorRange * (100.0 - rc["level"]) / 100.0 + min - 25)
                    )
                    # toolLevelColor = 'rgba(' + percentColor + ', ' + percentColor + ', 255, ' + str(tansparency) + ')'
                    toolLevelColor = (
                        "rgba("
                        + percentColor
                        + ", "
                        + percentColor
                        + ", "
                        + percentColor
                        + ", "
                        + str(tansparency)
                        + ")"
                    )

            toolLevelColor = "background-color: " + toolLevelColor + ";"
            role_time = planning_stop_time if planning_stop_time > 60000 else 60000
            timelineData["Items"].append(
                {
                    "start": -10,
                    "end": role_time,
                    "group": player["id"],
                    "subgroup": "sg0",
                    "subgroupOrder": sg0_order,
                    "content": role_name,
                    "type": "background",
                    "style": toolLevelColor,
                }
            )
        elif expType == 2:
            mbLegend = (
                player["markerblocklegend"]
                if "markerblocklegend" in player.keys()
                else "None"
            )
            if len(mbLegend) <= 0:
                mbLegend = "None"
            smv = (
                player["staticmapversion"]
                if "staticmapversion" in player.keys()
                else "Saturn None"
            )
            if len(smv) <= 0:
                smv = "Saturn None"
            timelineData["Groups"].append(
                {
                    "id": player["id"],
                    "order": player["id"],
                    "content": player["callsign"]
                    + "<br>["
                    + mbLegend[0]
                    + "] / "
                    + smv.replace("Saturn", ""),
                    "title": "Id: "
                    + player["participant_id"]
                    + " Markers: "
                    + mbLegend
                    + " Map: "
                    + smv,
                    "style": "color: white; background-color: "
                    + player["callsign"]
                    + ";",
                    "subgroupStack": {"sg1": True, "sg0": False, "sgc": False},
                    "callsign": player["callsign"],
                    "markerblocklegend": mbLegend,
                    "staticmapversion": smv,
                }
            )

            timelineData["Items"].append(
                {
                    "start": 0,
                    "end": 1200000,
                    "group": player["id"],
                    "subgroup": "sg1",
                    "subgroupOrder": sg1_order,
                    "type": "background",
                    "style": "background-color: rgba(255, 255, 255, 0.0); z-index:0",
                }
            )
            timelineData["Items"].append(
                {
                    "start": 0,
                    "end": 1200000,
                    "group": player["id"],
                    "subgroup": "sg0",
                    "subgroupOrder": sg0_order,
                    "type": "background",
                    "style": "background-color: rgba(255, 255, 255, 0.0); z-index:0",
                }
            )
            timelineData["Items"].append(
                {
                    "start": 0,
                    "end": 1200000,
                    "group": player["id"],
                    "subgroup": "sgc",
                    "subgroupOrder": sgc_order,
                    "type": "background",
                    "style": "background-color: rgba(255, 255, 255, 0.0); z-index:0",
                }
            )

            for rc in player["role_changes"]:
                toolLevelColor = "rgba(255, 255, 255, " + str(tansparency) + ")"
                if rc["level"] > 0.0:

                    if rc["role"].lower() == "searcher":
                        percentColor = str(
                            round(colorRange * (100.0 - rc["level"]) / 100.0 + min)
                        )
                        # toolLevelColor = 'rgba(255, ' + percentColor + ', ' + percentColor + ', ' + str(tansparency) + ')'
                        toolLevelColor = (
                            "rgba(255, "
                            + percentColor
                            + ", "
                            + "0"
                            + ", "
                            + str(tansparency)
                            + ")"
                        )
                    elif rc["role"].lower() == "medic":
                        percentColor = round(
                            colorRange * (100.0 - rc["level"]) / 100.0 + min
                        )
                        # toolLevelColor = 'rgba(' + percentColor + ', 255, ' + percentColor + ', ' + str(tansparency) + ')'
                        # toolLevelColor = 'rgba(' + '0' + ', 255, ' + percentColor + ', ' + str(tansparency) + ')'
                        toolLevelColor = (
                            "rgba("
                            + str(percentColor)
                            + ", "
                            + str(round(percentColor / 2))
                            + ", 255, "
                            + str(tansparency)
                            + ")"
                        )
                    elif rc["role"].lower() == "rubbler":
                        percentColor = str(
                            round(colorRange * (100.0 - rc["level"]) / 100.0 + min - 25)
                        )
                        # toolLevelColor = 'rgba(' + percentColor + ', ' + percentColor + ', 255, ' + str(tansparency) + ')'
                        toolLevelColor = (
                            "rgba("
                            + percentColor
                            + ", "
                            + percentColor
                            + ", "
                            + percentColor
                            + ", "
                            + str(tansparency)
                            + ")"
                        )
                    # percentColor = str(round(255.0 * (100.0 - rc['level'])/100.0))
                    # toolLevelColor = 'rgba(255, 255, 255, 0.5)'
                    # if rc['role'].lower() == 'searcher':
                    #     toolLevelColor = 'rgba(255, ' + percentColor + ', ' + percentColor + ', 0.5)'
                    # elif rc['role'].lower() == 'medic':
                    #     toolLevelColor = 'rgba(' + percentColor + ', 255, ' + percentColor + ', 0.5)'
                    # elif rc['role'].lower() == 'rubbler':
                    #     toolLevelColor = 'rgba(' + percentColor + ', ' + percentColor + ', 255, 0.5)'

                toolLevelColor = "background-color: " + toolLevelColor + ";"
                if (
                    len(timelineData["Items"]) > 0
                    and timelineData["Items"][-1]["group"] == player["id"]
                    and timelineData["Items"][-1]["type"] == "background"
                    and timelineData["Items"][-1]["style"] == toolLevelColor
                ):
                    timelineData["Items"][-1]["end"] = rc["et"]
                else:
                    timelineData["Items"].append(
                        {
                            "start": rc["st"],
                            "end": rc["et"],
                            "group": player["id"],
                            "subgroup": "sg0",
                            "type": "background",
                            "style": toolLevelColor,
                        }
                    )

        for tg in player["triaging"]:
            if tg["triaged"]:
                timelineData["Items"].append(
                    {
                        "start": tg["st"],
                        "end": tg["et"],
                        "title": "Triaging "
                        + ("cv-" if tg["is-critical"] else "v-")
                        + str(tg["vid"]),
                        "group": player["id"],
                        "subgroup": "sg0",
                        "subgroupOrder": sg0_order,
                        "editable": False,
                        "className": "def_triaging-success",
                    }
                )
                timelineData["Items"].append(
                    {
                        "start": tg["et"],
                        "end": tg["et"],
                        "title": "Triaged "
                        + ("cv-" if tg["is-critical"] else "v-")
                        + str(tg["vid"]),
                        "group": player["id"],
                        "subgroup": "sg0",
                        "subgroupOrder": sg0_order,
                        "type": "box",
                        "editable": False,
                        "className": (
                            "def_triaged-critical-victim"
                            if tg["is-critical"]
                            else "def_triaged-victim"
                        ),
                    }
                )
            else:
                timelineData["Items"].append(
                    {
                        "start": tg["st"],
                        "end": tg["et"],
                        "title": "Triaging "
                        + ("cv-" if tg["is-critical"] else "v-")
                        + str(tg["vid"])
                        + " (will fail)",
                        "group": player["id"],
                        "subgroup": "sg0",
                        "subgroupOrder": sg0_order,
                        "editable": False,
                        "className": "def_triaging-fail",
                    }
                )

        for fz in player["frozen"]:
            timelineData["Items"].append(
                {
                    "start": fz["st"],
                    "end": fz["et"],
                    "title": "Frozen",
                    "group": player["id"],
                    "subgroup": "sg0",
                    "subgroupOrder": sg0_order,
                    "editable": False,
                    "className": "def_frozen",
                }
            )

        for ca in player["carrying"]:
            if ca["evac_state"] == "SUCCESSFUL":
                if ca["is-correcting"]:
                    timelineData["Items"].append(
                        {
                            "start": ca["st"],
                            "end": ca["et"],
                            "title": "Carrying "
                            + ("cv-" if ca["is-critical"] else "v-")
                            + str(ca["vid"])
                            + " to destination (correcting)",
                            "group": player["id"],
                            "subgroup": "sg0",
                            "subgroupOrder": sg0_order,
                            "editable": False,
                            "className": "def_carrying-evacuated-corrected",
                        }
                    )
                else:
                    timelineData["Items"].append(
                        {
                            "start": ca["st"],
                            "end": ca["et"],
                            "title": "Carrying "
                            + ("cv-" if ca["is-critical"] else "v-")
                            + str(ca["vid"])
                            + " to destination",
                            "group": player["id"],
                            "subgroup": "sg0",
                            "subgroupOrder": sg0_order,
                            "editable": False,
                            "className": "def_carrying-evacuated-successful",
                        }
                    )
            elif ca["evac_state"] == "UNSUCCESSFUL":
                timelineData["Items"].append(
                    {
                        "start": ca["st"],
                        "end": ca["et"],
                        "title": "Carrying "
                        + ("cv-" if ca["is-critical"] else "v-")
                        + str(ca["vid"] + " to wrong destination"),
                        "group": player["id"],
                        "subgroup": "sg0",
                        "subgroupOrder": sg0_order,
                        "editable": False,
                        "className": "def_carrying-evacuated-unsuccessful",
                    }
                )
            else:  # ca['evac_state'] =='IN_PROGRESS'
                timelineData["Items"].append(
                    {
                        "start": ca["st"],
                        "end": ca["et"],
                        "title": "Carrying "
                        + ("cv-" if ca["is-critical"] else "v-")
                        + str(ca["vid"] + " not to destination"),
                        "group": player["id"],
                        "subgroup": "sg0",
                        "subgroupOrder": sg0_order,
                        "editable": False,
                        "className": "def_carrying-victim",
                    }
                )

        for rb in player["rubble"]:
            timelineData["Items"].append(
                {
                    "start": rb["st"],
                    "title": "Rubble Cleared",
                    "group": player["id"],
                    "subgroup": "sg0",
                    "subgroupOrder": sg0_order,
                    "type": "point",
                    "editable": False,
                    "className": "def_rubble",
                    "rubble_id": rb["rid"],
                }
            )
        
        for bombkey in player["bombs"].keys():
            for bomb in player["bombs"][bombkey]:
                # print (bomb)
                state = bomb['state']
                className = "def_bomb_inactive"
                if bomb['state'] == 'INACTIVE':
                    state = "is inactive"
                    continue
                elif bomb['state'] in ['INFO_ADDED']:
                    if bomb['tool'] == 'BOMB_BEACON':
                        state = "bomb beacon added"
                        className = 'def_bomb_beacon'
                    elif bomb['tool'] == 'HAZARD_BEACON':
                        state = "hazard beacon added"
                        if 'msg' in bomb.keys():
                            state = state + " msg: " + bomb['msg']
                        className = 'def_hazard_beacon'
                elif bomb['state'] in ['INFO_REMOVED']:
                    if bomb['tool'] in ['BOMB_BEACON', 'block_beacon_bomb']:
                        state = "bomb beacon removed"
                        className = 'def_bomb_beacon_removed'
                    elif bomb['tool'] in ['HAZARD_BEACON', 'block_beacon_hazard']:
                        state = "hazard beacon removed"
                        className = 'def_hazard_beacon_removed'
                elif bomb['state'] in ['FIRE_EXTINGUISHER']:
                    state = "Fire Extinguisher Used"
                    className = 'def_fire_extinguisher'
                elif bomb['state'] in ['TRIGGERED', 'PERTURBATION_FIRE_TRIGGER']:
                    state = "is triggered"
                    className = 'def_bomb_triggered'
                elif bomb['state'] in ['TRIGGERED_ADVANCE_SEQ']:
                    continue
                elif bomb['state'] in ['TOOL_USED']:
                    state = bomb['tool'][0] + " tool used"
                    if bomb['tool'][0] == 'R':
                        className = 'def_bomb_cut_red_wire'
                    elif bomb['tool'][0] == 'G':
                        className = 'def_bomb_cut_green_wire'
                    elif bomb['tool'][0] == 'B':
                        className = 'def_bomb_cut_blue_wire'
                elif bomb['state'] in ['EXPLODE_TIME_LIMIT', 'EXPLODE_TOOL_MISMATCH', 'EXPLODE_FIRE', 'EXPLODE_CHAINED_ERROR']:
                    state = 'exploded'
                    className = 'def_bomb_exploded'
                elif bomb['state'] == 'DEFUSED':
                    state = " has been defused"
                    className = 'def_bomb_defused'
                elif bomb['state'] == 'DEFUSED_DISPOSER':
                    state = " has been defused with a Disposer"
                    className = 'def_bomb_defused'

                timelineData["Items"].append(
                    {
                        "start": bomb["elapsed_milliseconds"],
                        "title": "Bomb " + bomb['id'] + " " + state,
                        "group": player["id"],
                        "subgroup": "sg0",
                        "subgroupOrder": sg0_order,
                        "type": "point",
                        "editable": False,
                        "className": className,
                        "bomb_id": bomb["id"],
                    }
                )
        
        for tr in player["trapped"]:
            timelineData["Items"].append(
                {
                    "start": tr["st"],
                    "end": tr["et"],
                    "title": f"Trapped In Room {tr['room']}",
                    "group": player["id"],
                    "subgroup": "sg0",
                    "subgroupOrder": sg0_order,
                    "editable": False,
                    "className": "def_trapped",
                }
            )
            if tr['type'] == 'threat':
                timelineData["Items"][-1]["title"] = f'Trapped In Threat Room {tr["room"]}'     
            if tr['type'] == 'perturbation':
                timelineData["Items"][-1]["title"] = f'Trapped In {tr["room"]} By Perturbation'

        # ts = 0
        # for diId in player['jag']:
        #     disc = player['jag'][diId]
        #     victim_id = disc["inputs"]["victim-id"] if "inputs" in disc and "victim-id" in disc["inputs"] else None
        #     victim_type = disc["inputs"]["victim-type"] if "inputs" in disc and "victim-type" in disc["inputs"] else None

        #     if victim_id is None or victim_type is None:
        #         continue

        #     if victim_id == 19:
        #         print('"player": "' + player['callsign'] + '",')
        #         print('"participant_id": "' + player['participant_id'] + '",')
        #         print(json.dumps(disc))

        #     # disc_leaves = getChildLeaves(disc)
        #     # for leaf in disc_leaves:
        #     #     if victim_id == 19:
        #     #         print(json.dumps(leaf) + ",")

        #     #     # if 'jags' not in leaf:
        #     #     #     continue
        #     #     # for jag in leaf['jags']:
        #     #     #     awareness = player['jag']['Event:Awareness'][leaf['id']] if 'Event:Awareness' in player['jag'] and leaf['id'] in player['jag']['Event:Awareness'] else None
        #     #     #     preparing = player['jag']['Event:Preparing'][leaf['id']] if 'Event:Preparing' in player['jag'] and leaf['id'] in player['jag']['Event:Preparing'] else None
        #     #     #     addressing = player['jag']['Event:Addressing'][leaf['id']] if 'Event:Addressing' in player['jag'] and leaf['id'] in player['jag']['Event:Addressing'] else None
        #     #     #     completion = player['jag']['Event:Completion'][leaf['id']] if 'Event:Completion' in player['jag'] and leaf['id'] in player['jag']['Event:Completion'] else None

        #     #     #     st = 999999999
        #     #     #     et = 0
        #     #     #     if awareness is not None:
        #     #     #         if awareness[-1]['elapsed_milliseconds'] > et:
        #     #     #             et = awareness[-1]['elapsed_milliseconds']
        #     #     #         if awareness[-1]['elapsed_milliseconds'] < st:
        #     #     #             st = awareness[-1]['elapsed_milliseconds']
        #     #     #     if preparing is not None:
        #     #     #         if preparing[-1]['elapsed_milliseconds'] > et:
        #     #     #             et = preparing[-1]['elapsed_milliseconds']
        #     #     #         if preparing[-1]['elapsed_milliseconds'] < st:
        #     #     #             st = preparing[-1]['elapsed_milliseconds']
        #     #     #     if addressing is not None:
        #     #     #         if addressing[-1]['elapsed_milliseconds'] > et:
        #     #     #             et = addressing[-1]['elapsed_milliseconds']
        #     #     #         if addressing[-1]['elapsed_milliseconds'] < st:
        #     #     #             st = addressing[-1]['elapsed_milliseconds']
        #     #     #     if completion is not None:
        #     #     #         if completion[-1]['elapsed_milliseconds'] > et:
        #     #     #             et = completion[-1]['elapsed_milliseconds']
        #     #     #         if completion[-1]['elapsed_milliseconds'] < st:
        #     #     #             st = completion[-1]['elapsed_milliseconds']

        #     #     #     timelineData['Items'].append({'start': ts, 'end': et, 'group': player['id'], 'subgroup': 'sg1', 'subgroupOrder': sg1_order,
        #     #     #                                     'editable': False, 'className': 'def_carrying-victim',
        #     #     #                                     'title': leaf["urn"].split(":")[-1] + " " + str(victim_id) + " " + victim_type[0]})

        # print()

        if player['callsign'] != 'server':
            playerColor = "background-color: " + player["callsign"] + ";"
            timelineData["Items"].append(
                {
                    "start": 0,
                    "end": 20 * 60000,
                    "group": player["id"],
                    "subgroup": "sgc",
                    "subgroupOrder": sgc_order,
                    "type": "background",
                    "style": playerColor,
                }
            )

    # Add the markers to the timline data logic and file.
    for markerGroup in markers:

        for marker in markers[markerGroup]:
            timelineData["Items"].append(
                {
                    "start": marker["start"],
                    "title": marker_title_map[marker["block_type"].split("_")[1]],
                    "group": groups[marker["callsign"]],
                    "subgroup": "sg0",
                    "subgroupOrder": 200,
                    "type": "box",
                    "editable": False,
                    "className": "marker_"
                    + marker["block_type"].split("_")[1]
                    + ("_placed" if marker["end"] == 9999999 else "_removed"),
                    "marker_location": f"{marker['z']}{marker['x']}",
                }
            )

    return timelineData

def processJagExp4(bombJags, callsign, participant_id, pid):
    """Used for processing the jag data for the timeline web visualizer."""
    
    activity_style_map = {
        "defuse-bomb-start": "background-color: rgba(255, 204,   0, 0.5); stroke-color: rgba(255, 204,   0, 0.5); border-color: gray; padding-top: 0px; margin-top: 0px;;",
        "defuse-bomb-success": "background-color: rgba(  0, 255,   0, 0.5); stroke-color: rgba(  0, 255,   0, 0.8); border-color: gray; padding-top: 0px; margin-top: 0px;",
        "defuse-bomb-failure": "background-color: rgba(255,   0,   0, 0.5); stroke-color: rgba(255,   0,   0, 0.8); border-color: gray; padding-top: 0px; margin-top: 0px;;",
        "cut-wire-success": "background-color: rgba(  0, 255, 255, 0.5); border-width: 0px, padding-top: 0px; margin-top: 0px;",
        "cut-wire-failure": "background-color: rgba(  0,   0, 255, 0.5); border-width: 0px, padding-top: 0px; margin-top: 0px;",
    }
    
    items = []
    for bomb in bombJags:
        if participant_id not in bombJags[bomb]['participants']:
            continue

        title = "" + bomb + " " + ','.join(bombJags[bomb]['sequence'])
        short_title = "" + bomb
        ts = bombJags[bomb]['start']
        te = 9999999
        style = activity_style_map['defuse-bomb-start']
        if 'success' in bombJags[bomb]:
            te = bombJags[bomb]['success']
            style = activity_style_map['defuse-bomb-success']
            title = title + " Success"
            short_title = short_title + " S"
        elif 'failure' in bombJags[bomb]:
            te = bombJags[bomb]['failure']
            style = activity_style_map['defuse-bomb-failure']
            title = title + " Failure"
            short_title = short_title + " F"

        item = {
            "start": ts,
            "end": te,
            "group": pid,
            "subgroup": "sg2",
            "subgroupOrder": sg2_order,
            "editable": False,
            "className": "def_jag_activity",
            "style": style,
            "content": short_title,
            "title": title,
            "bombJagKey": bomb
        }
        items.append(item)
    
    items.sort(key = lambda x: x['end'])
    lastGoodEnd = 0
    outputItems = []
    for i, item in enumerate(items):
        if i != 0:
            if items[i-1]['end'] != 9999999:
                item['start'] = items[i-1]['end']
                lastGoodEnd = item['start'];
            else:
                item['start'] = lastGoodEnd
        outputItems.append(item)

        prevStart = item['start']
        for wire in bombJags[item['bombJagKey']]['sequence']:
            if wire in bombJags[item['bombJagKey']]:
                if bombJags[item['bombJagKey']][wire]['pid'] == participant_id:
                    style = activity_style_map['cut-wire-success'] if bombJags[item['bombJagKey']][wire]['status'] == 'success' else activity_style_map['cut-wire-failure'];
                    title = "Cutting Wire " + wire + " on " + item['bombJagKey']
                    short_title = wire
                    wItem = {
                        "start": prevStart,
                        "end": bombJags[item['bombJagKey']][wire]['ts'],
                        "group": pid,
                        "subgroup": "sg2",
                        "subgroupOrder": sg2_order,
                        "editable": False,
                        "className": "def_jag_addressing",
                        "style": style,
                        "content": short_title,
                        "title": title
                    }
                    outputItems.append(wItem)
                prevStart = bombJags[item['bombJagKey']][wire]['ts']

    return outputItems


def processJag(jag, jagGroups, callsign, pid, planning_stop_time):
    """Used for processing the jag data for the timeline web visualizer."""

    activity_style_map = {
        "check-if-unlocked": "background-color: rgba(255, 255,   0, 0.5); padding-top: 0px; margin-top: 0px; font-size: 12px; height: 35px;",
        "unlock-victim": "background-color: rgba(  0, 255,   0, 0.5); padding-top: 0px; margin-top: 0px; font-size: 12px; height: 35px;",
        "pick-up-victim": "background-color: rgba(  0, 255, 255, 0.5); padding-top: 0px; margin-top: 0px; font-size: 12px; height: 35px;",
        "drop-off-victim": "background-color: rgba(  0,   0, 255, 0.5); padding-top: 0px; margin-top: 0px; font-size: 12px; height: 35px;",
        "at-proper-triage-area": "background-color: rgba(255, 255,   0, 0.5); padding-top: 0px; margin-top: 0px; font-size: 12px; height: 35px;",
        "default": "background-color: rgba(255,   0,   0, 0.5); padding-top: 0px; margin-top: 0px; font-size: 12px; height: 35px;",
    }

    if jag.has_children:
        for child in jag.children:
            processJag(child, jagGroups, callsign, pid, planning_stop_time)
    else:
        activity_type = jag.urn.split(":")[-1]
        title = (
            activity_type
            + " - "
            + str(jag.inputs["victim-id"])
            + " "
            + jag.inputs["victim-type"][0]
        )

        short_title = (
            str(jag.inputs["victim-id"])
            + jag.inputs["victim-type"][0]
            + " "
            + activity_type[0:3].upper()
        )

        summary = " - " + title
        handled = False

        prep_list = None
        addr_list = None

        preparing = jag.get_preparing()
        if callsign in preparing:
            prep_list = preparing[callsign]
        addressing = jag.get_addressing()
        if callsign in addressing:
            addr_list = addressing[callsign]

        size = len(prep_list) if prep_list is not None else 0
        # print("There are " + str(size) + " prepare activities for " + title)
        for i in range(0, size):
            jag_activity = {"start": None, "end": None, "items": [], "main": None}

            activity = prep_list[i]
            time_period = activity.time_period
            if time_period.has_ended:
                ts = (
                    time_period.start
                    if time_period.start >= planning_stop_time
                    else planning_stop_time
                )
                te = (
                    time_period.end
                    if time_period.end >= planning_stop_time
                    else planning_stop_time
                )
                jag_activity["start"] = ts
                jag_activity["end"] = te
                if ts == te:
                    te = ts + 100
                jag_activity["items"].append(
                    {
                        "start": ts,
                        "end": te,
                        "group": pid,
                        "subgroup": "sg2",
                        "subgroupOrder": sg2_order,
                        "editable": False,
                        "className": "def_jag_preparing",
                        "style": "background-color: rgba(100, 100, 100, 0.5); height: 10px;",
                        "title": title + " preparing",
                    }
                )

                summary += "    - preparing: " + str(ts) + " - " + str(te)
                handled = True

            if addr_list is not None and i < len(addr_list):
                activity = addr_list[i]
                time_period = activity.time_period
                if time_period.has_ended:
                    ts = (
                        time_period.start
                        if time_period.start >= planning_stop_time
                        else planning_stop_time
                    )
                    te = (
                        time_period.end
                        if time_period.end >= planning_stop_time
                        else planning_stop_time
                    )
                    if jag_activity["start"] is None or jag_activity["start"] > ts:
                        jag_activity["start"] = ts
                    if jag_activity["end"] is None or jag_activity["end"] < te:
                        jag_activity["end"] = te
                    if ts == te:
                        te = ts + 100
                    jag_activity["items"].append(
                        {
                            "start": ts,
                            "end": te,
                            "group": pid,
                            "subgroup": "sg2",
                            "subgroupOrder": sg2_order,
                            "editable": False,
                            "className": "def_jag_addressing",
                            "style": "background-color: rgba(200, 200, 200, 0.5); height: 15px;",
                            "title": title + " addressing",
                        }
                    )

                    summary += "    - addressing: " + str(ts) + " - " + str(te)
                    handled = True

            if jag_activity["start"] is not None and jag_activity["end"] is not None:
                style = (
                    activity_style_map[activity_type]
                    if activity_type in activity_style_map
                    else activity_style_map["default"]
                )
                jag_activity["main"] = {
                    "start": jag_activity["start"],
                    "end": jag_activity["end"],
                    "group": pid,
                    "subgroup": "sg2",
                    "subgroupOrder": sg2_order,
                    "editable": False,
                    "className": "def_jag_activity",
                    "style": style,
                    "content": short_title,
                    "title": title,
                }
                updateJagGroups(jag_activity, jagGroups)

        # if handled:
        #     print(summary)


def updateJagGroups(jag_activity, jagGroups):
    """Updates the Jag Groups for the timeline on the web visualizer."""

    if (
        jag_activity is None
        or "main" not in jag_activity
        or jag_activity["main"] is None
    ):
        return

    for jg in jagGroups:
        overlayed = False
        for ja in jg:
            if "main" in ja and ja["main"] is not None:
                start = max(ja["main"]["start"], jag_activity["main"]["start"])
                end = min(ja["main"]["end"], jag_activity["main"]["end"])
                if start < end:
                    overlayed = True
                    break
        if not overlayed:
            jg.append(jag_activity)
            return

    jagGroups.append([jag_activity])

def getChildLeaves(tree, leaves=[]):
    """"""

    if "children" in tree and len(tree["children"]) > 0:
        for child in tree["children"]:
            getChildLeaves(child, leaves)
    else:
        leaves.append(tree)

    return leaves
import json

def getBoundingBox(obj):
    if 'bounds' not in obj.keys():
        return None

    x1 = 999999
    z1 = 999999
    x2 = -999999
    z2 = -999999
    
    bounds = obj['bounds']

    bt = bounds['type']
    if bt == 'rectangle' or bt == 'line':
        x1 = bounds['coordinates'][0]['x']
        z1 = bounds['coordinates'][0]['z']
        x2 = bounds['coordinates'][1]['x']
        z2 = bounds['coordinates'][1]['z']
    elif bt == 'block':
        x1 = bounds['coordinates'][0]['x']
        z1 = bounds['coordinates'][0]['z']
        x2 = x1+1
        z2 = z1+1
    else:
        return None
        
    if x1 > x2:
        tmp = x2
        x2 = x1
        x1 = tmp
    if z1 > z2:
        tmp = z2
        z2 = z1
        z1 = tmp
        
    return [x1, z1, x2, z2]

def getBounds(semanticMap, expVersion):
    semanticMapSVG = ''
    max_x = -999999
    max_y = -999999
    min_x = 999999
    min_y = 999999
    
    for loc in semanticMap['locations']:
        bounds = getBoundingBox(loc)
        if bounds is None:
            continue;
        
        if expVersion == 4 and bounds[0] < 0:
            bounds[0] = bounds[0] + 36
        if expVersion == 4 and bounds[0] < 0:
            bounds[2] = bounds[2] + 36

        if min_x > bounds[0]:
            min_x = bounds[0]
        if max_x < bounds[2]:
            max_x = bounds[2]
        if min_y > bounds[1]:
            min_y = bounds[1]
        if max_y < bounds[3]:
            max_y = bounds[3]
    
    for conn in semanticMap['connections']:
        bounds = getBoundingBox(conn)
        if bounds is None:
            continue;
        
        if expVersion == 4 and bounds[0] < 0:
            bounds[0] = bounds[0] + 36
        if expVersion == 4 and bounds[0] < 0:
            bounds[2] = bounds[2] + 36

        if min_x > bounds[0]:
            min_x = bounds[0]
        if max_x < bounds[2]:
            max_x = bounds[2]
        if min_y > bounds[1]:
            min_y = bounds[1]
        if max_y < bounds[3]:
            max_y = bounds[3]
    
    for obj in semanticMap['objects']:
        bounds = getBoundingBox(obj)
        if bounds is None:
            continue;

        if expVersion == 4 and bounds[0] < 0:
            bounds[0] = bounds[0] + 36
        if expVersion == 4 and bounds[0] < 0:
            bounds[2] = bounds[2] + 36

        if min_x > bounds[0]:
            min_x = bounds[0]
        if max_x < bounds[2]:
            max_x = bounds[2]
        if min_y > bounds[1]:
            min_y = bounds[1]
        if max_y < bounds[3]:
            max_y = bounds[3]

    return [min_x, min_y, max_x-min_x, max_y-min_y]

def BaseMap2SVG(baseMap, xtrans, ztrans, expVersion):
    baseMapSVG = ''

    walls = {}
    signs = []

    if expVersion == 4:
        extraInfo = [
            [[2,52,38],'dirt',0], [[2,52,39],'dirt',0], [[3,52,40],'dirt',0], [[4,52,41],'dirt',0], [[5,52,42],'dirt',0], 
            [[4,52,44],'dirt',0], [[6,52,44],'dirt',0], [[8,52,48],'dirt',0], [[9,52,47],'dirt',0], [[10,52,47],'dirt',0],
            [[10,52,48],'dirt',0], [[11,52,49],'dirt',0], [[12,53,50],'dirt',0], [[13,53,50],'dirt',0], [[14,53,50],'dirt',0],
            [[15,53,50],'dirt',0], [[16,53,50],'dirt',0], [[17,53,50],'dirt',0], [[18,53,50],'dirt',0], [[19,53,50],'dirt',0],
            [[21,53,50],'dirt',0], [[22,53,50],'dirt',0], [[23,53,50],'dirt',0], [[30,53,50],'dirt',0], [[31,53,50],'dirt',0],
            [[32,53,50],'dirt',0], [[33,53,50],'dirt',0], [[38,53,50],'dirt',0], [[39,53,50],'dirt',0], [[40,53,50],'dirt',0],
            [[41,53,50],'dirt',0], [[42,53,50],'dirt',0], [[43,53,50],'dirt',0], [[44,53,50],'dirt',0], [[35,53,44],'dirt',0],
            [[26,53,50],'dirt',0], [[34,53,50],'dirt',0]
        ]
        for data in extraInfo:
            x = data[0][0]
            z = data[0][2]
            if x < 0:
                x = x + 36
            key = '' + str(x) + ',' + str(z) 
            if key not in walls.keys():
                walls[key] = []
            walls[key].append(data)

    for data in baseMap['data']:
        x = data[0][0]
        y = data[0][1]
        z = data[0][2]

        if expVersion == 4:
            if y <= 51:
                continue
            if x < 0:
                x = x + 36
        
        if data[1] == 'wall_sign':
            signs.append(data)
            continue

        if data[1] in ['wall_sign', 'wall_banner', 'lever', 'flower_pot', 'barrier', \
                       'tripwire_hook', 'redstone_torch', 'ladder', 'cake', 'brewing_stand', 'torch'] or \
                       data[1].endswith('_button') or data[1].endswith('_door') or data[1].endswith('plant') or data[1].endswith('grass'):
            continue
        if expVersion != 4 and data[1] in ['water']:
            continue;

        key = '' + str(x) + ',' + str(z) 
        if key not in walls.keys():
            walls[key] = []
        walls[key].append(data)
    
    # default to the color of something that cannot be passed through
    wallStyle = "fill:rgb(100, 45, 102); fill-opacity:0.7;"
    # Color single blocks on the groud differently.  They are normally chairs or tables.
    furnitureStyle = "fill:rgb(194, 128, 16); fill-opacity:0.7;"
    wallSVG = '<g style="' + wallStyle + '">'
    furnitureSVG = '<g style="' + furnitureStyle + '">'
    # column that has more 1 block or the one block is not high enough is a wall
    for wall in walls.values():
        x = wall[0][0][0] - xtrans
        y = wall[0][0][1]
        z = wall[0][0][2] - ztrans
        type = wall[0][1]

        # if the wall only has one block at the top ignore it.
        # if len(wall) <= 1 and y in [60, 29, 52]:
        #     continue

        if len(wall) == 1 and y in [60, 29, 52]:
            furnitureSVG += '<rect x="' + str(x) + '" y="' + str(z) + '" width="1" height="1"></rect>'
        else:
            wallSVG += '<rect x="' + str(x) + '" y="' + str(z) + '" width="1" height="1"></rect>'

    wallSVG += '</g>'
    furnitureSVG += '</g>'
    baseMapSVG += wallSVG + furnitureSVG

    # for door in baseMap['doors']:
    #     x = door[0][0] - xtrans
    #     z = door[0][2] - ztrans

    #     width = 1
    #     height = 1
    
    #     style = "fill:rgb(232, 45, 12);"
    #     baseMapSVG += '<rect x="' + str(x) + '" y="' + str(z) + '" width="' + str(width) + '" height="' + str(height) + '" style="' + style + '"></rect>';

    # for lever in baseMap['levers']:
    #     x = lever[0][0] - xtrans
    #     y = lever[0][1]
    #     z = lever[0][2] - ztrans
    #     facing = lever[2] & 0x7

    #     style =     "fill:rgb(200, 45, 102);"
    #     if lever[1].endswith('_button'):
    #         style =     "fill:rgb(100, 100, 100);"
        
    #     width = 1
    #     height = 1

    #     if facing == 4:    # 4 = north
    #         z+=.6
    #         height = 0.4
    #     elif facing == 3:  # 3 = south
    #         height = 0.4
    #     elif facing == 2:  # 2 = west
    #         x+=.6
    #         width = 0.4
    #     else:              # 1 = east
    #         width = 0.4
    #     baseMapSVG += '<rect x="' + str(x) + '" y="' + str(z) + '" width="' + str(width) + '" height="' + str(height) + '" style="' + style + '"></rect>'

    signSVG = '<g style="fill:rgb(200, 45, 102);">'

    for sign in signs:
        x = sign[0][0] - xtrans
        y = sign[0][1]
        z = sign[0][2] - ztrans
        facing = sign[2]
        text = sign[3]
        width = 1
        height = 1

        if facing == 2:      # 2 = north
            z+=.6
            height = 0.4;
        elif facing == 3:    # 3 = south
            height = 0.4;
        elif facing == 4:    # 4 = west
            x+=.6
            width = 0.4
        else:    # 5 = east
            width = 0.4

        signSVG += '<rect x="' + str(x) + '" y="' + str(z) + '" width="' + str(width) + '" height=\"' + str(height) + '"></rect>';

    baseMapSVG += signSVG + '</g>'

    return '<g id="' + 'svg_basemap' + '">' + baseMapSVG + '</g>'

def SemanticMap2SVG(semanticMap, xtrans, ztrans, expVersion):
    semanticMapSVG = ''
    for loc in semanticMap['locations']:
        if 'bounds' not in loc.keys():
            continue
        
        bounds = loc['bounds']

        x1 = bounds['coordinates'][0]['x'];
        z1 = bounds['coordinates'][0]['z'];
        x2 = bounds['coordinates'][1]['x'];
        z2 = bounds['coordinates'][1]['z'];
        if expVersion == 4 and x1 < 0:
            x1 = x1 + 36
        if expVersion == 4 and x2 < 0:
            x2 = x2 + 36

        if x1 > x2:
            tmp = x2
            x2 = x1
            x1 = tmp
        if z1 > z2:
            tmp = z2
            z2 = z1
            z1 = tmp
        
        width = x2 - x1
        height = z2 - z1

        x1 -= xtrans
        z1 -= ztrans

        style = "fill:rgb(200,200,200);"
        style += 'stroke:rgb(200, 200, 200);stroke-width:0.1;'

        semanticMapSVG += "<rect x=\"" + str(x1) + "\" y=\"" + str(z1) + "\" width=\"" + str(width) + "\" height=\"" + str(height) + "\" style=\"" + style + "\"></rect>"

    for conn in semanticMap['connections']:
        if 'bounds' not in conn.keys():
            continue

        bounds = conn['bounds']

        x1 = bounds['coordinates'][0]['x']
        z1 = bounds['coordinates'][0]['z']
        x2 = bounds['coordinates'][1]['x']
        z2 = bounds['coordinates'][1]['z']
        if expVersion == 4 and x1 < 0:
            x1 = x1 + 36
        if expVersion == 4 and x2 < 0:
            x2 = x2 + 36
        
        if x1 > x2:
            tmp = x2
            x2 = x1
            x1 = tmp
        if z1 > z2:
            tmp = z2
            z2 = z1
            z1 = tmp

        width = x2 - x1
        height = z2 - z1

        if width == 0 or height == 0:
            continue

        x1 -= xtrans
        z1 -= ztrans
        x2 -= xtrans
        z2 -= ztrans

        style =       "fill:rgb(163, 73, 24); fill-opacity:0.7;"
        line_style =  "stroke:rgb(2, 158, 62);stroke-width:0.2;"
        t = conn['type']
        if t == 'elevator_door':
            style = "fill:rgb(191, 38, 11); fill-opacity:0.7;"
            line_style =  "stroke:rgb(191, 38, 11);stroke-width:0.2;"
        elif t == 'opening':
            style = "fill:rgb(150,150,150); fill-opacity:0.7;"
            line_style =  "stroke:rgb(150,150,150);stroke-width:0.2;"
        elif t == 'extension':
            style = "fill:rgb(166,166,166); fill-opacity:0.7;"
            line_style =  "stroke:rgb(166,166,166);stroke-width:0.2;"
        else:
            # case 'door':
            # case 'double_door':
            # default:  // doors
            style = "fill:rgb(144, 70, 14); fill-opacity:0.7;"
            line_style =  "stroke:rgb(144, 70, 14);stroke-width:0.2;"

        semanticMapSVG += "<rect x=\"" + str(x1) + "\" y=\"" + str(z1) + "\" width=\"" + str(width) + "\" height=\"" + str(height) + "\" style=\"" + style + "\"></rect>"

    return '<g id="' + 'svg_semanticmap' + '">' + semanticMapSVG + "</g>"

def generateMarkerBlockSVG(markers, xtrans, ztrans):
    if markers is None or len(markers) <= 0:
        return '', []

    midx = 0
    markersSVG = '<g id="svg_markers" style="stroke-width:0.1;">'
    markerTimings = []
    markerPerColor = {}
    for marker_list in markers.values():
        for marker in marker_list:
            if marker['color'] not in markerPerColor.keys():
                styleLine = "stroke:rgb(10, 10, 150);"
                if marker['color'] == 'rgba(0, 0, 255, 0.6)':
                    styleLine = "stroke:rgb(200, 200, 200);"
                style = "fill:" + marker['color'] + ";" + styleLine
                markerPerColor[marker['color']] = '<g id="svg_mg_' + marker['callsign'][0] + '" style="' + style + '">'
                # print("*** New Marker Color: " + marker['color'])
                # print("***      - " + markerPerColor[marker['color']])
            x = marker['x'] - xtrans - 0.2
            z = marker['z'] - ztrans - 0.2
            mid = 'svg_' + marker['id']
            midx = midx + 1
            markerPerColor[marker['color']] += '<g id="' + mid + '">'
            markerPerColor[marker['color']] += "<rect x=\"" + str(x) + "\" y=\"" + str(z) + "\" width=\"1\" height=\"1\"></rect>"

            if marker['number'] == 1:
                markerPerColor[marker['color']] += '<line x1="' + str(x + 0.5) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.5) + '" y2="' + str(z+0.8) + '"></line>'
            elif marker['number'] == 2:
                markerPerColor[marker['color']] += '<line x1="' + str(x + 0.3) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.6) + '" y2="' + str(z+0.2) + '"></line>'
                markerPerColor[marker['color']] += '<line x1="' + str(x + 0.6) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.6) + '" y2="' + str(z+0.5) + '"></line>'
                markerPerColor[marker['color']] += '<line x1="' + str(x + 0.6) + '" y1="' + str(z + 0.5) + '" x2="' + str(x+0.3) + '" y2="' + str(z+0.5) + '"></line>'
                markerPerColor[marker['color']] += '<line x1="' + str(x + 0.3) + '" y1="' + str(z + 0.5) + '" x2="' + str(x+0.3) + '" y2="' + str(z+0.8) + '"></line>'
                markerPerColor[marker['color']] += '<line x1="' + str(x + 0.3) + '" y1="' + str(z + 0.8) + '" x2="' + str(x+0.6) + '" y2="' + str(z+0.8) + '"></line>'
            elif marker['number'] == 3:
                markerPerColor[marker['color']] += '<line x1="' + str(x + 0.3) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.6) + '" y2="' + str(z+0.2) + '"></line>'
                markerPerColor[marker['color']] += '<line x1="' + str(x + 0.6) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.6) + '" y2="' + str(z+0.8) + '"></line>'
                markerPerColor[marker['color']] += '<line x1="' + str(x + 0.6) + '" y1="' + str(z + 0.5) + '" x2="' + str(x+0.3) + '" y2="' + str(z+0.5) + '"></line>'
                markerPerColor[marker['color']] += '<line x1="' + str(x + 0.3) + '" y1="' + str(z + 0.8) + '" x2="' + str(x+0.6) + '" y2="' + str(z+0.8) + '"></line>'
            else:
                if marker['block_type'].endswith('_abrasion'):
                    markerPerColor[marker['color']] += '<line x1="' + str(x + 0.5) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.2) + '" y2="' + str(z+0.8) + '"></line>'
                    markerPerColor[marker['color']] += '<line x1="' + str(x + 0.5) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.8) + '" y2="' + str(z+0.8) + '"></line>'
                    markerPerColor[marker['color']] += '<line x1="' + str(x + 0.35) + '" y1="' + str(z + 0.6) + '" x2="' + str(x+0.65) + '" y2="' + str(z+0.6) + '"></line>'
                elif marker['block_type'].endswith('_bonedamage'):
                    markerPerColor[marker['color']] += '<line x1="' + str(x + 0.3) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.3) + '" y2="' + str(z+0.8) + '"></line>'
                    markerPerColor[marker['color']] += '<line x1="' + str(x + 0.3) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.5) + '" y2="' + str(z+0.2) + '"></line>'
                    markerPerColor[marker['color']] += '<line x1="' + str(x + 0.5) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.7) + '" y2="' + str(z+0.35) + '"></line>'
                    markerPerColor[marker['color']] += '<line x1="' + str(x + 0.7) + '" y1="' + str(z + 0.35) + '" x2="' + str(x+0.5) + '" y2="' + str(z+0.5) + '"></line>'
                    markerPerColor[marker['color']] += '<line x1="' + str(x + 0.5) + '" y1="' + str(z + 0.5) + '" x2="' + str(x+0.3) + '" y2="' + str(z+0.5) + '"></line>'
                    markerPerColor[marker['color']] += '<line x1="' + str(x + 0.5) + '" y1="' + str(z + 0.5) + '" x2="' + str(x+0.7) + '" y2="' + str(z+0.65) + '"></line>'
                    markerPerColor[marker['color']] += '<line x1="' + str(x + 0.7) + '" y1="' + str(z + 0.65) + '" x2="' + str(x+0.5) + '" y2="' + str(z+0.8) + '"></line>'
                    markerPerColor[marker['color']] += '<line x1="' + str(x + 0.5) + '" y1="' + str(z + 0.8) + '" x2="' + str(x+0.3) + '" y2="' + str(z+0.8) + '"></line>'
                elif marker['block_type'].endswith('_novictim'):
                    markerPerColor[marker['color']] += ""
                elif marker['block_type'].endswith('_regularvictim'):
                    markerPerColor[marker['color']] += '<polyline points="' + str(x + 0.3) + ',' + str(z + 0.8) + ' ' \
                                                                            + str(x + 0.3) + ',' + str(z + 0.2) + ' ' \
                                                                            + str(x + 0.5) + ',' + str(z + 0.2) + ' ' \
                                                                            + str(x + 0.7) + ',' + str(z + 0.35) + ' ' \
                                                                            + str(x + 0.5) + ',' + str(z + 0.5) + ' ' \
                                                                            + str(x + 0.4) + ',' + str(z + 0.5) + ' ' \
                                                                            + str(x + 0.7) + ',' + str(z + 0.8) + '">'
                elif marker['block_type'].endswith('_criticalvictim'):
                    markerPerColor[marker['color']] += '<polyline points="' + str(x + 0.7) + ',' + str(z + 0.35) + ' ' \
                                                                            + str(x + 0.5) + ',' + str(z + 0.2) + ' ' \
                                                                            + str(x + 0.3) + ',' + str(z + 0.35) + ' ' \
                                                                            + str(x + 0.3) + ',' + str(z + 0.65) + ' ' \
                                                                            + str(x + 0.5) + ',' + str(z + 0.8) + ' ' \
                                                                            + str(x + 0.7) + ',' + str(z + 0.65) + '">'
                elif marker['block_type'].endswith('_threat'):
                    markerPerColor[marker['color']] += '<polygon points="' + str(x + 0.5) + ',' + str(z + 0.2) + ' ' \
                                                                           + str(x + 0.8) + ',' + str(z + 0.8) + ' ' \
                                                                           + str(x + 0.2) + ',' + str(z + 0.8) + ' ' \
                                                                         '" style="fill:yellow;" />'
                elif marker['block_type'].endswith('_rubble'):
                    markerPerColor[marker['color']] += '<rect x="' + str(x+0.4) + '" y="' + str(z+0.4) + '" width="0.6" height="0.6" style="fill:grey;"></rect>'
                elif marker['block_type'].endswith('_sos'):
                    markerPerColor[marker['color']] += '<polyline points="' + str(x + 0.7) + ',' + str(z + 0.35) + ' ' \
                                                                            + str(x + 0.5) + ',' + str(z + 0.2) + ' ' \
                                                                            + str(x + 0.3) + ',' + str(z + 0.35) + ' ' \
                                                                            + str(x + 0.3) + ',' + str(z + 0.4) + ' ' \
                                                                            + str(x + 0.7) + ',' + str(z + 0.6) + ' ' \
                                                                            + str(x + 0.7) + ',' + str(z + 0.65) + ' ' \
                                                                            + str(x + 0.5) + ',' + str(z + 0.8) + ' ' \
                                                                            + str(x + 0.3) + ',' + str(z + 0.65) + '">'
            markerPerColor[marker['color']] += '</g>'
            end = marker['end'] if marker['end'] != 9999999 else -1
            markerTimings.append([mid, marker['start'], end, 'gd'])

    for svg in markerPerColor.values():
        markersSVG += svg + '</g>'

    markersSVG += '</g>'

    return markersSVG, markerTimings

def generateItemSVG(items, xtrans, ztrans):
    if items is None or len(items) <= 0:
        return ''
    
    styleItem = "fill:rgb(10, 10, 10);stroke:rgb(0, 0, 0);stroke-width:0;"
    styleBombItem = "fill:rgb(0, 250, 250);stroke:rgb(10, 10, 10);stroke-width:0.1;"
    styleHazardItem = "fill:rgb(230, 130, 0);stroke:rgb(10, 10, 10);stroke-width:0.1;"
    styleFireItem = "fill:rgb(250, 0, 0);"
    itemSVG = '<g id="svg_items" style="' + styleItem + '">'
    itemTimings = []

    for item_list in items.values():
        st = 9999999
        et = -1
        id = None
        x = -1
        z = -1
        type = None

        for idx, item in enumerate(item_list):
            if id is None and 'id' in item.keys():
                id = 'svg_' + item['id']
            if x < 0 and 'x' in item.keys():
                x = item['x'] - xtrans
            if z < 0 and 'z' in item.keys():
                z = item['z'] - ztrans
            if 'ts' in item.keys():
                if item['topic'] == 'environment/created/single' and st > item['ts']:
                    st = item['ts']
                if item['topic'] == 'environment/removed/single' and et < item['ts']:
                    et = item['ts']
            if type is None and 'type' in item.keys():
                type = item['type']

        if st == 9999999:
            st = 0
        if et == -1:
            et = 9999999
        style = ''
        size = 1
        if type is not None:
            if type in ['block_beacon_bomb', 'BOMB_BEACON']:
                style = styleBombItem
                size = 1.4
                x = x - 0.2
                z = z - 0.2
            elif type in ['block_beacon_hazard']:
                style = styleHazardItem
                size = 1.4
                x = x - 0.2
                z = z - 0.2
            elif type == 'block_fire_custom':
                style = styleFireItem
            else:
                # if type not in ['BOMB_SENSOR', 'FIRE_EXTINGUISHER', 'block_bomb_fire', 'block_bomb_standard', 'block_bomb_chained', 'WIRECUTTERS_BLUE', 'WIRECUTTERS_RED', 'WIRECUTTERS_GREEN']:
                #     print("Unhandled Item Type:")
                #     print("==========================================")
                #     print(json.dumps(item_list))
                #     print("==========================================")
                continue
        else:
            # if type not in ['BOMB_SENSOR', 'FIRE_EXTINGUISHER', 'block_bomb_fire', 'block_bomb_standard', 'block_bomb_chained', 'WIRECUTTERS_BLUE', 'WIRECUTTERS_RED', 'WIRECUTTERS_GREEN']:
            #     print("Unhandled Item Type:")
            #     print("==========================================")
            #     print(json.dumps(item_list))
            #     print("==========================================")
            continue

        itemSVG += '<g id="' + id + '"> <rect  x="' + str(x) + '" y="' + str(z) + '" width="' + str(size) + '" height="' + str(size) + '" style="' + style + '"></rect></g>'
        itemTimings.append([id, st, et, 'gd'])
        # if type != 'block_fire_custom':
        #     print("item: " + id + " st:" + str(st) + " et:" + str(et))
        if st == 0:
            print("item: " + id + " st:" + str(st) + " et:" + str(et))

    itemSVG += '</g>'
    return itemSVG, itemTimings

def generateBombSVG(bombs, xtrans, ztrans):
    # print("Generating BombSVG for " + str(len(bombs)) + " bombs.")
    if bombs is None or len(bombs) <= 0:
        return '', []
    
    styleBomb = "fill:rgb(10, 10, 10);stroke:rgb(200, 200, 200);stroke-width:0.1;"
    styleTriggered = "fill:rgb(248, 252, 3);stroke:rgb(10, 10, 10)"
    styleExploded = "fill:rgb(255, 10, 10);stroke:rgb(10, 10, 10)"
    styleDiffused = "fill:rgb(10, 200, 10);stroke:rgb(10, 10, 10)"
    bombSVG = '<g id="svg_bombs" style="' + styleBomb + '">'
    bombTimings = []
    prevTime = -1
    for bomb_list in bombs.values():
        prevTime = 0
        for idx, bomb in enumerate(bomb_list):
            x = bomb['x'] - xtrans
            z = bomb['z'] - ztrans
            bid = 'svg_' + bomb['id'] + '_' + str(idx)
            if bomb['state'] == 'INACTIVE':
                bombSVG += '<g id="' + bid + '">'
            elif bomb['state'] in ['TRIGGERED_ADVANCE_SEQ', 'TRIGGERED', 'PERTURBATION_FIRE_TRIGGER']:
                bombSVG += '<g id="' + bid + '" style="' + styleTriggered + '">'
            elif bomb['state'] in ['EXPLODE_TIME_LIMIT', 'EXPLODE_TOOL_MISMATCH', 'EXPLODE_FIRE', 'EXPLODE_CHAINED_ERROR']:
                bombSVG += '<g id="' + bid + '" style="' + styleExploded + '">'
            elif bomb['state'] in ['DEFUSED', 'DEFUSED_DISPOSER']:
                bombSVG += '<g id="' + bid + '" style="' + styleDiffused + '">'
            else:
                bombSVG += '<g id="' + bid + '">'

            bombSVG += "<rect x=\"" + str(x) + "\" y=\"" + str(z) + "\" width=\"1\" height=\"1\"></rect>"
            if bomb['type'] == 'block_bomb_chained':
                # draw an 'C' to indicate chained bomb.
                bombSVG += '<line x1="' + str(x+0.2) + '" y1="' + str(z+0.65) + '" x2="' + str(x+0.5) + '" y2="' + str(z+0.35) + '"></line>'
                bombSVG += '<line x1="' + str(x+0.5) + '" y1="' + str(z+0.35) + '" x2="' + str(x+0.8) + '" y2="' + str(z+0.65) + '"></line>'
            elif bomb['type'] == 'block_bomb_fire':
                # draw an 'F' to indicate fire bomb.
                bombSVG += '<line x1="' + str(x+0.8) + '" y1="' + str(z+0.35) + '" x2="' + str(x+0.2) + '" y2="' + str(z+0.35) + '"></line>'
                bombSVG += '<line x1="' + str(x+0.8) + '" y1="' + str(z+0.35) + '" x2="' + str(x+0.8) + '" y2="' + str(z+0.65) + '"></line>'
                bombSVG += '<line x1="' + str(x+0.5) + '" y1="' + str(z+0.35) + '" x2="' + str(x+0.5) + '" y2="' + str(z+0.6) + '"></line>'

            bombSVG += '</g>'

            if bomb['start'] != 0 or bomb['end'] != 9999999:
                end = bomb['end'] if bomb['end'] != 9999999 else -1
                # if victim['id'].startswith('12 '):
                #     print("  - Timings End at: " + str(end))
                bombTimings.append([bid, bomb['start'], end, 'gd'])
   
    bombSVG += '</g>'
                           
    return bombSVG, bombTimings


def generateVictimSVG(victims, xtrans, ztrans):
    if victims is None or len(victims) <= 0:
        return '', []

    styleTriaged = "fill:rgb(10, 10, 150);"
    styleEvacuated = "fill:rgb(10, 200, 10);stroke:rgb(200, 200, 200);stroke-width:0.01;"
    styleLocked = "stroke:rgb(10, 10, 150);stroke-width:0.1;"
    # styleNonCritical = "fill:rgb(8, 161, 56); fill-opacity:0.7;stroke:rgb(10, 10, 10);stroke-width:0.1;"
    styleNonCritical = "fill:rgb(248, 252, 3);stroke:rgb(10, 10, 10);stroke-width:0.1;"
    # styleCritical = "fill:rgb(255, 165, 0);stroke:rgb(10, 10, 10);stroke-width:0.1;"
    styleCritical = "fill:rgb(255, 0, 0);stroke-width:0.01;"

    victimSVG = '<g id="svg_victims" style="' + styleNonCritical + '">'
    victimTimings = []
    for victim_list in victims.values():
        for victim in victim_list:
            # if victim['end'] != 9999999:
            #     continue
            x = victim['x'] - xtrans
            z = victim['z'] - ztrans
            vid = 'svg_' + victim['sid']
            victimSVG += '<g id="' + vid + '">'

            # set the fill/stroke based on critical or none critical and draw the victim's box
            victimSVG += "<rect x=\"" + str(x) + "\" y=\"" + str(z) + "\" width=\"1\" height=\"1\"></rect>"

            if victim['is-critical']:
                victimSVG += "<rect x=\"" + str(x+0.05) + "\" y=\"" + str(z+0.05) + "\" width=\"0.25\" height=\"0.25\" style=\"" + styleCritical + "\"></rect>"
                victimSVG += "<rect x=\"" + str(x+0.7) + "\" y=\"" + str(z+0.05) + "\" width=\"0.25\" height=\"0.25\" style=\"" + styleCritical + "\"></rect>"
                victimSVG += "<rect x=\"" + str(x+0.05) + "\" y=\"" + str(z+0.7) + "\" width=\"0.25\" height=\"0.25\" style=\"" + styleCritical + "\"></rect>"
                victimSVG += "<rect x=\"" + str(x+0.7) + "\" y=\"" + str(z+0.7) + "\" width=\"0.25\" height=\"0.25\" style=\"" + styleCritical + "\"></rect>"
            elif victim['id'].endswith('A'):
                # draw an 'A' to indicate victim type A.
                victimSVG += '<line x1="' + str(x+0.5) + '" y1="' + str(z+0.2) + '" x2="' + str(x+0.2) + '" y2="' + str(z+0.8) + '"></line>'
                victimSVG += '<line x1="' + str(x+0.5) + '" y1="' + str(z+0.2) + '" x2="' + str(x+0.8) + '" y2="' + str(z+0.8) + '"></line>'
                victimSVG += '<line x1="' + str(x+0.35) + '" y1="' + str(z+0.6) + '" x2="' + str(x+0.65) + '" y2="' + str(z+0.6) + '"></line>'

            if victim['evacuated']:
                # draw the little green square indicating evacuated.
                victimSVG += '<rect x="' + str(x + 0.3) + '" y="' + str(z + 0.5) + '" width="0.4" height="0.4" style="' + styleEvacuated + '"></rect>'
            elif victim['triaged']:
                # draw the little black square indicating triaged.
                victimSVG += '<rect x="' + str(x + 0.3) + '" y="' + str(z + 0.5) + '" width="0.4" height="0.4" style="' + styleTriaged + '"></rect>'
            elif not victim['unlocked']:
                # draw an X over the victim if it is still locked
                victimSVG += '<line x1="' + str(x) + '" y1="' + str(z) + '" x2="' + str(x+1) + '" y2="' + str(z+1) + '"></line>'
                victimSVG += '<line x1="' + str(x+1) + '" y1="' + str(z) + '" x2="' + str(x) + '" y2="' + str(z+1) + '"></line>'

            victimSVG += '</g>'

            # if victim['id'].startswith('12 '):
            #      printVictimInfo(victim)

            if victim['start'] != 0 or victim['end'] != 9999999:
                end = victim['end'] if victim['end'] != 9999999 else -1
                # if victim['id'].startswith('12 '):
                #     print("  - Timings End at: " + str(end))
                victimTimings.append([vid, victim['start'], end, 'gd'])

    victimSVG += '</g>'
                           
    return victimSVG, victimTimings

def printVictimInfo(v):
    print("VI: " + v['id'] + \
          " st:" + str(v['start']) + \
          " ed:" + str(v['end']) + \
          " x:" + str(v['x']) + \
          " z:" + str(v['z']) + \
          " ul:" + str(v['unlocked']) + \
          " tr:" + str(v['triaged']) + \
          " ev:" + str(v['evacuated'])
    )

def generateRubbleSVG(rubbleHT, xtrans, ztrans):
    if rubbleHT is None or len(rubbleHT) <= 0:
        return '', []

    style = "fill:rgb(120, 120, 120);stroke:rgb(10, 10, 50);stroke-width:0.1;"
    rubbleSVG = '<g id="svg_rubble" style="' + style + '">'
    rubbleTimings = []
    for rubble_list in rubbleHT.values():
        for rubble in rubble_list:
            # if rubble['end'] != 9999999:
            #     continue
            x = rubble['x'] - xtrans
            z = rubble['z'] - ztrans
            rid = 'svg_' + str(rubble['id']) + '_' + str(rubble['total'])
            rubbleSVG += '<g id="' + rid + '">'
            rubbleSVG += "<rect x=\"" + str(x) + "\" y=\"" + str(z) + "\" width=\"1\" height=\"1\"></rect>"

            if rubble['total'] >= 1:
                rubbleSVG += '<line x1="' + str(x + 0.2) + '" y1="' + str(z + 0.75) + '" x2="' + str(x+0.8) + '" y2="' + str(z+0.75) + '"></line>'
            if rubble['total'] >= 2:
                rubbleSVG += '<line x1="' + str(x + 0.2) + '" y1="' + str(z + 0.5) + '" x2="' + str(x+0.8) + '" y2="' + str(z+0.5) + '"></line>'
            if rubble['total'] >= 3:
                rubbleSVG += '<line x1="' + str(x + 0.2) + '" y1="' + str(z + 0.25) + '" x2="' + str(x+0.8) + '" y2="' + str(z+0.25) + '"></line>'

            rubbleSVG += '</g>'
            if rubble['start'] != 0 or rubble['end'] != 9999999:
                end = rubble['end'] if rubble['end'] != 9999999 else -1
                rubbleTimings.append([rid, rubble['start'], end, 'gd'])

    rubbleSVG += '</g>'
    return rubbleSVG, rubbleTimings

def generateThreatSVG(threats, xtrans, ztrans):
    if threats is None or len(threats) <= 0:
        return '', []

    styleArmed = "fill:rgb(30, 30, 230);stroke:rgb(190, 190, 190);stroke-width:0.1;"
    styleActivated = "fill:rgb(30, 30, 230);stroke:rgb(190, 190, 190);stroke-width:0.1;"
    styleDisarmed = "fill:rgb(30, 30, 230);stroke:rgb(190, 190, 190);stroke-width:0.1;"
    styleWarningSign = "fill:rgb(255, 165, 0);"
    styleWarningSignStroke = "stroke:rgb(10, 10, 10);stroke-width:0.1;"

    threatSVG = '<g id="svg_threats" style="' + styleWarningSign + styleWarningSignStroke + '">'
    threatTimings = []
    for threat_list in threats.values():
        for threat in threat_list:
            # if rubble['end'] != 9999999:
            #     continue
            x = threat['x'] - xtrans
            z = threat['z'] - ztrans
            tid = 'svg_' + threat['id']
            threatSVG += '<g id="' + tid + '">'

            if threat['state'] == 'armed':
                style = styleArmed
                threatSVG += "<rect x=\"" + str(x) + "\" y=\"" + str(z) + "\" width=\"1\" height=\"1\"></rect>"
                threatSVG += '<line x1="' + str(x + 0.3) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.7) + '" y2="' + str(z+0.2) + '"></line>';
                threatSVG += '<line x1="' + str(x + 0.5) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.5) + '" y2="' + str(z+0.8) + '"></line>';
            elif threat['state'] == 'activated':
                style = styleActivated
                threatSVG += "<rect x=\"" + str(x) + "\" y=\"" + str(z) + "\" width=\"1\" height=\"1\"></rect>"
                threatSVG += '<line x1="' + str(x + 0.3) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.3) + '" y2="' + str(z+0.8) + '"></line>';
                threatSVG += '<line x1="' + str(x + 0.3) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.7) + '" y2="' + str(z+0.2) + '"></line>';
                threatSVG += '<line x1="' + str(x + 0.3) + '" y1="' + str(z + 0.5) + '" x2="' + str(x+0.5) + '" y2="' + str(z+0.5) + '"></line>';
            elif threat['state'] == 'disarmed':
                style = styleDisarmed
                threatSVG += "<rect x=\"" + str(x) + "\" y=\"" + str(z) + "\" width=\"1\" height=\"1\"></rect>"
            elif threat['state'] == 'warning_sign':
                style = styleWarningSign
                threatSVG += "<rect x=\"" + str(x) + "\" y=\"" + str(z) + "\" width=\"1\" height=\"1\"></rect>"
                threatSVG += '<line x1="' + str(x + 0.2) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.3) + '" y2="' + str(z+0.8) + '"></line>';
                threatSVG += '<line x1="' + str(x + 0.3) + '" y1="' + str(z + 0.8) + '" x2="' + str(x+0.5) + '" y2="' + str(z+0.2) + '"></line>';
                threatSVG += '<line x1="' + str(x + 0.5) + '" y1="' + str(z + 0.2) + '" x2="' + str(x+0.7) + '" y2="' + str(z+0.8) + '"></line>';
                threatSVG += '<line x1="' + str(x + 0.7) + '" y1="' + str(z + 0.8) + '" x2="' + str(x+0.8) + '" y2="' + str(z+0.2) + '"></line>';
            threatSVG += '</g>'

            if threat['start'] != 0 or threat['end'] != 9999999:
                end = threat['end'] if threat['end'] != 9999999 else -1
                threatTimings.append([tid, threat['start'], end, 'gd'])

    threatSVG += '</g>'
                           
    return threatSVG, threatTimings

def generatePlayerSVG(player, xtrans, ztrans, expVersion):
    playerSVG = ''
    pathSVG = ''
    
    width = 0.4
    height = 0.4

    player_path_rgb_index = 0
    player_path_rgb_increment = 2
    rgb_range = [10, 180]
    style = "fill:rgb(40, 40, 250);"

    player_name = player['callsign']
    player_id = 'svg_p_' + player_name[0]

    player_color = player_name
    if player_color.lower() == 'red' or player_name.lower() == 'alpha':
        player_id = 'svg_p_R'
        player_color = 'rgba(255,0,0,0.5)'
    elif player_color.lower() == 'green' or player_name.lower() == 'bravo':
        player_id = 'svg_p_G'
        player_color = 'rgba(0,255,0,0.5)'
    elif player_color.lower() == 'blue' or player_name.lower() == 'delta':
        player_id = 'svg_p_B'
        player_color = 'rgba(0,0,255,0.5)'

    # generate the SVG which represents the player
    #playerSVG = '<g id="' + player_id + '"> <g id="' + player_id + '_rot"><polygon id="' + player_id + '_style" fill="black" stroke="' + player_name + '" stroke-width="0.3" points="1.2,0 -0.9,0.9 -0.9,-0.9" /></g></g>'
    playerSVG = '<g id="' + player_id + '"> <g id="' + player_id + '_rot"><circle cx="0" cy="0" r="1" fill="' + player_color + '" stroke="black" stroke-width="0.05"/><polygon id="' + player_id + '_style" fill="black" stroke="black" stroke-width="0.05" points="2.3,0 1,0.8 1,-0.8" /></g></g>'
    playerTimings = []
    pathSVG = '<g id="' + player_id + '_path" style="fill-opacity:0.3;stroke-opacity:0.4;stroke-width:0.2;">'
    p_x = None
    p_z = None
    p_ts = 0
    p_style_ts = [0,1,2,3]
    p_state = 'moving'
    p_role = 'NONE'
    if player_name == 'Red' or player_name == 'Alpha':
        player_path_rgb_index = 0
        player_path_rgb = [rgb_range[1], 0, 0]
    elif player_name == 'Green' or player_name == 'Bravo':
        player_path_rgb_index = 1
        player_path_rgb = [0, rgb_range[1], 0]
    elif player_name == 'Blue' or player_name == 'Delta':
        player_path_rgb_index = 2
        player_path_rgb = [0, 0, rgb_range[1]]

    # generate the path for the player
    for sample in player['locs']:
        ts = sample['ts']
        x = sample['x']
        z = sample['z']
        if expVersion == 4 and x < 0:
            x = x + 36
        x = round(x - xtrans + width/2, 1)
        z = round(z - ztrans + height/2, 1)
        path_id = player_id + '_' + str(ts)

        # Compute the path's color at this point
        player_path_rgb[player_path_rgb_index] += player_path_rgb_increment
        if player_path_rgb[player_path_rgb_index] < rgb_range[0] or player_path_rgb[player_path_rgb_index] > rgb_range[1]:
            player_path_rgb_increment *= -1
            player_path_rgb[player_path_rgb_index] += 2 * player_path_rgb_increment
        style  = "fill:rgb(" + str(player_path_rgb[0]) + ", " + str(player_path_rgb[1]) + ", " + str(player_path_rgb[2]) + ");" + \
            "stroke:rgb(" + str(player_path_rgb[0]) + ", " + str(player_path_rgb[1]) + ", " + str(player_path_rgb[2]) + ");"

        pathSVG += '<g id="' + path_id + '" style="' + style + '">'
        # pathSVG += '<rect x="' + str(x) + '" y="' + str(z) + '" width="' + str(width) + '" height="' + str(height) + '"></rect>'

        x = round(x+width/2, 1)
        z = round(z+height/2, 1)
        if p_x is not None:
            pathSVG += '<line x1="' + str(x) + '" y1="' + str(z) + '" x2="' + str(p_x) + '" y2="' + str(p_z) + '"></line>';
        pathSVG += '</g>'

        # playerTimings.append([path_id, ts, -1, 'd'])
        playerTimings.append([player_id, p_ts, ts, 'pd', '(' + str(x) + ',' + str(z) + ')', '(' + str(int(round(sample['yaw'] + 90,0))) + ')'])
        # playerTimings.append([player_id + '_rot', p_ts, ts, 'transform', 'rotate(' + str(sample['yaw'] + 90) + ')'])
        if sample['state'] != p_state or sample['role'] != p_role:
            fill_style = 'black'
            if sample['role'][0] == "M":    # Medic / First Ait Kit
                fill_style = 'darkviolet'
            elif sample['role'][0] == "H" or sample['role'][0] == "E":  # Engineer / Hammer
                fill_style = 'gainsboro'
            elif sample['role'][0] == "S" or sample['role'][0] == "T":  # Searcher / Stretcher
                fill_style = 'orange'

            p_style_ts[2] = ts
            p_style_ts = [player_id + '_style', ts, -1, 'fill', fill_style]
            playerTimings.append(p_style_ts)
            p_state = sample['state']
            p_role = sample['role']

        # p_ts = timestamp-startTime;
        # p_yaw = data['yaw'] + 90

        p_ts = ts
        p_x = x
        p_z = z

    pathSVG += '</g>'
    return pathSVG, playerSVG, playerTimings

def generateRoomStatusSVG(status_records, xtrans, ztrans):
    finished_style = 'fill:rgb(125,200,125);stroke:rgb(125,200,125);stroke-width:0.1;'
    in_progress_style = 'fill:rgb(215,165,0);stroke:rgb(215,165,0);stroke-width:0.1;'
    incorrectly_marked_style = 'fill:rgb(215,135,135);stroke:rgb(215,135,135);stroke-width:0.1;'

    roomStatusSVG = '<g id="svg_room_status">'
    roomStatusTimings = []
    for room in status_records.values():
        for status_record in room:
            style = ''
            status_id = f'svg_{status_record["id"]}'
            statusSVG = '<g id="' + status_id + '">'

            if status_record['status'] == 'FIN': style = finished_style
            if status_record['status'] == 'IP': style = in_progress_style
            if status_record['status'] == 'IM': style = incorrectly_marked_style
            
            for bound in status_record['room_bounds']:
                x = bound['x1'] - xtrans
                z = bound['z1'] - ztrans
                width = bound['x2'] - bound['x1']
                height = bound['z2'] - bound['z1']

                statusSVG += f'<rect x={x} y={z} width={width} height={height} style={style}></rect>'

            statusSVG += '</g>'

            roomStatusSVG += statusSVG
            roomStatusTimings.append([status_id, status_record['st'], status_record['et'], 'gd'])
    roomStatusSVG += '</g>'

    return roomStatusSVG, roomStatusTimings
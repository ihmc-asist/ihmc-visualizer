ROOMS_CONSIDERED_HALLWAYS = ['sga', 'ca', 'buf']
ROOM_MARKER_TYPES = ['novictim', 'criticalvictim', 'regularvictim']

# Room status constants
DEFAULT = ''
IN_PROGRESS = 'IP'
FINISHED = 'FIN'
INCORRECTLY_MARKED = 'IM'

class RoomStatusHelper:
    
    def __init__(self):
        self.__rooms: dict[str, Room] = {}
        self.__status_records = {} # {'room_id': [{'id': '', 'status': '', 'room_bounds': [], 'st': -1, 'et': -1}]}

        # Used to ignore markers meant for victims not rooms
        self.__victims_placed_outside_rooms = [] # [{'id': '', 'x': 0, 'z': 0, 'is_critical': ''}]

    @property
    def status_records(self):
        return self.__status_records

    def initialize_rooms(self, map_locations):
        for loc in map_locations:
            if loc['type'] == 'room' or loc['type'] == 'bathroom':
                if loc['id'] not in ROOMS_CONSIDERED_HALLWAYS:
                    self.__rooms[loc['id']] = Room(loc['id'], map_locations)
    
    def handle_room_enter(self, room_entered, elapsed_ms):
        if room_entered in self.__rooms.keys():
            if self.__rooms[room_entered].status == DEFAULT:
                self.__rooms[room_entered].set_status(IN_PROGRESS)
                self.add_status_record(room_entered, IN_PROGRESS, elapsed_ms)
    
    def handle_room_leave(self, room_left, all_player_locations, elapsed_ms):
        if room_left[:-1] in self.__rooms.keys() and room_left != 'A4A':
            return

        if room_left in self.__rooms.keys():
            # If all players have left a room
            if all_player_locations.count(room_left) == 0:
                self.handle_potential_room_status_change(room_left, elapsed_ms)
                if f'{room_left}A' in self.__rooms.keys() and room_left != 'A4':
                    self.handle_potential_room_status_change(f'{room_left}A', elapsed_ms)

    def handle_victim_picked_up(self, vic_id, player_loc, elapsed_ms):
        vic_room = self.get_room_victim_is_in(vic_id)
        if vic_room == None:
            self.remove_victim_from_outside(vic_id)
        else:
            self.remove_victim_from_room(vic_id, vic_room)
            if player_loc != vic_room:
                self.handle_potential_room_status_change(vic_room, elapsed_ms)

    def handle_victim_placed(self, vic_id, is_critical, x, z, elapsed_ms):
        placed_loc = self.get_room_from_coord(x, z)
        if placed_loc != None:
            self.add_victim_to_room(vic_id, is_critical, x, z)
        else:
            self.add_victim_to_outside(vic_id, is_critical, x, z, elapsed_ms)

    def handle_marker_placed(self, x, z, type, elapsed_ms):
        type = type.split('_')[1]
        
        for room in self.__rooms:
            marking_bounds = self.__rooms[room].marking_bounds
            for bound in marking_bounds:
                if x in range(bound['x1'],bound['x2']+1) and z in range(bound['z1'],bound['z2']+1):
                    # Check if marker placed removes an existing room marker
                    for marker in self.__rooms[room].markings:
                        if marker['x'] == x and marker['z'] == z:
                            self.__rooms[room].remove_marking(x, z)
                    if not type in ROOM_MARKER_TYPES:
                        self.handle_potential_room_status_change(room, elapsed_ms)
                        return
                    self.add_marking_to_room(room, x, z, type, elapsed_ms)
    
    def add_marking_to_room(self, room, x, z, type, elapsed_ms):
        # Check if marking is meant for outside victim
        for vic in self.__victims_placed_outside_rooms:
            if x in range(vic['x']-1, vic['x']+2) and z in range(vic['z']-1, vic['z']+2):
                if type == 'regularvictim' and not vic['is_critical']: return
                if type == 'criticalvictim' and vic['is_critical']: return

        self.__rooms[room].add_marking(x, z, type)
        self.handle_potential_room_status_change(room, elapsed_ms)
        
    def handle_marker_removed(self, x, z, elapsed_ms):
        for room in self.__rooms:
            if self.__rooms[room].has_marker(x, z):
                self.__rooms[room].remove_marking(x, z)
                self.handle_potential_room_status_change(room, elapsed_ms)

    def handle_potential_room_status_change(self, room, elapsed_ms):
        prev_status = self.__rooms[room].status
        self.__rooms[room].update_status()
        curr_status = self.__rooms[room].status
        if prev_status != curr_status:
            self.add_status_record(room, curr_status, elapsed_ms)

    def add_status_record(self, room_id, status, start_time_ms):
        status_record = {
            'id': '',
            'status': status,
            'room_bounds': self.__rooms[room_id].room_bounds,
            'st': start_time_ms,
            'et': 9999999
        }

        if room_id not in self.__status_records.keys():
            status_record['id'] = f'{room_id}-{0}'
            self.__status_records[room_id] = [status_record]
        else:
            status_record['id'] = f'{room_id}-{len(self.__status_records[room_id])}'
            self.set_status_record_end_time(room_id, start_time_ms)
            self.__status_records[room_id].append(status_record)

    def set_status_record_end_time(self, room_id, end_time_ms):
        if room_id in self.__status_records.keys():
            self.__status_records[room_id][-1]['et'] = end_time_ms

    def add_victim_to_outside(self, vic_id, is_critical, x, z, elapsed_ms):
        self.__victims_placed_outside_rooms.append({
            'id': vic_id,
            'x': x,
            'z': z,
            'is_critical': is_critical
        })
        # check if outside victim is meant for already placed room marker
        for room in self.__rooms:
            for marker in self.__rooms[room].markings:
                if x in range(marker['x']-1, marker['x']+2) and z in range(marker['z']-1, marker['z']+2):
                    if marker['type'] == 'regularvictim' and not is_critical:
                        self.__rooms[room].remove_marking(marker['x'], marker['z'])
                        self.handle_potential_room_status_change(room, elapsed_ms)
                    if marker['type'] == 'criticalvictim' and is_critical:
                        self.__rooms[room].remove_marking(marker['x'], marker['z'])
                        self.handle_potential_room_status_change(room, elapsed_ms)

    def remove_victim_from_outside(self, vic_id):
        for vic in self.__victims_placed_outside_rooms:
            if vic['id'] == vic_id:
                self.__victims_placed_outside_rooms.remove(vic)

    def add_victim_to_room(self, vic_id, is_critical, x, z):
        vic_room = self.get_room_from_coord(x, z)
        print("Adding Victim to Room: " + vic_room)
        self.__rooms[vic_room].add_victim(vic_id, is_critical)
        # Check for connected rooms
        if f'{vic_room}A' in self.__rooms.keys() and vic_room != 'A4':
            self.__rooms[f'{vic_room}A'].add_victim(vic_id, is_critical)
        if vic_room[:-1] in self.__rooms.keys() and vic_room != 'A4A':
            self.__rooms[vic_room[:-1]].add_victim(vic_id, is_critical)
    
    def remove_victim_from_room(self, vic_id, vic_room):
        self.__rooms[vic_room].remove_victim(vic_id)
        # Check for connected rooms
        if f'{vic_room}A' in self.__rooms.keys() and vic_room != 'A4':
            self.__rooms[f'{vic_room}A'].remove_victim(vic_id)
        if vic_room[:-1] in self.__rooms.keys() and vic_room != 'A4A':
            self.__rooms[vic_room[:-1]].remove_victim(vic_id)

    def get_room_from_coord(self, x, z):
        for room in self.__rooms:
            for bound in self.__rooms[room].room_bounds:
                if x in range(bound['x1'],bound['x2']+1) and z in range(bound['z1'],bound['z2']+1):
                    return room
        return None

    def get_room(self, room_id):
        return self.__rooms[room_id]

    def get_room_victim_is_in(self, vic_id):
        for room in self.__rooms:
            if self.__rooms[room].contains_victim(vic_id):
                return room
        return None


class Room:
    def __init__(self, id, map_locations):
        self.__id = id
        self.__status = DEFAULT

        self.__victims_inside = [] # [{'id': '', 'is_critical': ''}]
        self.__markings = [] # [{'x': -1, 'z': -1, 'type': ''}]

        self.__room_bounds = [] # [{'x1': -1, 'z1': -1, 'x2': -1, 'z2': -1}]

        self.__marking_bounds = get_marking_bounds(id) # [{'x1': -1, 'z1': -1, 'x2': -1, 'z2': -1}]

        for loc in map_locations:
            if loc['id'] == self.__id and 'bounds' in loc:
                self.__add_bound(loc['bounds'])
            if loc['id'].split('_')[0] == self.__id and 'room_part' in loc['type']:
                self.__add_bound(loc['bounds'])

    @property
    def status(self):
        return self.__status

    @property
    def room_bounds(self):
        return self.__room_bounds
    @property
    def marking_bounds(self):
        return self.__marking_bounds
    
    @property
    def contains_critical_victims(self):
        for vic in self.__victims_inside:
            if vic['is_critical']:
                return True
        return False

    @property
    def contains_regular_victims(self):
        for vic in self.__victims_inside:
            if not vic['is_critical']:
                return True
        return False

    @property
    def contians_victims(self):
        return len(self.__victims_inside) > 0

    @property
    def is_marked(self):
        return len(self.__markings) > 0
    
    @property
    def victims_inside(self):
        return self.__victims_inside

    @property
    def markings(self):
        return self.__markings

    def update_status(self):
        has_victims = self.contians_victims
        has_regular_victims = self.contians_victims
        has_critical_victim = self.contains_critical_victims

        num_no, num_reg, num_crit = 0, 0, 0
        for marker in self.__markings:
            if marker['type'] == 'novictim': num_no += 1
            if marker['type'] == 'regularvictim': num_reg += 1
            if marker['type'] == 'criticalvictim': num_crit += 1
        
        if not self.is_marked:
            self.__status = IN_PROGRESS
            return

        if not has_victims:
            if num_no > 0 and num_reg == 0 and num_crit == 0:
                self.__status = FINISHED
            else:
                self.__status = INCORRECTLY_MARKED
        if has_regular_victims and not has_critical_victim:
            if num_reg > 0 and num_no == 0 and num_crit == 0:
                self.__status = IN_PROGRESS
            else:
                self.__status = INCORRECTLY_MARKED
        if has_critical_victim:
            if num_crit > 0 and num_no == 0 and num_reg == 0:
                self.__status = IN_PROGRESS
            else:
                self.__status = INCORRECTLY_MARKED
        if has_regular_victims and has_critical_victim:
            if num_reg > 0 and num_crit > 0 and num_no == 0:
                self.__status = IN_PROGRESS

    def add_victim(self, id, is_critical):
        self.__victims_inside.append({
            'id': id,
            'is_critical': is_critical
        })
    
    def contains_victim(self, id):
        for vic in self.__victims_inside:
            if vic['id'] == id:
                return True
        return False
    
    def remove_victim(self, id):
        for vic in self.__victims_inside:
            if vic['id'] == id:
                self.__victims_inside.remove(vic)
    
    def has_marker(self, x, z):
        for marker in self.__markings:
            if marker['x'] == x and marker['z'] == z:
                return True
        return False

    def add_marking(self, x, z, type):
        self.__markings.append({
            'x': x,
            'z': z,
            'type': type
        })
    
    def remove_marking(self, x, z):
        for marker in self.__markings:
            if marker['x'] == x and marker['z'] == z:
                self.__markings.remove(marker)

    def set_status(self, updated_status):
        self.__status = updated_status

    def get_marking_types(self):
        marking_types = []
        for marker in self.__markings:
            marking_types.append(marker['type'])
        return marking_types

    def __add_bound(self, bounds):
        x1 = bounds['coordinates'][0]['x']
        z1 = bounds['coordinates'][0]['z']
        x2 = bounds['coordinates'][1]['x']
        z2 = bounds['coordinates'][1]['z']
        if x1 > x2:
            tmp = x2
            x2 = x1
            x1 = tmp
        if z1 > z2:
            tmp = z2
            z2 = z1
            z1 = tmp

        self.__room_bounds.append({
            'x1': x1,
            'z1': z1,
            'x2': x2,
            'z2': z2
        })

# Manually defined marking bounds for study 3 rooms
def get_marking_bounds(room_id):
    STUDY_3_CUSTOM_ROOM_MARKING_BOUNDS = {
        'A1': [{'x1': -2225, 'z1': 56, 'x2': -2214, 'z2': 60}],
        'A2': [{'x1': -2225, 'z1': 47, 'x2': -2214, 'z2': 55}],
        'A3': [{'x1': -2225, 'z1': 16, 'x2': -2214, 'z2': 46}],
        'A4': [{'x1': -2225, 'z1': 1, 'x2': -2214, 'z2': 14}, {'x1': -2215, 'z1': 2, 'x2': -2208, 'z2': 8}],
        'A4A': [{'x1': -2225, 'z1': -11, 'x2': -2208, 'z2': 0}],
        'B8': [{'x1': -2207,'z1': -2, 'x2': -2197,'z2': 8}],
        'B9': [{'x1': -2197,'z1': -2, 'x2': -2189, 'z2': 5}, {'x1': -2196,'z1': 6, 'x2': -2189, 'z2': 7}],
        'B7': [{'x1': -2210,'z1':  10, 'x2': -2206,'z2': 10}, {'x1': -2210,'z1':  11, 'x2': -2191,'z2': 23}, {'x1': -2191,'z1':  24, 'x2': -2191,'z2': 24}],
        'B5': [{'x1': -2210,'z1': 26, 'x2': -2201,'z2': 34}],
        'B6': [{'x1': -2200,'z1': 26, 'x2': -2191,'z2': 34}],
        'B2': [{'x1': -2212,'z1': 35, 'x2': -2206,'z2': 41}, {'x1': -2205,'z1': 36, 'x2': -2204,'z2': 37}],
        'B3': [{'x1': -2205,'z1': 38, 'x2': -2198,'z2': 42}, {'x1': -2203,'z1': 36, 'x2': -2199,'z2': 37}],
        'B4': [{'x1': -2197,'z1': 35, 'x2': -2191,'z2': 42}],
        'B1': [{'x1': -2212,'z1': 46, 'x2': -2189,'z2': 54}],
        'C8': [{'x1': -2185,'z1': -1, 'x2': -2175,'z2': 5}, {'x1': -2187,'z1': -1, 'x2': -2186,'z2': 7}, {'x1': -2174,'z1': -1, 'x2': -2173,'z2': 3}],
        'C7': [{'x1': -2185,'z1': 7, 'x2': -2175,'z2': 12}, {'x1': -2187,'z1': 8, 'x2': -2186,'z2': 14}],
        'C6': [{'x1': -2185,'z1': 14, 'x2': -2175,'z2': 19}, {'x1': -2187,'z1': 15, 'x2': -2186,'z2': 21}],
        'C5': [{'x1': -2185,'z1': 21, 'x2': -2175,'z2': 26}, {'x1': -2187,'z1': 22, 'x2': -2186,'z2': 28}],
        'C4': [{'x1': -2185,'z1': 28, 'x2': -2175,'z2': 33}, {'x1': -2187,'z1': 29, 'x2': -2186,'z2': 35}],
        'C3': [{'x1': -2185,'z1': 35, 'x2': -2175,'z2': 40}, {'x1': -2187,'z1': 36, 'x2': -2186,'z2': 42}],
        'C2': [{'x1': -2185,'z1': 42, 'x2': -2175,'z2': 47}, {'x1': -2187,'z1': 43, 'x2': -2186,'z2': 49}],
        'C1': [{'x1': -2185,'z1': 49, 'x2': -2175,'z2': 53}, {'x1': -2187,'z1': 50, 'x2': -2186,'z2': 55}],
        'D1': [{'x1': -2164,'z1': 49, 'x2': -2158,'z2': 54}],
        'D2': [{'x1': -2163,'z1': 45, 'x2': -2158,'z2': 48}],
        'D3': [{'x1': -2174,'z1': 34, 'x2': -2169,'z2': 45}],
        'D4': [{'x1': -2168,'z1': 34, 'x2': -2156,'z2': 44}],
        'E1': [{'x1': -2173,'z1': 28, 'x2': -2166,'z2': 31}],
        'E2': [{'x1': -2173,'z1': 22, 'x2': -2166,'z2': 27}],
        'E3': [{'x1': -2173,'z1': 13, 'x2': -2166,'z2': 18}],
        'E4': [{'x1': -2173,'z1': 5, 'x2': -2169,'z2': 10}],
        'E5': [{'x1': -2168,'z1': 5, 'x2': -2165,'z2': 10}],
        'F4': [{'x1': -2164,'z1': 5, 'x2': -2159,'z2': 13}],
        'F3': [{'x1': -2164,'z1': 14, 'x2': -2159,'z2': 18}],
        'F2': [{'x1': -2164,'z1': 22, 'x2': -2159,'z2': 26}],
        'F1': [{'x1': -2164,'z1': 27, 'x2': -2158,'z2': 31}],
        'G1': [{'x1': -2157,'z1': 22, 'x2': -2152,'z2': 27}],
        'G2': [{'x1': -2157,'z1': 13, 'x2': -2152,'z2': 18}],
        'G3': [{'x1': -2157,'z1': 8, 'x2': -2152,'z2': 12}],
        'H2': [{'x1': -2144,'z1': 5, 'x2': -2136,'z2': 36}],
        'H1': [{'x1': -2152,'z1': 38, 'x2': -2134,'z2': 56}],
        'H1A': [{'x1': -2152,'z1': 38, 'x2': -2134,'z2': 56}],
        'I4': [{'x1': -2134,'z1': 10, 'x2': -2121,'z2': 21}],
        'I4A': [{'x1': -2134,'z1': 10, 'x2': -2121,'z2': 21}],
        'I3': [{'x1': -2134,'z1': 22, 'x2': -2121,'z2': 32}],
        'I3A': [{'x1': -2134,'z1': 22, 'x2': -2121,'z2': 32}],
        'I2': [{'x1': -2134,'z1': 33, 'x2': -2121,'z2': 43}],
        'I2A': [{'x1': -2134,'z1': 33, 'x2': -2121,'z2': 43}],
        'I1': [{'x1': -2133,'z1': 44, 'x2': -2121,'z2': 54}],
        'I1A': [{'x1': -2133,'z1': 44, 'x2': -2121,'z2': 54}],
        'J1': [{'x1': -2118,'z1': 44, 'x2': -2111,'z2': 54}],
        'J2': [{'x1': -2118,'z1': 33, 'x2': -2111,'z2': 43}],
        'J3': [{'x1': -2118,'z1': 22, 'x2': -2111,'z2': 32}],
        'J4': [{'x1': -2118,'z1': 10, 'x2': -2111,'z2': 21}],
        'K1': [{'x1': -2110,'z1': 44, 'x2': -2103,'z2': 54}],
        'K2': [{'x1': -2109,'z1': 33, 'x2': -2103,'z2': 43}],
        'K3': [{'x1': -2110,'z1': 22, 'x2': -2103,'z2': 32}],
        'K4': [{'x1': -2110,'z1': 10, 'x2': -2103,'z2': 21}],
        'L1': [{'x1': -2101,'z1': 46, 'x2': -2091,'z2': 54}],
        'L2': [{'x1': -2101,'z1': 28, 'x2': -2091,'z2': 37}],
        'L3': [{'x1': -2101,'z1': 10, 'x2': -2091,'z2': 19}],
        'M1': [{'x1': -2100,'z1': 37, 'x2': -2089,'z2': 46}],
        'M2': [{'x1': -2100,'z1': 19, 'x2': -2089,'z2': 28}],
        'M3': [{'x1': -2100,'z1': 2, 'x2': -2089,'z2': 10}]
    }
    if room_id in STUDY_3_CUSTOM_ROOM_MARKING_BOUNDS.keys():
        return STUDY_3_CUSTOM_ROOM_MARKING_BOUNDS[room_id]
    return [{'x1': 1,'z1': 1, 'x2': -1,'z2': -1}]
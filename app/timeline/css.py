
def generateDefaultTimelineCSS():
    """Generate timeline css for the components on the timeline."""

    css = []

    marker_colors = {
        # Markers ranked from most used to least used.
        "marker_novictim": "rgba(250, 250, 250, 0.7)",  # "white",
        "marker_regularvictim": "rgba(  0, 250,   0, 0.7)",  # "green",
        "marker_criticalvictim": "rgba(255, 255,   0, 0.7)",  # "yellow",
        "marker_abrasion": "rgba(255,   0, 200, 0.7)",  # "pink",
        "marker_bonedamage": "rgba(255, 155,   0, 0.7)",  # "orange",
        "marker_threat": "rgba(125,  75,   0, 0.7)",  # "brown",
        "marker_sos": "rgba(155,   0, 255, 0.7)",  # "purple",
        "marker_rubble": "rgba(128, 128, 128, 0.7)",  # "grey"
    }

    # Adding styles for each marker.
    for name, color in marker_colors.items():
        css.append(
            {
                "name": name + "_placed",
                "z-index": "7",
                "height": "10px",
                "background-color": color,
                "border-width": "1px",
                "border-radius": "4px",
                "border-color": "black",
                "dot-width": "0px",
                "line-width": "0px",
                "font-size": "6pt",
                "font-family": "Arial",
            }
        )
        css.append(
            {
                "name": name + "_removed",
                "z-index": "7",
                "height": "10px",
                "background-color": color,
                "border-width": "1px",
                "border-radius": "4px",
                "border-color": "red",
                "dot-width": "0px",
                "line-width": "0px",
                "font-size": "6pt",
                "font-family": "Arial",
            }
        )

    css.append(
        {
            "name": "def_bomb_inactive",
            "z-index": "8",
            "dot-radius": "5px",
            "dot-width": "5px",
            "dot-color": "rgb(100, 100, 100)",
            "line-width": "1px",
            "line-color": "rgb(50, 50, 50)",
        }
    )
    css.append(
        {
            "name": "def_bomb_triggered",
            "z-index": "8",
            "dot-radius": "3px",
            "dot-width": "3px",
            "dot-color": "rgb(100, 100, 100)",
            "line-width": "1px",
            "line-color": "rgb(50, 50, 50)",
        }
    )
    css.append(
        {
            "name": "def_bomb_cut_green_wire",
            "z-index": "8",
            "dot-radius": "3px",
            "dot-width": "3px",
            "dot-color": "rgb(0, 200, 0)",
            "line-width": "1px",
            "line-color": "rgb(0, 250, 0)",
        }
    )
    css.append(
        {
            "name": "def_bomb_cut_red_wire",
            "z-index": "8",
            "dot-radius": "3px",
            "dot-width": "3px",
            "dot-color": "rgb(200, 0, 0)",
            "line-width": "1px",
            "line-color": "rgb(250, 0, 0)",
        }
    )
    css.append(
        {
            "name": "def_bomb_cut_blue_wire",
            "z-index": "8",
            "dot-radius": "3px",
            "dot-width": "3px",
            "dot-color": "rgb(0, 100, 250)",
            "line-width": "1px",
            "line-color": "rgb(0, 0, 250)",
        }
    )
    css.append(
        {
            "name": "def_bomb_defused",
            "z-index": "7",
            "dot-radius": "6px",
            "dot-width": "6px",
            "dot-color": "rgb(0, 250, 200)",
            "line-width": "2px",
            "line-color": "rgb(50, 50, 50)",
            "border-color": "rgb(50, 50, 50)",
            "border-width": "2px"
        }
    )
    css.append(
        {
            "name": "def_bomb_exploded",
            "z-index": "7",
            "dot-radius": "6px",
            "dot-width": "6px",
            "dot-color": "rgb(250, 80, 0)",
            "line-width": "5px",
            "line-color": "rgb(50, 50, 50)",
        }
    )
    css.append(
        {
            "name": "def_bomb_beacon",
            "z-index": "6",
            "dot-width": "5px",
            "dot-color": "rgb(0, 250, 250)",
        }
    )
    css.append(
        {
            "name": "def_hazard_beacon",
            "z-index": "6",
            "dot-width": "5px",
            "dot-color": "rgb(250, 200, 0)",
        }
    )
    css.append(
        {
            "name": "def_bomb_beacon_removed",
            "z-index": "6",
            "dot-width": "4px",
            "dot-color": "rgb(0, 200, 200)",
        }
    )
    css.append(
        {
            "name": "def_hazard_beacon_removed",
            "z-index": "6",
            "dot-width": "4px",
            "dot-color": "rgb(200, 150, 0)",
        }
    )
    css.append(
        {
            "name": "def_fire_extinguisher",
            "z-index": "6",
            "dot-width": "5px",
            "dot-color": "rgb(250, 0, 250)",
        }
    )

    css.append(
        {
            "name": "def_rubble",
            "z-index": "8",
            "dot-radius": "3px",
            "dot-width": "3px",
            "dot-color": "rgb(100, 100, 100, 0.5)",
            "line-width": "1px",
            "line-color": "rgb(50, 50, 50)",
        }
    )
    css.append(
        {
            "name": "def_triaged-victim",
            "z-index": "6",
            "height": "14px",
            "background-color": "rgba(0, 255, 0, 0.7)",
            # 'border-color':'rgba(0, 255, 0, 0.7)', 'border-width':'1px', 'border-radius':'2px',
            "border-color": "rgba(230, 255, 15, 0.7)",
            "border-width": "2px",
            "border-radius": "2px",
            "dot-radius": "1px",
            "dot-width": "0px",  #'dot-color':'rgba(0, 255, 0, 0.5)',
            "line-width": "0px",  #'line-color':'rgba(0, 255, 0, 0.5)'
        }
    )
    css.append(
        {
            "name": "def_triaged-critical-victim",
            "z-index": "6",
            "height": "14px",
            "background-color": "rgba(255, 0, 0, 0.7)",
            # 'border-color':'rgba(255, 0, 0, 0.7)', 'border-width':'1px', 'border-radius':'2px',
            "border-color": "rgba(230, 255, 15, 0.7)",
            "border-width": "2px",
            "border-radius": "2px",
            "dot-radius": "0px",
            "dot-width": "0px",  #'dot-color':'rgba(255, 0, 0, 0.5)',
            "line-width": "0px",  #'line-color':'rgba(255, 0, 0, 0.5)'
        }
    )
    css.append(
        {
            "name": "def_carrying-victim",
            "z-index": "2",
            "height": "20px",
            "background-color": "rgba(200, 150, 0, 0.55)",
            "border-color": "rgba(0, 0, 0, 0.6)",
            "border-width": "2px 2px 2px 1px",
            "border-radius": "1px",
        }
    )
    css.append(
        {
            "name": "def_carrying-evacuated-successful",
            "z-index": "2",
            "height": "20px",
            "background-color": "rgba(200, 150, 0, 0.55)",
            "border-color": "rgba(0, 255, 0, 0.8)",
            "border-width": "2px 2px 2px 1px",
            "border-radius": "2px",
        }
    )
    css.append(
        {
            "name": "def_carrying-evacuated-unsuccessful",
            "z-index": "2",
            "height": "20px",
            "background-color": "rgba(200, 150, 0, 0.55)",
            "border-color": "rgba(255, 0, 0, 0.8)",
            "border-width": "2px 2px 2px 1px",
            "border-radius": "2px",
        }
    )
    css.append(
        {
            "name": "def_carrying-evacuated-corrected",
            "z-index": "2",
            "height": "20px",
            "background-color": "rgba(200, 150, 0, 0.55)",
            "border-color": "rgba(64, 2, 250, 0.8)",
            "border-width": "2px 2px 2px 1px",
            "border-radius": "2px",
        }
    )
    css.append(
        {
            "name": "def_frozen",
            "z-index": "3",
            "height": "20px",
            "background-color": "rgb(150, 255, 255)",
            "border-color": "rgb(255, 0, 0)",
            "border-width": "1px",
            "border-radius": "0px",
        }
    )
    css.append(
        {
            "name": "def_triaging-success",
            "z-index": "4",
            "height": "17px",
            "background-color": "rgb(140, 185, 15)",
            "border-color": "rgb(140, 185, 15)",
            "border-width": "1px",
            "border-radius": "2px",
        }
    )
    css.append(
        {
            "name": "def_triaging-fail",
            "z-index": "45",
            "height": "17px",
            "background-color": "rgb(140, 185, 15)",
            "border-color": "rgb(255, 0, 0)",
            "border-width": "1px",
            "border-radius": "2px",
        }
    )
    css.append(
        {
            "name": "def_unlocked-victim",
            "z-index": "11",
            "dot-radius": "3px",
            "dot-width": "3px",
            "dot-color": "rgb(255, 0, 0)",
        }
    )
    css.append(
        {
            "name": "def_score",
            "height": "5px",
            "z-index": "1",
            "background-color": "transparent",
            "font-size": "8pt",
            "font-family": "Arial",
            "dot-radius": "0px",
            "dot-width": "1px",
        }
    )
    css.append(
        {
            "name": "def_trapped",
            "z-index": "1",
            "height": "30px",
            "background-color": "rgba(220, 220, 220, 0.5)",
            "border-radius": "0",
            "border-width": "1px",
            "border-color": "rgba(255, 0, 0, 0.85)"
        }
    )

    css.append(
        {
            "name": "def_blank",
            "z-index": "0",
            "height": "1px",
            "background-color": "rgba(255, 255, 255, 0.0)",
            "border-color": "rgba(255, 255, 255, 0.0)",
            "border-width": "0px",
            "border-radius": "0px",
            "dot-radius": "0px",
            "dot-width": "0px",
            "dot-color": "rgba(255, 255, 255, 0.0)",
            "line-width": "0px",
            "line-color": "rgba(255, 255, 255, 0.0)",
        }
    )

    css.append(
        {
            "name": "def_jag_activity",
            "padding-top": "2px",
            "font-size": "8px",
            "z-index": "2",
            "height": "30px",
            "background-color": "rgba(150, 255, 255, 0.5)",
            "border-color": "rgb(0, 0, 0)",
            "border-width": "1px",
        }
    )
    css.append(
        {
            "name": "def_jag_preparing",
            "z-index": "3",
            "height": "10px",
            "background-color": "rgba(200, 150, 0, 0.5)",
            "border-color": "rgb(0, 0, 0)",
            "border-width": "1px",
        }
    )
    css.append(
        {
            "name": "def_jag_addressing",
            "z-index": "4",
            "height": "10px",
            "background-color": "rgba(150, 200, 0, 0.5)",
            "border-color": "rgb(0, 0, 0)",
            "border-width": "1px",
        }
    )

    css.append(
        {
            "name": "def_gt_competence_bad",
            "z-index": "6",
            "height": "10px",
            "background-color": "rgb(255, 255, 0)",
            "border-color": "rgb(255, 0, 0)", "border-width": "2px", "border-radius": "2px",
            "dot-radius": "1px", "dot-width": "0px", "line-width": "0px"
        }
    )
    css.append(
        {
            "name": "def_gt_competence_good",
            "z-index": "6",
            "height": "10px",
            "background-color": "rgb(255, 255, 0)",
            "border-color": "rgb(0, 0, 255)", "border-width": "2px", "border-radius": "2px",
            "dot-radius": "1px", "dot-width": "0px", "line-width": "0px"
        }
    )
    css.append(
        {
            "name": "def_gt_competence_really_good",
            "z-index": "6",
            "height": "10px",
            "background-color": "rgb(255, 255, 0)",
            "border-color": "rgb(0, 150, 0)", "border-width": "2px", "border-radius": "2px",
            "dot-radius": "1px", "dot-width": "0px", "line-width": "0px"
        }
    )

    css.append(
        {
            "name": "def_gt_coordination_bad",
            "z-index": "4",
            "height": "14px",
            "background-color": "rgb(0, 255, 255)",
            "border-color": "rgb(255, 0, 0)", "border-width": "2px", "border-radius": "2px",
            "dot-radius": "1px", "dot-width": "0px", "line-width": "0px"
        }
    )
    css.append(
        {
            "name": "def_gt_coordination_good",
            "z-index": "4",
            "height": "14px",
            "background-color": "rgb(0, 255, 255)",
            "border-color": "rgb(0, 0, 255)", "border-width": "2px", "border-radius": "2px",
            "dot-radius": "1px", "dot-width": "0px", "line-width": "0px"
        }
    )
    css.append(
        {
            "name": "def_gt_coordination_really_good",
            "z-index": "4",
            "height": "14px",
            "background-color": "rgb(0, 255, 255)",
            "border-color": "rgb(0, 150, 0)", "border-width": "2px", "border-radius": "2px",
            "dot-radius": "1px", "dot-width": "0px", "line-width": "0px"
        }
    )

    css.append(
        {
            "name": "def_gt_priority_bad",
            "z-index": "2",
            "height": "18px",
            "background-color": "rgb(150, 0, 255)",
            "border-color": "rgb(255, 0, 0)", "border-width": "2px", "border-radius": "2px",
            "dot-radius": "1px", "dot-width": "0px", "line-width": "0px"
        }
    )
    css.append(
        {
            "name": "def_gt_priority_good",
            "z-index": "2",
            "height": "18px",
            "background-color": "rgb(150, 0, 255)",
            "border-color": "rgb(0, 0, 255)", "border-width": "2px", "border-radius": "2px",
            "dot-radius": "1px", "dot-width": "0px", "line-width": "0px"
        }
    )
    css.append(
        {
            "name": "def_gt_priority_really_good",
            "z-index": "2",
            "height": "18px",
            "background-color": "rgb(150, 0, 255)",
            "border-color": "rgb(0, 150, 0)", "border-width": "2px", "border-radius": "2px",
            "dot-radius": "1px", "dot-width": "0px", "line-width": "0px"
        }
    )


    types = []
    types.append(
        {
            "name": "Teaming",
            "background-color": "rgb(0, 200, 200)",
            "border-color": "rgb(0,0,0)",
            "font-color": "rgb(0,0,0)",
            "z-index": "10",
        }
    )
    types.append(
        {
            "name": "Solo",
            "background-color": "rgb(110, 60, 175)",
            "border-color": "rgb(55,55,55)",
            "font-color": "rgb(255,255,255)",
            "z-index": "10",
        }
    )
    types.append(
        {
            "name": "Lost",
            "background-color": "rgb(200, 200, 0)",
            "border-color": "rgb(0,0,0)",
            "font-color": "rgb(0,0,0)",
            "z-index": "10",
        }
    )

    return {"defaults": css, "added": types}


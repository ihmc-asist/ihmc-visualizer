from datetime import datetime
import math
import os
import json

from scipy.sparse import base
from .SVGHelper import getBounds, BaseMap2SVG, SemanticMap2SVG, generateThreatSVG, generateMarkerBlockSVG, generatePlayerSVG, generateRubbleSVG, generateVictimSVG, generateBombSVG, generateItemSVG, generateRoomStatusSVG
from .SemanticMap import SemanticMap
from .TimelineHelper import generateDefaultTimelineCSS, generateTimelineData
# from .MapSearchTools import MapSearchTools
from .JAGHelper import JointActivityHelper
from .RoomStatusHelper import RoomStatusHelper

LOG_LEVEL = 1

class ASISTDataTools(object):

    LOG_LEVEL_ERROR = 0
    LOG_LEVEL_WARNING = 1
    LOG_LEVEL_INFO = 2
    LOG_LEVEL_DEBUG = 3
    LOG_LEVEL_DEBUG_2 = 4
    LOG_LEVEL_DEBUG_3 = 5

    def __init__(self, maps_folder='.', data_folder='.', log_level=LOG_LEVEL_WARNING):
        self.maps_folder = maps_folder
        self.data_folder = data_folder
        self.trial_infos = []
        LOG_LEVEL = log_level

    @staticmethod
    def set_log_level(level):
        LOG_LEVEL = level

    @staticmethod
    def log(level, msg):
        if level <= LOG_LEVEL:
            print(msg)

    @staticmethod
    def dateStringToTimestamp(dateString):
        timestamp = -1
        try:
            # timestamp = datetime.fromisoformat(dateString.replace("Z", "+00:00")).timestamp()
            # 2021-06-04T22:37:25.918301000Z
            dateTimeSplit = dateString
            if len(dateString) > 26 and dateString.endswith('000Z'):
                dateTimeSplit = dateString.split('000Z')
            elif dateString.endswith('Z'):
                dateTimeSplit = dateString.split('Z')
            if '.' in dateTimeSplit[0]:
                timestamp = datetime.strptime(dateTimeSplit[0], '%Y-%m-%dT%H:%M:%S.%f').timestamp()
            else:
                timestamp = datetime.strptime(dateTimeSplit[0], '%Y-%m-%dT%H:%M:%S').timestamp()
        except Exception as ex:
            print(ex)
            print('Invalid time: ', dateString)

        return timestamp

    @staticmethod
    def generateMsgTimestamp():
        return str(datetime.utcnow().isoformat()) + 'Z'

    def getTrialTopicMessageFromMetadata(self, filename):
        trialData = None
        with open(filename, encoding="utf8") as f:
            for line in f:
                try:
                    data = json.loads(line)
                    if data is not None and 'topic' in data.keys() and \
                    data['topic'].lower() == 'trial' and 'data' in data.keys():
                        trialData = data['data']
                        break
                except Exception as ex:
                    print(ex)
                    print('  - Invalid JSON: "', line, '"')
        return trialData

    def getHighestFileVersion(self, files, filename):
        ext = '.' + filename.split('.')[-1]
        parts = filename.split('_Vers-')
        
        highest = 0
        fn2Return = filename
        for fn in files:
            if fn.startswith(parts[0]) and fn.endswith(ext):
                try:
                    ver = int(fn.split('_Vers-')[1].split('.')[0])
                    if ver > highest:
                        highest = ver
                        fn2Return = fn
                except:
                    fn2Return = fn

        return fn2Return

    def getListOfTrials(self):
        print("Looking for data in : " + self.data_folder)
        processed = []
        self.trial_infos = []
        files = [f for f in os.listdir(self.data_folder) if os.path.isfile(os.path.join(self.data_folder,f))]
        for fn in files:
            if fn.endswith('.metadata'):
                f = self.getHighestFileVersion(files, fn)
                if f in processed:
                    continue
                processed.append(f)
                name = f.split('.metadata')[0]
                print("Processing: " + name)
                trial = {'name': name, 'metadata-filename': os.path.join(self.data_folder, f), 'video-filename': None}

                if f.find('_TrialMessages_') != -1 and f.lower().find('hsrdata_') != -1:
                    formattedText = name.split('_TrialMessages_')
                    if len(formattedText) > 1:
                        # Formated so see if there are other "versions" and only process the highest version number
                        parts = formattedText[1].split('_')
                        #print(parts)
                        if len(parts) > 5:
                            trial['type'] = 'TrialMessages'
                            try:
                                trial['trial-name'] = parts[0].split('-')[1]
                                trial['team'] = parts[1].split('-')[1]
                                trial['condition'] = parts[3].split('-')[1]
                                trial['map'] = parts[4].split('-')[1]
                                trial['version'] = parts[5]

                                # find video filename with the highest version number
                                htn = self.getHighestFileVersion(files, name.replace('_TrialMessages_', '_OBVideo_') + '.mp4')
                                trial['video-filename'] = os.path.join(self.data_folder, htn)
                            except:
                                print("  - Not in standard format!!")


                if trial['video-filename'] is None:
                    trial['video-filename'] = os.path.join(self.data_folder, name + '.mp4')
                trial['video-exists'] = os.path.exists(trial['video-filename']) if 'video-filename' in trial.keys() else False
                print("   video exists: " + str(trial['video-exists']))
                if trial['video-exists']:
                    print("   video-filename: " + trial['video-filename'])

                # Open the file and look for a 'trial' message which can be used to set the actual trial data.
                trial['info'] = self.getTrialTopicMessageFromMetadata(trial['metadata-filename'])

                if trial['info'] is not None:
                    trial['type'] = 'TrialMessages'
                    if 'experiment_name' in trial['info'].keys():
                        trial['trial-name'] = trial['info']['experiment_name']
                    if 'experiment_mission' in trial['info'].keys():
                        trial['team'] = trial['info']['experiment_mission']
                    if 'group_number' in trial['info'].keys():
                        trial['condition'] = trial['info']['group_number']
                    if 'experiment_mission' in trial['info'].keys():
                        trial['map'] = trial['info']['experiment_mission']
                    if ('map_name' in trial['info'].keys() and trial['info']['map_name'].startswith("Dragon_")) or \
                       ('testbed_version' in trial['info'].keys() and \
                        (trial['info']['testbed_version'].startswith("V") or \
                         trial['info']['testbed_version'].startswith("Live") or \
                         trial['info']['testbed_version'].startswith("WGTechSolutions"))):
                        trial['expVersion'] = 4
                    elif 'intervention_agents' in trial['info'].keys():
                        trial['expVersion'] = 3
                    else:
                        trial['expVersion'] = 2

                    if 'map_name' not in trial['info']:
                        if trial['name'].lower().find("training") != -1:
                            trial['info']['map_name'] = 'Saturn_Training_1.4_3D'
                        elif trial['name'].lower().find("competency") != -1:
                            trial['info']['map_name'] = 'Saturn_Competency_Test_1.4_3D'
                        else:
                            trial['info']['map_name'] = 'Saturn_1.4_3D'

                if trial['info'] is not None and 'date' in trial['info'].keys():
                    trial['ts'] = self.dateStringToTimestamp(trial['info']['date'])
                else:
                    trial['ts'] = self.dateStringToTimestamp(self.generateMsgTimestamp())

                self.trial_infos.append(trial)

        self.trial_infos = sorted(self.trial_infos, key=lambda d: d['ts'], reverse=True)
        return self.trial_infos

    @staticmethod
    def genBombInfo(id, type, who, state, x, z, start = 0, end = 9999999):
        bombInfo = {
            'id': str(id),
            'type': type,
            'state': state,
            'start': start,
            'end' : end,
            'who' : who,
            'x' : x,
            'z' : z
        }
        # print("BI: " + id + " " + type + " " + state + " ()" + str(x) + ", " + str(z) + ")")
        return bombInfo

    @staticmethod
    def getClosestBomb(x, z, bombs):
        dist = 99999
        closestBomb = None
        for id in bombs:
            bd = ASISTDataTools.distance(x, z, bombs[id][0]['x'], bombs[id][0]['z'])
            if bd < dist:
                dist = bd
                closestBomb = id
        return closestBomb, dist

    @staticmethod
    def distance (x1, y1, x2, y2):
        return math.hypot(x2-x1, y2-y1)

    @staticmethod
    def genVictimInfo(id, type, x, z, start = 0, end = 9999999, triaged = False, unlocked = True, evacuated = False):
        victim = {'id': str(id), 
                  'start': start,
                  'end': end,
                  'is-critical': False, 
                  'triaged': triaged,
                  'evacuated': evacuated,
                  'unlocked': unlocked,
                  'x': x,
                  'z': z}
        if type == 'block_victim_proximity' or type == 'CRITICAL' or type == 'victim_saved_c' or type == 'victim_c':
            victim['is-critical'] = True
            victim['id'] += ' C'
        elif type == 'block_victim_1b' or type == 'victim_saved_b' or type == 'victim_b':
            victim['id'] += ' B'
        else:
            victim['id'] += ' A'

        return victim

    @staticmethod
    def getPlayerCallsignFromNameOrId(playername, participant_id, client_info):
        callsign = None
        for client in client_info:
            if 'participant_id' in client.keys():
                if participant_id is not None and client['participant_id'] == participant_id:
                    callsign = client['callsign'].lower().strip()
            else:
                if participant_id is not None and client['participantid'] == participant_id:
                    callsign = client['callsign'].lower().strip()
            if callsign is None and playername is not None and 'playername' in client.keys() and client['playername'] == playername:
                callsign = client['callsign'].lower().strip()
        callsign = ASISTDataTools.getCallsignFromMetadataCallsign(callsign)
        if callsign is None:
            return playername
        return callsign

    @staticmethod
    def getCallsignFromMetadataCallsign(callsign):
        if callsign is not None:
            if callsign.lower() == 'red':
                return 'Red'
            if callsign.lower() == 'green':
                return 'Green'
            if callsign.lower() == 'blue':
                return 'Blue'
            if callsign.lower() == 'alpha':
                return 'Alpha'
            if callsign.lower() == 'beta' or callsign.lower() == 'bravo':
                return 'Bravo'
            if callsign.lower() == 'delta':
                return 'Delta'
            if len(callsign) > 0:
                return callsign
        return None


    @staticmethod
    def getPlayerCallsign(data, client_info):
        playername = data['playername'] if 'playername' in data.keys() else None
        participant_id = data['participant_id'] if 'participant_id' in data.keys() else data['triggering_entity'] if 'triggering_entity' in data.keys() else data['owner'] if 'owner' in data.keys() else None
        return ASISTDataTools.getPlayerCallsignFromNameOrId(playername, participant_id, client_info)

    @staticmethod
    def getPlayerColorFromCallsign(callsign):
        if callsign is not None:
            if callsign == 'Red' or callsign == 'Alpha':
                return 'rgba(255, 0, 0, 0.6)'
            if callsign == 'Green' or callsign == 'Bravo':
                return 'rgba(0, 255, 0, 0.6)'
            if callsign == 'Blue' or callsign == 'Delta':
                return 'rgba(0, 0, 255, 0.6)'
        return 'rgba(20, 20, 20, 0.6)'

    @staticmethod
    def getPlayerColor(data, client_info):
        return ASISTDataTools.getPlayerColorFromCallsign(ASISTDataTools.getPlayerCallsign(data, client_info))

    @staticmethod
    def updateBasemap(basemap, x, y, z):
        if basemap is None:
            return basemap

        # remove any block which are at location x,z
        newData = []
        if 'data' in basemap.keys():
            for block in basemap['data']:
                # if block[0][0] == x and block[0][1] == y and block[0][2] == z:
                if block[0][0] == x and block[0][2] == z:
                    continue
                newData.append(block)
            basemap['data'] = newData

        return basemap


    def saveAnnotations(self, trial_info, annotations, updatedGroups):
        if trial_info is None:
            return

        timelineData = {}
        if os.path.exists(trial_info['metadata-filename'] + '.timeline'):
            # load the timings
            with open(trial_info['metadata-filename'] + '.timeline') as f:
                timelineData = json.load(f)

        if 'Annotations' not in timelineData.keys():
            timelineData['Annotations'] = {}
        if isinstance(timelineData['Annotations'], list):
            timelineData['Annotations'] = {'Teamwork': timelineData['Annotations']}

        for group in updatedGroups:
            if group in annotations.keys():
                timelineData['Annotations'][group] = annotations[group]
            elif group in timelineData['Annotations'].keys():
                # if the group is updated but not in annotations, then delete it.
                del timelineData['Annotations'][group]

        f = open(trial_info['metadata-filename'] + ".timeline", "w")
        f.write(json.dumps(timelineData))
        f.close()

        # print(json.dumps(timelineData, indent=4))


    @staticmethod
    def loadAndSortMetadataFile(filename):
        file_data = []
        if os.path.exists(filename):
            with open(filename, encoding="utf8") as f:
                for line in f:
                    data = json.loads(line)
                    if 'error' in data.keys() and 'data' in data['error'].keys():
                        data['data'] = json.loads(data['error']['data'])
                    if "@timestamp" in data.keys() and 'topic' in data.keys() and 'data' in data.keys():
                        timestamp = ASISTDataTools.dateStringToTimestamp(data["@timestamp"])
                        data['ts'] = timestamp
                        file_data.append(data)
            file_data = sorted(file_data, key=lambda d: d['ts'])
        return file_data


    def loadTrial(self, trial_info, generateSVG=True, generateHypothesisData=False, regenerateData=False):
        if trial_info is None:
            return 100, 100, 0, 0, '', [], [], 0, 0
        if 'info' not in trial_info.keys() or trial_info['info'] is None:
            return 100, 100, 0, 0, '', [], [], 0, 0
        if 'map_name' not in trial_info['info'].keys():
            return 100, 100, 0, 0, '', [], [], 0, 0

        width = -1
        height = -1
        xtrans = -1
        ztrans = -1
        svg_map = ''
        timings = []
        timelineData = {}
        lastMsTimeOfTrial = 0
        rubble_index = 0
        version_suffix = ''
        urns = []
        sub_types = []

        hypoDataOutput = 1000
        planning_stop_time = 0

        print("Loading Trail: " + trial_info['name'])

        files = [f for f in os.listdir(self.data_folder) if os.path.isfile(os.path.join(self.data_folder,f))]

        # Load the Semantic Map from the Semantic map file
        semanticmap = None
        filename = os.path.join(self.maps_folder, trial_info['info']['map_name'] + '_sm_v1.0.json')
        if not os.path.exists(filename):
            filename = os.path.join(self.maps_folder, trial_info['info']['map_name'] + '_sm_v2.0.json')
            if not os.path.exists(filename) and trial_info['expVersion'] != 4:
                filename = os.path.join(self.maps_folder, 'Saturn_2.1_3D_sm_v1.0.json')
            else:
                filename = os.path.join(self.maps_folder, 'Dragon_1.0_3D_sm_v2.0.json')

        if os.path.exists(filename):
            print("  - semantic map: " + filename)
            semanticmap = SemanticMap()
            semanticmap.load_semantic_map(filename)

        if semanticmap is None:
            print ("   *** No Semantic map at: " + filename)
            return 100, 100, 0, 0, '', [], [], 0, 0

        # trial_info['mapSearchTools'] = MapSearchTools()
        # trial_info['mapSearchTools'].setSemanticMap(semanticmap)

        timelineFilename = self.getHighestFileVersion(files, trial_info['metadata-filename'] + '.timeline')
        if os.path.exists(timelineFilename):
            # load the timeline items
            with open(timelineFilename) as f:
                timelineData = json.load(f)

        # maptoolsFilename = self.getHighestFileVersion(files, trial_info['metadata-filename'] + '.maptools')
        # if not regenerateData and os.path.exists(maptoolsFilename):
        #     trial_info['mapSearchTools'].load(maptoolsFilename)

        # See if we have already generated this data and saved it.
        timingsFilename = self.getHighestFileVersion(files, trial_info['metadata-filename'] + '.timings')
        if not regenerateData and os.path.exists(timingsFilename):
            print("  - Loading Previously saved Data")
            # load the timings
            with open(timingsFilename) as f:
                timings = json.load(f)

            # load the svg
            svglines = []
            svgFilename = self.getHighestFileVersion(files, trial_info['metadata-filename'] + '.svg')
            with open(svgFilename) as f:
                for line in f:
                    svglines.append(line)

            # parse the svg rect on the 3rd line and get the width and height
            # '<rect x="0" y="0" width="' + str(width) + '" height="' + str(height) + '" style="fill:rgb(255,255,255)"></rect>\n'
            # sizeInfo = json.loads(svglines[2].replace('<rect x="0" y="0" width=', '{"w":').replace(' height=', ', "h":').replace(' style="fill:rgb(255,255,255)"></rect>', '}'))
            sizeInfo = json.loads(
            svglines[2].replace('<rect id="x_', '{"x":"').replace('_z_', '", "z":"').replace(' x="0" y="0" width=', ', "w":').replace(' height=', ', "h":').replace(' style="fill:rgb(255,255,255)"></rect>', '}'))
            xtrans = int(sizeInfo['x'])
            ztrans = int(sizeInfo['z'])
            width = int(sizeInfo['w'])
            height = int(sizeInfo['h'])

            # strip out the svg_map lines from the svg loaded
            for i in range(3, len(svglines)-1):
                svg_map += svglines[i] + '\n'

        if regenerateData or len(timings) <= 0:
            print("  - Generating new Data")
            svg_map = ''
            timings = []

            jah = JointActivityHelper()
            rsh = RoomStatusHelper()

            roleMap = {
                'NONE': {'name': 'None', 'max_level':0.0},
                'Hazardous_Material_Specialist': {'name': 'Rubbler', 'max_level': 40.0},
                'Search_Specialist': {'name': 'Searcher', 'max_level': 20.0},
                'Medical_Specialist': {'name': 'Medic', 'max_level': 30.0},
                'Engineering_Specialist': {'name': 'Rubbler', 'max_level': 100.0},
                'Transport_Specialist': {'name': 'Transporter', 'max_level': 100.0}
            }

            # generate SVG for the Semantic Map
            bounds = getBounds(semanticmap.semantic_map, trial_info['expVersion'])
            xtrans = bounds[0]-2
            ztrans = bounds[1]-2
            width = bounds[2]+4
            height = bounds[3]+4
            rotate = 0
            trans_x = 0
            trans_y = 0
            if generateSVG:
                semanticMapSVG = SemanticMap2SVG(semanticmap.semantic_map, xtrans, ztrans, trial_info['expVersion'])

            # Load the Base Map from the base map file
            basemap = None
            filename = os.path.join(self.maps_folder, trial_info['info']['map_name'] + '_bm_v1.0.json')
            if not os.path.exists(filename):
                filename = os.path.join(self.maps_folder, trial_info['info']['map_name'] + '_bm_v2.0.json')
                if not os.path.exists(filename) and trial_info['expVersion'] != 4:
                    filename = os.path.join(self.maps_folder, 'Saturn_2.1_3D_bm_v1.0.json')
                else:
                    filename = os.path.join(self.maps_folder, 'Dragon_1.0_3D_bm_v2.0.json')
            if os.path.exists(filename):
                print("       base map: " + filename)
                # generate SVG for the base map
                with open(filename) as json_file:
                    basemap = json.load(json_file)

            missionStartTime = -1
            mbidx = 0
            thidx = 0
            vicidx = 0
            step = 1

            jagData = {}
            teamScore = 0
            victims = {}
            bombs = {}
            items = {}
            rubble = {}
            markers = {}
            threats = {}
            players = {}
            playerSVG = []
            pathSVG = []
            twentyMinutesAsMS = 18*60*1000
            timeline = {
                'score': [{'st': 0, 'score': 0}],
                'endTime': -1,
                'unlocked': [],             # {'vid': '', 'st': -1}
                'stageTransition': []       # {'st': -1, 'stage': ''}
            }

            hazard_rooms = {}
            victims_incorrectly_evacuated = []

            PlayerGroupOrder = {"Red": 1, "Green": 2, "Blue": 3}
            
            # Load the metadata messages from the metadata file
            if os.path.exists(trial_info['metadata-filename']):
                print("  metadata file: " + trial_info['metadata-filename'])
                sorted_data = ASISTDataTools.loadAndSortMetadataFile(trial_info['metadata-filename'])

                if (trial_info['expVersion'] == 4):
                    PlayerGroupOrder = {"Alpha": 1, "Bravo": 2, "Delta": 3}
                    lastMsTimeOfTrial = timeline['endTime'] = 10 * 60 * 1000
                    version_suffix = '_v4'

                print("Experiment Type is: " + str(trial_info['expVersion']))

                players['server'] = {
                    "id": 0,
                    "callsign": 'server',
                    "carrying": [], # [{'vid': '', 'st': -1, 'et': -1}]
                    "triaging": [], # [{'vid': '', 'type': '', 'st': -1, 'et': -1}]
                    "rubble": [],   # [{'rid': '', 'st': -1, 'x': -1, 'y': -1}]
                    "trapped": [],  # [{'room': '', 'st': -1, 'et': -1}]
                    "frozen": [],
                    "bombs": {}
                }
                timings.append(['mission_score' + version_suffix, 0, -1, 'st', 0])
                for client in trial_info['info']['client_info']:
                    # print(client)
                    callsign = self.getCallsignFromMetadataCallsign(client['callsign'])
                    if callsign is None:
                        continue
                    gid = len(players)+4
                    if callsign in PlayerGroupOrder.keys():
                        gid = PlayerGroupOrder[callsign]
                    players[callsign] = {"callsign": callsign,
                                        "participant_id": client['participant_id'],
                                        "markerblocklegend": client['markerblocklegend'] if 'markerblocklegend' in client.keys() else None,
                                        "staticmapversion": client['staticmapversion'] if 'staticmapversion' in client.keys() else None,
                                        "last_role": 'NONE',
                                        "last_tool_level": 0,
                                        "last_location": 'UNKNOWN',
                                        "last_state": '',
                                        "locs": [],
                                        "step": step,
                                        "last_x": -9999,
                                        "last_z": -9999,
                                        "last_yaw": -9999,
                                        "color": self.getPlayerColorFromCallsign(callsign),
                                        "debris_cleared": 0,
                                        "victims_moved": 0,
                                        "critical_evacuated": 0,
                                        "non-critical_evacuated": 0,
                                        "evacuation_corrections": 0,
                                        "incorrect_evacuations": 0,
                                        "critical_triaged": 0,
                                        "non-critical_triaged": 0,
                                        "asr_ts": -1,
                                        "id": gid,
                                        "carrying": [], # [{'vid': '', 'st': -1, 'et': -1}]
                                        "triaging": [], # [{'vid': '', 'type': '', 'st': -1, 'et': -1}]
                                        "rubble": [],   # [{'rid': '', 'st': -1, 'x': -1, 'y': -1}]
                                        "trapped": [],  # [{'room': '', 'st': -1, 'et': -1}]
                                        "frozen": [],
                                        "tripped_hazard": [],
                                        "role_changes": [{'role': 'unknown', 'level': 0.0, 'st':0, 'et': twentyMinutesAsMS}],
                                        "jag": {},
                                        "bombs": {},
                                        "beacons": {},
                                        "flags": {},
                                        "is_frozen": "false",
                                        "ppe_equipped": "false",
                                        "health": "20.0"
                                    }
                    timings.append(['m_' + callsign[0] + '_st' + version_suffix, 0, -1, 'st', players[callsign]['last_state']])
                    timings.append(['m_' + callsign[0] + '_sc' + version_suffix, 0, -1, 'st', 0])
                    timings.append(['m_' + callsign[0] + '_rl' + version_suffix, 0, -1, 'st', players[callsign]['last_role']])
                    if trial_info['expVersion'] < 3:
                        timings.append(['m_' + callsign[0] + '_tl' + version_suffix, 0, -1, 'st', players[callsign]['last_tool_level']])
                    timings.append(['m_' + callsign[0] + '_rb' + version_suffix, 0, -1, 'st', players[callsign]['debris_cleared']])
                    timings.append(['m_' + callsign[0] + '_mv' + version_suffix, 0, -1, 'st', players[callsign]['victims_moved']])
                    timings.append(['m_' + callsign[0] + '_nv' + version_suffix, 0, -1, 'st', players[callsign]['non-critical_triaged']])
                    timings.append(['m_' + callsign[0] + '_cv' + version_suffix, 0, -1, 'st', players[callsign]['critical_triaged']])
                    timings.append(['m_' + callsign[0] + '_loc' + version_suffix, 0, -1, 'st', players[callsign]['last_location']])
                    timings.append(['m_' + callsign[0] + '_ce' + version_suffix, 0, -1, 'st', players[callsign]['critical_evacuated']])
                    timings.append(['m_' + callsign[0] + '_ne' + version_suffix, 0, -1, 'st', players[callsign]['non-critical_evacuated']])
                    timings.append(['m_' + callsign[0] + '_ie' + version_suffix, 0, -1, 'st', players[callsign]['incorrect_evacuations']])
                    timings.append(['m_' + callsign[0] + '_ec' + version_suffix, 0, -1, 'st', players[callsign]['evacuation_corrections']])

                    timings.append(['m_' + callsign[0] + '_ppe' + version_suffix, 0, -1, 'st', ""])
                    timings.append(['m_' + callsign[0] + '_hlth' + version_suffix, 0, -1, 'st', players[callsign]['health'].split('.')[0] + "/20"])

                last_elapsed_time = -1
                last_mission_timer = "Mission Timer not initialized."
                finished = False
                for msg in sorted_data:
                    topic = msg['topic']
                    data = msg['data']

                    if trial_info['expVersion'] == 3:
                        jah.handle_message(topic, msg['header'], msg['msg'], data, None)

                    if topic == 'observations/events/player/jag':
                        if 'jag' in data.keys() and 'id' in data['jag'].keys() and 'urn' in data['jag'].keys():
                            jagKey = data['jag']['urn'] + data['jag']['id']
                            if jagKey not in jagData:
                                jagData[jagKey] = []
                            jagData[jagKey].append(data)

                    elif topic == 'observations/events/mission':
                        # print(msg)
                        if data['mission_state'] == 'Stop':
                            finished = True

                            # Mark an end of the mission here
                            lastMsTimeOfTrial = timeline['endTime'] = last_elapsed_time
                            if len(timeline['stageTransition']) > 0:
                                timeline['stageTransition'][-1]['et'] = lastMsTimeOfTrial
                            
                            break
                        elif data['mission_state'] == 'Start':
                            missionStartTime = self.dateStringToTimestamp(msg['header']['timestamp'])
                            rubble = {}
                            victims = {}
                            threats = {}
                            print("Cleared all the mission stuff...");
                    elif topic == 'observations/events/mission/planning':
                        if 'state' in data and data['state'].lower() == 'stop':
                            if 'elapsed_milliseconds' in data:
                                planning_stop_time = data['elapsed_milliseconds']
                            else:
                                planning_stop_time = 123000

                    elif topic == 'observations/events/stage_transition':
                        if 'mission_stage' in data and 'elapsed_milliseconds' in data:
                            ts = data['elapsed_milliseconds']
                            currentStage = data['mission_stage']
                            if len(timeline['stageTransition']) > 0:
                                timeline['stageTransition'][-1]['et'] = ts
                            timeline['stageTransition'].append({'st': ts, 'et': 99999999, 'stage': currentStage})

                    elif topic == 'player/state/change':
                        if 'currAttributes' in data and 'elapsed_milliseconds' in data:
                            callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                            if callsign is not None and callsign in players.keys():
                                player = players[callsign]
                                ts = data['elapsed_milliseconds']
                                if 'is_frozen' in data['currAttributes'] and player['is_frozen'] != data['currAttributes']['is_frozen']:
                                    timings.append(['m_' + callsign[0] + '_st' + version_suffix, ts, -1, 'st', "" if data['currAttributes']['is_frozen'] == 'false' else 'FROZEN'])
                                    player['is_frozen'] = data['currAttributes']['is_frozen']
                                if 'ppe_equipped' in data['currAttributes'] and player['ppe_equipped'] != data['currAttributes']['ppe_equipped']:
                                    timings.append(['m_' + callsign[0] + '_ppe' + version_suffix, ts, -1, 'st', "NONE" if data['currAttributes']['ppe_equipped'] == 'false' else 'EQUIPPED'])
                                    player['ppe_equipped'] = data['currAttributes']['ppe_equipped']
                                if 'health' in data['currAttributes'] and player['health'] != data['currAttributes']['health']:
                                    timings.append(['m_' + callsign[0] + '_hlth' + version_suffix, ts, -1, 'st', data['currAttributes']['health'].split('.')[0] + "/20"])
                                    player['health'] = data['currAttributes']['health']

                    elif topic == 'player/state' and trial_info['expVersion'] >= 4:
                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            player = players[callsign]
                            ts = data['elapsed_milliseconds_global']
                            if ts < 0:
                                if missionStartTime >= 0:
                                    ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                                else:
                                    continue
                            last_elapsed_time = ts

                            if 'mission_timer' in data and data['mission_timer'] != last_mission_timer:
                                last_mission_timer = data['mission_timer']
                                if last_mission_timer == "Mission Timer not initialized.":
                                    timings.append(['mission_timer' + version_suffix, ts, -1, 'st', "?? : ??"])
                                else:
                                    timings.append(['mission_timer' + version_suffix, ts, -1, 'st', last_mission_timer])


                            x = round(data['x'], 1)
                            z = round(data['z'], 1)
                            yaw = data['yaw']
                        
                            if player['last_x'] != x or player['last_z'] != z or player['last_yaw'] != yaw:
                                player['step'] += 1
                                if player['step'] > step:
                                    location = semanticmap.get_locations_containing(x, z, use_updated_map=False)
                                    location = location[0]['name'] if len(location) > 0 else "UNKNOWN"
                                    player['locs'].append({
                                        'ts': ts,
                                        'state': player['last_state'],
                                        'x': x,
                                        'z': z,
                                        'yaw': yaw,
                                        'role': player['last_role']
                                    })
                                    player['step'] = 0
                                    player['last_x'] = x
                                    player['last_z'] = z
                                    player['last_yaw'] = yaw
                                    if location != 'UNKNOWN' and location != player['last_location']:
                                        last_location = player['last_location']
                                        player['last_location'] = location
                                        if len(location) > 30:
                                            location = location[0:29]
                                        timings.append(['m_' + callsign[0] + '_loc' + version_suffix, ts, -1, 'st', location])

                                        rsh.handle_room_enter(location.split(' ')[0], ts)
                                        all_player_locations = []
                                        for player in players.values():
                                            if player['callsign'] == 'server':
                                                continue
                                            all_player_locations.append(player['last_location'].split(' ')[0])
                                        rsh.handle_room_leave(last_location.split(' ')[0], all_player_locations, ts)


                    elif topic == 'observations/state' and trial_info['expVersion'] <= 3:
                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            player = players[callsign]
                            ts = data['elapsed_milliseconds']
                            if ts < 0:
                                if missionStartTime >= 0:
                                    ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                                else:
                                    continue
                            last_elapsed_time = ts

                            x = round(data['x'], 1)
                            z = round(data['z'], 1)
                            yaw = data['yaw']
                        
                            if player['last_x'] != x or player['last_z'] != z or player['last_yaw'] != yaw:
                                player['step'] += 1
                                if player['step'] > step:
                                    location = semanticmap.get_locations_containing(x, z, use_updated_map=False)
                                    location = location[0]['name'] if len(location) > 0 else "UNKNOWN"
                                    player['locs'].append({
                                        'ts': ts,
                                        'state': player['last_state'],
                                        'x': x,
                                        'z': z,
                                        'yaw': yaw,
                                        'role': player['last_role']
                                    })
                                    player['step'] = 0
                                    player['last_x'] = x
                                    player['last_z'] = z
                                    player['last_yaw'] = yaw
                                    if location != 'UNKNOWN' and location != player['last_location']:
                                        last_location = player['last_location']
                                        player['last_location'] = location
                                        if len(location) > 30:
                                            location = location[0:29]
                                        timings.append(['m_' + callsign[0] + '_loc' + version_suffix, ts, -1, 'st', location])

                                        # Account for missed trapped players (happens if a player is entering room right when rubble falls)
                                        if location in hazard_rooms:
                                            for hazard_loc in hazard_rooms:
                                                hazard_room = hazard_rooms[hazard_loc]
                                                if hazard_room['is_blocked'] \
                                                        and last_location not in hazard_room['enclosed_locations'] \
                                                        and location in hazard_room['enclosed_locations'] \
                                                        and callsign not in hazard_room['players_trapped']:
                                                    if (callsign == 'Blue'):
                                                        # If engineer enters blocked threat room consider room unblocked
                                                        hazard_room['is_blocked'] = False
                                                        if len(hazard_room['players_trapped']) > 0:
                                                            # Mark end of player trapped
                                                            for callsign in hazard_room['players_trapped']:
                                                                if len(players[callsign]['trapped']) > 0:
                                                                    players[callsign]['trapped'][-1]['et'] = ts
                                                            hazard_room['players_trapped'] = []
                                                    else:
                                                        # If non-engineer enters blocked threat room consider that player trapped
                                                        hazard_rooms[location]['players_trapped'].append(callsign)
                                                        # Mark the start of player trapped
                                                        player['trapped'].append({'room': location.split(' ')[0], 'st': ts, 'et': twentyMinutesAsMS, 'type': 'threat'})
                                        
                                        # End perturbation trapped status if they exit the triage zone
                                        if 'Zone C' in last_location and player['callsign'] != 'Blue':
                                            if len(players[callsign]['trapped']) > 0:
                                                if players[callsign]['trapped'][-1]['room'] == last_location \
                                                    and players[callsign]['trapped'][-1]['et'] == twentyMinutesAsMS \
                                                    and (location == 'Lobby' or location == 'Limping Lamb Corridor North'):
                                                        players[callsign]['trapped'][-1]['et'] = ts
                                        
                                        rsh.handle_room_enter(location.split(' ')[0], ts)
                                        all_player_locations = []
                                        for player in players.values():
                                            all_player_locations.append(player['last_location'].split(' ')[0])
                                        rsh.handle_room_leave(last_location.split(' ')[0], all_player_locations, ts)


                    elif topic == 'observations/events/player/role_selected':
                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                            last_elapsed_time = ts

                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            player = players[callsign]
                            if ts >= 0:
                                player['locs'].append({
                                    'ts': ts,
                                    'state': player['last_state'],
                                    'x': player['last_x'],
                                    'z': player['last_z'],
                                    'yaw': player['last_yaw'],
                                    'role': data['new_role']
                                })
                            else:
                                ts = 0
                            player['last_role'] = data['new_role']
                            player['last_tool_level'] = 100
                            timings.append(['m_' + callsign[0] + '_rl' + version_suffix, ts, -1, 'st', roleMap[player['last_role']]['name']])
                            if trial_info['expVersion'] < 3:
                                timings.append(['m_' + callsign[0] + '_tl' + version_suffix, ts, -1, 'st', player['last_tool_level']])

                            player['role_changes'][-1]['et'] = ts
                            player['role_changes'].append({'role': roleMap[player['last_role']]['name'], 'level': 100.0, 'st': ts, 'et': twentyMinutesAsMS})

                    elif topic in ['environment/created/single', 'environment/removed/single'] and trial_info['expVersion'] >= 4:
                        ts = data['elapsed_milliseconds']
                        if ts < 0:
                            if missionStartTime >= 0:
                                ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                            else:
                                continue
                        last_elapsed_time = ts

                        if 'obj' in data.keys():
                            itemId = data['obj']['id']
                            if itemId not in items.keys():
                                items[itemId] = []
                            item = {
                                'id': itemId,
                                'ts': ts,
                                'type': data['obj']['type'],
                                'x': data['obj']['x'],
                                'y': data['obj']['y'],
                                'z': data['obj']['z'],
                                'topic': topic,
                            }
                            if 'currAttributes' in data['obj'].keys():
                                if 'bomb_id' in data['obj']['currAttributes'].keys():
                                    item['bomb_id'] = data['obj']['currAttributes']['bomb_id']
                            items[itemId].append(item)

                            if topic == 'environment/removed/single' and item['type'] in ['block_beacon_bomb', 'BOMB_BEACON', 'block_beacon_hazard']:
                                callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                                if callsign is not None and callsign in players.keys():
                                    player = players[callsign]
                                    bombId, dist = ASISTDataTools.getClosestBomb(item['x'], item['z'], bombs)
                                    if bombId not in player['bombs'].keys():
                                        player['bombs'][bombId] = []

                                    player['bombs'][bombId].append({
                                        "id": bombId[4:],
                                        "elapsed_milliseconds": ts,
                                        "state": "INFO_REMOVED",
                                        "tool": item['type']
                                    })


                    elif topic == 'item/state/change' and trial_info['expVersion'] >= 4:
                        ts = data['elapsed_milliseconds']
                        if ts < 0:
                            if missionStartTime >= 0:
                                ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                            else:
                                continue
                        last_elapsed_time = ts

                        itemId = data['item_id']
                        if itemId not in items.keys():
                            items[itemId] = []
                        item = {
                            'id': itemId,
                            'ts': ts,
                            'type': data['item_name'],
                            'topic': topic,
                        }
                        if 'currAttributes' in data.keys():
                            if 'bomb_id' in data['currAttributes'].keys():
                                item['bomb_id'] = data['currAttributes']['bomb_id']
                        items[itemId].append(item)

                        if data['item_name'] == 'block_beacon_hazard' and 'message' in data['currAttributes'].keys():
                            callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                            if callsign is not None and callsign in players.keys():
                                player = players[callsign]

                                foundIt = False
                                # search for a bomb_beacon with the same item_id and update the message value.
                                for bomb in player['bombs']:
                                    for bombInfo in player['bombs'][bomb]:
                                        if 'item_id' in bombInfo.keys() and bombInfo['item_id'] == data['item_id']:
                                            bombInfo['msg'] = data['currAttributes']['message']
                                            # print ("Added msg '" + bombInfo['msg'] + "' to item: " + bombInfo["item_id"] + " associated with bomb: " + bomb)
                                            foundIt = True
                                        if foundIt:
                                            break
                                    if foundIt:
                                        break
                        
                    # elif topic == 'observations/events/player/tool_used' and trial_info['expVersion'] >= 4:
                    elif topic == 'item/used' and trial_info['expVersion'] >= 4:
                        # 'item/used' may have been a better topic to look for...
                        ts = data['elapsed_milliseconds']
                        if ts < 0:
                            if missionStartTime >= 0:
                                ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                            else:
                                continue
                        last_elapsed_time = ts

                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            player = players[callsign]
                            bombId, dist = ASISTDataTools.getClosestBomb(data['target_x'], data['target_z'], bombs)
                            if bombId not in player['bombs'].keys():
                                player['bombs'][bombId] = []

                            if data['item_name'] in ['BOMB_BEACON', 'HAZARD_BEACON'] and data['input_mode'] == 'RIGHT_MOUSE':
                                player['bombs'][bombId].append({
                                    "id": bombId[4:],
                                    "elapsed_milliseconds": ts,
                                    "state": "INFO_ADDED",
                                    "tool": data['item_name'],
                                    "item_id": data['item_id']
                                })

                            elif data['item_name'] in ['WIRECUTTERS_RED', 'WIRECUTTERS_GREEN', 'WIRECUTTERS_BLUE']:
                                player['bombs'][bombId].append({
                                    "id": bombId[4:],
                                    "elapsed_milliseconds": ts,
                                    "state": "TOOL_USED",
                                    "tool": data['item_name'][12:],
                                    "item_id": data['item_id']
                                })
                                # print("item/used: " + json.dumps(data))

                            elif data['item_name'] == 'FIRE_EXTINGUISHER':
                                player['bombs'][bombId].append({
                                    "id": bombId[4:],
                                    "elapsed_milliseconds": ts,
                                    "state": "FIRE_EXTINGUISHER",
                                    "tool": data['item_name'],
                                    "item_id": data['item_id']
                                })

                            # else:
                            #     print("*** Item used: " + data['item_name'] + " ")

                    elif topic == 'observations/events/player/tool_used' and trial_info['expVersion'] <= 3:
                        ts = data['elapsed_milliseconds']
                        if ts < 0:
                            if missionStartTime >= 0:
                                ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                            else:
                                continue
                        last_elapsed_time = ts

                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            player = players[callsign]
                            max_level = roleMap[player['last_role']]['max_level']
                            if data['tool_type'] == 'STRETCHER':
                                continue
                            if data['tool_type'] == 'STRETCHER_OCCUPIED':
                                # Stretcher durability is off by 1 and is only valid on 'Stretcher_Occupied' and not on 'Strecher' tool type values.
                                data['durability'] = data['durability'] + 1
                            else:
                                # Hammer and Med Kit messages are avg 900ms late.
                                ts += 900
                            # print(data)
                            player['last_tool_level'] = round(100.0 * data['durability'] / max_level, 1)
                            player['locs'].append({
                                'ts': ts,
                                'state': player['last_state'],
                                'x': player['last_x'],
                                'z': player['last_z'],
                                'yaw': player['last_yaw'],
                                'role': player['last_role']
                            })
                            if trial_info['expVersion'] < 3:
                                timings.append(['m_' + callsign[0] + '_tl' + version_suffix, ts, -1, 'st', player['last_tool_level']])

                            if player['role_changes'][-1]['level'] != player['last_tool_level']:
                                player['role_changes'][-1]['et'] = ts
                                player['role_changes'].append({'role': roleMap[player['last_role']]['name'], 'level': player['last_tool_level'], 'st': ts, 'et': twentyMinutesAsMS})
                        # print ('player: ' + callsign + '  role: ' + player['last_role'] + ' tool: ' + str(player['last_tool_level']))

                    elif topic == 'environment/created/list' or \
                         topic == 'groundtruth/environment/created/list':
                        if 'list' in data.keys():
                            for item in data['list']:
                                if item['type'] in ['block_bomb_standard', 'block_bomb_fire', 'block_bomb_chained']:
                                    key = item['id']
                                    bombs[key] = [self.genBombInfo(key, 
                                                    item['type'], 
                                                    self.getPlayerCallsignFromNameOrId(item['triggering_entity'], item['triggering_entity'], trial_info['info']['client_info']) if 'triggering_entity' in item.keys() else None, 
                                                    item['currAttributes']['outcome'],
                                                    item['x'],
                                                    item['z'],
                                                    item['elapsed_milliseconds'])]

                    elif topic == 'object/state/change':
                        if 'type' in data and data['type'] in ['block_bomb_standard', 'block_bomb_fire', 'block_bomb_chained']:
                            if data['id'] not in bombs.keys() or bombs[data['id']][-1]['state'] != 'DEFUSED':
                                if data['id'] not in bombs.keys():
                                    bombs[data['id']] = []
                                elif bombs[data['id']][-1]['end'] == 999999:
                                    bombs[data['id']][-1]['end'] = data['elapsed_milliseconds']
                                bombs[data['id']].append(self.genBombInfo(data['id'], 
                                                            data['type'], 
                                                            data['triggering_entity'], 
                                                            data['currAttributes']['outcome'],
                                                            data['x'],
                                                            data['z'],
                                                            data['elapsed_milliseconds']))
                                # Update the bomb list for this player stating what happened.
                                callsign = self.getPlayerCallsignFromNameOrId(data['triggering_entity'].lower(), data['triggering_entity'], trial_info['info']['client_info'])
                                if callsign is None:
                                    callsign = 'server'
                                player = players[callsign]
                                bombId = data['id'][4:]
                                if bombId not in player['bombs'].keys():
                                    player['bombs'][bombId] = []
                                tool = None
                                if 'changedAttributes' in data and 'sequence' in data['changedAttributes']:
                                    tool = data['changedAttributes']['sequence'][0]
                                player['bombs'][bombId].append({
                                    "id": bombId,
                                    "elapsed_milliseconds": data['elapsed_milliseconds'],
                                    "state": data['currAttributes']['outcome'],
                                    "tool": tool
                                })

                    elif topic == 'ground_truth/mission/victims_list':
                        if 'mission_victim_list' in data.keys():
                            for victim in data['mission_victim_list']:
                                vic = self.genVictimInfo(victim['unique_id'], victim['block_type'], victim['x'], victim['z'])
                                if vic['is-critical']:
                                    vic['unlocked'] = False
                                vic['sid'] = 'v_' + str(vicidx)
                                vicidx = vicidx + 1
                                victims[vic['id']] = [vic]
                                basemap = self.updateBasemap(basemap, victim['x'], victim['y'], victim['z'])

                                print(vic)
                                rsh.add_victim_to_room(vic['id'], vic['is-critical'], victim['x'], victim['z'])

                    elif topic == 'ground_truth/semantic_map/initialized':
                        if data['semantic_map'] is not None and trial_info['expVersion'] >= 3:
                            semanticmap.set_semantic_map(data['semantic_map'])
                            rsh.initialize_rooms(data['semantic_map']['locations'])

                    elif topic == 'observations/events/player/victim_evacuated' or \
                         topic == 'observations/events/server/victim_evacuated':

                        if 'success' in data.keys():
                            callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                            # print(str(data['elapsed_milliseconds']) + ',' + str(data['victim_id']) + ',' + data['triage_state'] + ',' + data['playername'])
                            player = {"locs": []}
                            if callsign is not None and callsign in players.keys():
                                player = players[callsign]

                            ts = data['elapsed_milliseconds']
                            if ts < 0 and missionStartTime >= 0:
                                ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                            last_elapsed_time = ts
                            vic = self.genVictimInfo(data['victim_id'], data['type'], data['victim_x'], data['victim_z'], start=ts, triaged=True, evacuated=True)
                            
                            if data['success']:
                                vic['sid'] = 'v_' + str(vicidx)
                                vicidx = vicidx + 1
                                if vic['id'] not in victims.keys():
                                    victims[vic['id']] = [vic]
                                else:
                                    if victims[vic['id']][-1]['end'] == 999999:
                                        victims[vic['id']][-1]['end'] = ts
                                    victims[vic['id']].append(vic)
                                if vic['is-critical']:
                                    player['critical_evacuated'] += 1
                                    timings.append(['m_' + callsign[0] + '_ce' + version_suffix, ts, -1, 'st', player['critical_evacuated']])
                                else:
                                    player['non-critical_evacuated'] += 1
                                    timings.append(['m_' + callsign[0] + '_ne' + version_suffix, ts, -1, 'st', player['non-critical_evacuated']])

                                if vic['id'] in victims_incorrectly_evacuated:
                                    player['evacuation_corrections'] += 1
                                    victims_incorrectly_evacuated.remove(vic['id'])
                                    timings.append(['m_' + callsign[0] + '_ec' + version_suffix, ts, -1, 'st', player['evacuation_corrections']])
                                    
                                if len(player['carrying']) > 0:
                                    players[callsign]['carrying'][-1]['evac_state'] = 'SUCCESSFUL'
                            else:
                                if vic['id'] not in victims_incorrectly_evacuated:
                                    victims_incorrectly_evacuated.append(vic['id'])
                                player['incorrect_evacuations'] += 1
                                timings.append(['m_' + callsign[0] + '_ie' + version_suffix, ts, -1, 'st', player['incorrect_evacuations']])

                                if len(player['carrying']) > 0:
                                    player['carrying'][-1]['evac_state'] = 'UNSUCCESSFUL'

                    elif topic == 'observations/events/player/triage':
                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        # print(str(data['elapsed_milliseconds']) + ',' + str(data['victim_id']) + ',' + data['triage_state'] + ',' + data['playername'])
                        player = {"locs": []}
                        if callsign is not None and callsign in players.keys():
                            player = players[callsign]

                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                        last_elapsed_time = ts
                        vic = self.genVictimInfo(data['victim_id'], data['type'], data['victim_x'], data['victim_z'], start=ts, triaged=True)
                        if data['triage_state'] == 'SUCCESSFUL':
                            player['last_state'] = ''
                            vic['sid'] = 'v_' + str(vicidx)
                            vicidx = vicidx + 1
                            if vic['id'] not in victims.keys():
                                victims[vic['id']] = [vic]
                            else:
                                victims[vic['id']][-1]['end'] = ts
                                victims[vic['id']].append(vic)
                            if vic['is-critical']:
                                player['critical_triaged'] += 1
                                timings.append(['m_' + callsign[0] + '_cv' + version_suffix, ts, -1, 'st', player['critical_triaged']])
                            else:
                                player['non-critical_triaged'] += 1
                                timings.append(['m_' + callsign[0] + '_nv' + version_suffix, ts, -1, 'st', player['non-critical_triaged']])

                            # mark the end of a successful timeline triage
                            # Need to end any other players triaging of this same victim.
                            for key in players:
                                otherPlayer = players[key]
                                if len(otherPlayer['triaging']) > 0 and otherPlayer['triaging'][-1]['vid'] == vic['id']:
                                    otherPlayer['triaging'][-1]['et'] = ts
                                    if key == callsign:
                                        player['triaging'][-1]['triaged'] = True
                                    else:
                                        otherPlayer['last_state'] = ''
                                        timings.append(['m_' + otherPlayer['callsign'][0] + '_st' + version_suffix, ts, -1, 'st', otherPlayer['last_state']])

                            # if there was never a record of this triage starting, update it here.
                            if len(player['triaging']) <= 0 or player['triaging'][-1]['vid'] != vic['id']:
                                st = ts - 3000 if trial_info['expVersion'] > 2 else ts - 15000 if vic['is-critical'] else ts - 7500
                                player['triaging'].append({'vid': vic['id'], 'is-critical': vic['is-critical'], 'st': st, 'et': ts, 'triaged': True})
                                timings.append(['m_' + callsign[0] + '_st', st, -1, 'st', 'triaging'])

                        elif data['triage_state'] == 'IN_PROGRESS':
                            player['last_state'] = 'triaging'

                            et = ts + 3000 if trial_info['expVersion'] > 2 else ts + 15000 if vic['is-critical'] else ts + 7500

                            # mark the start of a timeline triage
                            player['triaging'].append({'vid': vic['id'], 'is-critical': vic['is-critical'], 'st': ts, 'et': et, 'triaged': False})

                        elif data['triage_state'] == 'UNSUCCESSFUL':
                            player['last_state'] = ''

                            # mark the end of an unsuccessful timeline triage
                            player['triaging'][-1]['et'] = ts

                        timings.append(['m_' + callsign[0] + '_st' + version_suffix, ts, -1, 'st', player['last_state']])

                        player['locs'].append({
                            'ts': ts,
                            'state': player['last_state'],
                            'x': player['last_x'],
                            'z': player['last_z'],
                            'yaw': player['last_yaw'],
                            'role': player['last_role']
                        })

                    elif topic == 'observations/events/player/victim_picked_up':
                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                        last_elapsed_time = ts
                        vic = self.genVictimInfo(data['victim_id'], data['type'], data['victim_x'], data['victim_z'], start=ts)
                        if vic['id'] not in victims.keys():
                            victims[vic['id']] = [vic]
                        else:
                            victims[vic['id']][-1]['end'] = ts
                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            players[callsign]['victims_moved'] += 1
                            players[callsign]['last_state'] = 'carrying'
                            timings.append(['m_' + callsign[0] + '_mv' + version_suffix, ts, -1, 'st', players[callsign]['victims_moved']])
                            timings.append(['m_' + callsign[0] + '_st' + version_suffix, ts, -1, 'st', players[callsign]['last_state']])

                            # mark the start of a victim carry
                            is_correcting = vic['id'] in victims_incorrectly_evacuated
                            players[callsign]['carrying'].append({'vid': vic['id'], 'st': ts, 'et': twentyMinutesAsMS, \
                                    'is-critical': vic['is-critical'], 'evac_state':'IN_PROGRESS', 'is-correcting': is_correcting})

                        player_loc = players[callsign]['last_location'].split(' ')[0]
                        rsh.handle_victim_picked_up(vic['id'], player_loc, ts)

                    elif topic == 'observations/events/player/victim_placed':
                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                        last_elapsed_time = ts
                        vic = self.genVictimInfo(data['victim_id'], data['type'], data['victim_x'], data['victim_z'], start=ts)
                        vic['sid'] = 'v_' + str(vicidx)
                        vicidx = vicidx + 1
                        if vic['id'] not in victims.keys():
                            victims[vic['id']] = [vic]
                        else:
                            if victims[vic['id']][-1]['end'] == 999999:
                                victims[vic['id']][-1]['end'] = ts 
                            vic['triaged'] = victims[vic['id']][-1]['triaged']
                            vic['unlocked'] = victims[vic['id']][-1]['unlocked']
                            vic['evacuated'] = victims[vic['id']][-1]['evacuated']
                            victims[vic['id']].append(vic)
                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            players[callsign]['last_state'] = ''
                            timings.append(['m_' + callsign[0] + '_st' + version_suffix, ts, -1, 'st', players[callsign]['last_state']])

                            # mark the end of a victim carry
                            if len(players[callsign]['carrying']) > 0:
                                players[callsign]['carrying'][-1]['et'] = ts
                        
                        rsh.handle_victim_placed(vic['id'], vic['is-critical'], data['victim_x'], data['victim_z'], ts)

                    elif topic == 'observations/events/player/proximity_victim' or \
                         topic == 'observations/events/player/proximity_block':
                        if data['victim_id'] != -1 and ((trial_info['expVersion'] == 2 and data['players_in_range'] >= 3) or \
                            (trial_info['expVersion'] == 3 and ('awake' in data and data['awake']) or ('awake' not in data and data['players_in_range'] >= 2))):
                            ts = data['elapsed_milliseconds']
                            if ts < 0 and missionStartTime >= 0:
                                ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                            last_elapsed_time = ts
                            vic = self.genVictimInfo(data['victim_id'], 'CRITICAL', data['victim_x'], data['victim_z'], start=ts)
                            vic['sid'] = 'v_' + str(vicidx)
                            vicidx = vicidx + 1
                            if vic['id'] not in victims.keys():
                                victims[vic['id']] = [vic]
                            else:
                                victims[vic['id']][-1]['end'] = ts
                                victims[vic['id']].append(vic)

                            # mark the time a Critical victim is unlocked
                            timeline['unlocked'].append({'vid': vic['id'], 'st': ts})

                    elif topic == 'ground_truth/mission/blockages_list':
                        if 'mission_blockage_list' in data.keys():
                            for blockage in data['mission_blockage_list']:
                                id = str(blockage['x']) + '_' + str(blockage['z'])
                                if id not in rubble.keys():
                                    rubble[id] = [{'id': 'rb' + str(rubble_index),
                                                'start': 0,
                                                'end': 9999999,
                                                'x': blockage['x'],
                                                'z': blockage['z'],
                                                'total': 0}]
                                    rubble_index = rubble_index + 1
                                    basemap = self.updateBasemap(basemap, blockage['x'], blockage['y'], blockage['z'])
                                rubble[id][0]['total'] += 1
                    elif topic == 'observations/events/mission/perturbation':
                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                        last_elapsed_time = ts
                        if 'perturbation' not in timeline:
                            timeline['perturbation'] = []
                        if 'type' in data:
                            if data['type'] == 'blackout':
                                if data['mission_state'].lower() == 'start':
                                    timeline['perturbation'].append(
                                        {'start':ts, 'end': 9999999, 'type': 'blackout'}
                                    )
                                elif len(timeline['perturbation']) > 0:
                                    timeline['perturbation'][-1]['end'] = ts
                            elif data['type'] == 'rubble':
                                if data['mission_state'].lower() == 'start':
                                    timeline['perturbation'].append(
                                        {'start':ts, 'end': 9999999, 'type': 'rubble'}
                                    )

                    elif topic == 'observations/events/mission/perturbation_rubble_locations':
                        if 'elapsed_milliseconds' in data:
                            ts = data['elapsed_milliseconds']
                            last_elapsed_time = ts
                        elif ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                            last_elapsed_time = ts
                        if 'mission_blockage_list' in data.keys():
                            # TODO:: possible issue if marker block or victim has been placed at any of these locations.
                            for blockage in data['mission_blockage_list']:
                                id = str(blockage['x']) + '_' + str(blockage['z'])
                                if id not in rubble.keys():
                                    rubble[id] = [{'id': 'rb' + str(rubble_index),
                                                'start': ts,
                                                'end': 9999999,
                                                'x': blockage['x'],
                                                'z': blockage['z'],
                                                'total': 0}]
                                    rubble_index = rubble_index + 1
                                    basemap = self.updateBasemap(basemap, blockage['x'], blockage['y'], blockage['z'])
                                if rubble[id][0]['total'] < 3:
                                    rubble[id][0]['total'] += 1
                            
                            # check if players get trapped in critical zones
                            for player in players.values():
                                location = player['last_location']
                                if 'Zone C' in location and player['callsign'] != 'Blue':
                                    player['trapped'].append({'room': location,'st': ts, 'et': twentyMinutesAsMS, 'type': 'perturbation'})
                    
                    elif topic == 'observations/events/player/rubble_collapse' or \
                         topic == 'observations/events/server/rubble_collapse':
                        # TODO:: possible issue if marker block or victim has been placed at any of these locations.
                        # print("Handling topic: " + topic)
                        # print(json.dumps(data))

                        try:
                            ts = data['elapsed_milliseconds']
                            if ts < 0 and missionStartTime >= 0:
                                ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                            last_elapsed_time = ts

                            # update the threat trigger
                            id = str(data['triggerLocation_x']) + '_' + str(data['triggerLocation_z'])
                            if id in threats.keys():
                                threats[id][-1]['end'] = ts
                                threats[id].append({
                                    'id': 'th_' + str(thidx),
                                    'start': ts,
                                    'end': ts+40000,
                                    'x': data['triggerLocation_x'],
                                    'z': data['triggerLocation_z'],
                                    'state': 'activated'
                                })
                                thidx = thidx + 1
                                threats[id].append({
                                    'id': 'th_' + str(thidx),
                                    'start': ts+40000,
                                    'end': 9999999,
                                    'x': data['triggerLocation_x'],
                                    'z': data['triggerLocation_z'],
                                    'state': 'armed'
                                })
                                thidx = thidx + 1

                            # update the rubble which collapsed.
                            fromX = data['fromBlock_x']
                            toX = data['toBlock_x'] + 1
                            fromZ = data['fromBlock_z']
                            toZ = data['toBlock_z'] + 1
                            fromY = data['fromBlock_y']
                            toY = data['toBlock_y'] + 1
                            # correcting inconsistent rubble collapse data
                            if(fromX >= toX):
                                fromX = data['toBlock_x']
                                toX = data['fromBlock_x'] + 1
                            if(fromZ >= toZ):
                                fromZ = data['toBlock_z']
                                toZ = data['fromBlock_z'] + 1
                            if(fromY >= toY):
                                fromY = data['toBlock_y']
                                toY = data['fromBlock_y'] + 1
                            maxTotal = toY - fromY
                            if maxTotal > 3:
                                maxTotal = 3
                            # print("  maxTotal = " + str(maxTotal) + " fromY:" + str(fromY) + " toY:" + str(toY))
                            handledIDs = []
                            for x in range(fromX, toX):
                                for z in range(fromZ, toZ):
                                    id = str(x) + '_' + str(z)
                                    if id in rubble.keys():
                                        if rubble[id][-1]['end'] == 9999999 and rubble[id][-1]['total'] >= maxTotal:
                                            continue
                                        if rubble[id][-1]['end'] == 9999999:
                                            rubble[id][-1]['end'] = ts
                                    else:
                                        rubble[id] = []
                                        for y in range(fromY, toY):
                                            basemap = self.updateBasemap(basemap, x, y, z)

                                    rubble[id].append({
                                        'id': 'rb' + str(rubble_index),  #rubble[id][0]['id'],
                                        'start': ts,
                                        'end': 9999999,
                                        'x': x,
                                        'z': z,
                                        'total': maxTotal
                                    })
                                    rubble_index = rubble_index + 1
                                    # print("Rubble_Collapse at [" + str(ts) + "] added rubble at " + id + "  total:" + str(rubble[id][-1]['total']))
                            
                            # update hazard room blockages and check if players are trapped
                            callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                            if callsign is not None and callsign in players.keys():
                                location = semanticmap.get_locations_containing(data['triggerLocation_x'], data['triggerLocation_z'], use_updated_map=False)
                                location = location[0]['name'] if len(location) > 0 else "UNKNOWN"
                                
                                player_is_trapped = False
                                blockage = {'from_x': fromX, 'to_x': toX, 'from_z': fromZ, 'to_z': toZ}
                                current_blockages = hazard_rooms[location]['exit_blockages']
                                if blockage not in current_blockages:
                                    hazard_rooms[location]['exit_blockages'].append(blockage)
                                    if(hazard_rooms[location]['num_triggers'] == len(current_blockages)):
                                        if players['Blue']['last_location'] not in hazard_rooms[location]['enclosed_locations']:
                                            hazard_rooms[location]['is_blocked'] = True
                                            player_is_trapped = True
                                        else:
                                            hazard_rooms[location]['is_blocked'] = False

                                if player_is_trapped:
                                    for player in players.values():
                                        if player['last_location'] in hazard_rooms[location]['enclosed_locations'] and player['callsign'] != 'Blue':
                                            hazard_rooms[location]['players_trapped'].append(player['callsign'])
                                            # mark the start of a player trapped
                                            player['trapped'].append({'room': player['last_location'].split(' ')[0],'st': ts, 'et': twentyMinutesAsMS, 'type': 'threat'})
                        except:
                            print("Invalid rubble_collapse data!!")
                    elif topic == 'observations/events/player/rubble_destroyed':
                        id = str(data['rubble_x']) + '_' + str(data['rubble_z'])
                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                        last_elapsed_time = ts

                        if id in rubble.keys():
                            if rubble[id][-1]['end'] == 9999999:
                                rubble[id][-1]['end'] = ts
                            total = rubble[id][-1]['total'] - 1
                            if total > 0:
                                rubble[id].append({
                                    'id': 'rb' + str(rubble_index),  #rubble[id][0]['id'],
                                    'start': ts,
                                    'end': 9999999,
                                    'x': data['rubble_x'],
                                    'z': data['rubble_z'],
                                    'total': total
                                })
                                rubble_index = rubble_index + 1
                            callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                            if callsign is not None and callsign in players.keys():
                                players[callsign]['debris_cleared'] += 1
                                timings.append(['m_' + callsign[0] + '_rb' + version_suffix, ts, -1, 'st', players[callsign]['debris_cleared']])

                                # mark the time rubble is destroyed
                                players[callsign]['rubble'].append({'rid': id, 'st': ts})

                        # check if rubble destroyed was a hazard room blockage and update hazard room's blockages and trapped players if so
                        for hazard_loc in hazard_rooms:
                            hazard_room = hazard_rooms[hazard_loc]
                            for blockage in hazard_room['exit_blockages']:
                                if data['rubble_x'] in range(blockage['from_x'], blockage['to_x']) \
                                        and data['rubble_z'] in range(blockage['from_z'], blockage['to_z']):
                                    hazard_room['is_blocked'] = False
                                    hazard_room['exit_blockages'].remove(blockage)
                                    if len(hazard_room['players_trapped']) > 0:
                                        # Mark end of player trapped
                                        for callsign in hazard_room['players_trapped']:
                                            if len(players[callsign]['trapped']) > 0 and players[callsign]['trapped'][-1]['et'] == twentyMinutesAsMS:
                                                players[callsign]['trapped'][-1]['et'] = ts
                                        hazard_room['players_trapped'] = []
                    elif topic == 'observations/events/player/location':
                        # check if players are still trapped
                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        if callsign is not None and callsign != "Blue" and callsign in players.keys():
                            locations = []
                            if 'locations' in data:
                                for loc in data['locations']:
                                    locations.append(loc['name'])
                            if len(locations) > 0:
                                # see if this player is in any of the blocked locations
                                for location in hazard_rooms:
                                    if callsign in hazard_rooms[location]['players_trapped'] and location not in locations:
                                        if len(players[callsign]['trapped']) > 0 and players[callsign]['trapped'][-1]['et'] == twentyMinutesAsMS:
                                            # make sure the player did not enter another room enclosed by the rubble collapse
                                            if not any(loc in locations for loc in hazard_rooms[location]['enclosed_locations']):
                                                players[callsign]['trapped'][-1]['et'] = data['elapsed_milliseconds']

                    elif topic == 'ground_truth/mission/freezeblock_list':
                        if 'mission_freezeblock_list' in data.keys():
                            for block in data['mission_freezeblock_list']:
                                id = str(block['x']) + '_' + str(block['z'])
                                threats[id] = [{'id': 'th_' + str(thidx),
                                            'start': 0,
                                            'end': 9999999,
                                            'x': block['x'],
                                            'z': block['z'],
                                            'state': 'armed'}]
                                thidx = thidx + 1
                    elif topic == 'observations/events/player/freeze':
                        id = str(data['player_x']) + '_' + str(data['player_z'])
                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            player = players[callsign]
                            ts = data['elapsed_milliseconds']
                            if ts < 0 and missionStartTime >= 0:
                                ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                            last_elapsed_time = ts
                            if data['state_changed_to'] == 'FROZEN':
                                player['last_state'] = 'frozen'
                                if id in threats.keys():
                                    threats[id][-1]['end'] = ts
                                    threats[id].append({
                                        'id': 'th_' + str(thidx),
                                        'start': ts,
                                        'end': 9999999,
                                        'x': data['player_x'],
                                        'z': data['player_z'],
                                        'state': 'activated'
                                    })
                                    thidx = thidx + 1
                                # mark when a player is fozen
                                player['frozen'].append({'st': ts, 'et': twentyMinutesAsMS})
                            else:
                                player['last_state'] = ''
                                if id in threats.keys():
                                    threats[id][-1]['end'] = ts
                                    threats[id].append({
                                        'id': 'th_' + str(thidx),
                                        'start': ts,
                                        'end': 9999999,
                                        'x': data['player_x'],
                                        'z': data['player_z'],
                                        'state': 'disarmed'
                                    })
                                    thidx = thidx + 1
                                # mark when a player is unfozen
                                player['frozen'][-1]['et'] = ts

                            timings.append(['m_' + callsign[0] + '_st' + version_suffix, ts, -1, 'st', player['last_state']])

                        player['locs'].append({
                            'ts': ts,
                            'state': player['last_state'],
                            'x': player['last_x'],
                            'z': player['last_z'],
                            'yaw': player['last_yaw'],
                            'role': player['last_role']
                        })

                    elif topic == 'ground_truth/mission/threatsign_list':
                        if 'mission_threatsign_list' in data.keys():
                            for block in data['mission_threatsign_list']:
                                id = str(block['x']) + '_' + str(block['z'])
                                threat_type = 'armed'
                                if trial_info['expVersion'] == 2:
                                    threat_type = 'warning_sign'
                                threats[id] = [{'id': 'th_' + str(thidx),
                                            'start': 0,
                                            'end': 9999999,
                                            'x': block['x'],
                                            'z': block['z'],
                                            'state': threat_type}]
                                thidx = thidx + 1

                                # Add known triggers and hazard rooms to hazard_rooms
                                trigger_location = semanticmap.get_locations_containing(block['x'], block['z'], use_updated_map=False)
                                trigger_location = trigger_location[0]['name'] if len(trigger_location) > 0 else "UNKNOWN"
                                if (trigger_location == 'Sundollars Coffee'):
                                    print(semanticmap.get_locations_containing(block["x"], block["z"], use_updated_map=True))
                                if trigger_location not in hazard_rooms:
                                    hazard_rooms[trigger_location] = {
                                        'num_triggers': 1,
                                        'exit_blockages': [],
                                        'players_trapped': [],
                                        'enclosed_locations': [trigger_location],
                                        'is_blocked': False
                                    }
                                else:
                                    hazard_rooms[trigger_location]['num_triggers'] += 1
                            # Manually Accounting For Grouped Rooms
                            if 'I1 Storage A' in hazard_rooms:  hazard_rooms['I1 Storage A']['enclosed_locations'].append('I1A Storage B')
                            if 'I2 Storage C' in hazard_rooms:  hazard_rooms['I2 Storage C']['enclosed_locations'].append('I2A Storage D')
                            if 'I3A Storage F' in hazard_rooms: hazard_rooms['I3A Storage F']['enclosed_locations'].append('I3 Storage E')
                            if 'I4 Storage G' in hazard_rooms:  hazard_rooms['I4 Storage G']['enclosed_locations'].append('I4A Storage H')
                            if 'A4 King Chris\'s Office' in hazard_rooms: hazard_rooms['A4 King Chris\'s Office']['enclosed_locations'].append('A4A The King\'s Terrace')

                    elif topic == 'observations/events/player/marker_placed':
                        id = str(data['marker_x']) + '_' + str(data['marker_z'])
                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                        last_elapsed_time = ts
                        if id not in markers.keys():
                            markers[id] = []
                        elif markers[id][-1]['end'] == 9999999:
                            markers[id][-1]['end'] = ts
                        callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])

                        #Need to update to handle new marker blocks
                        block_type = data['type']
                        type = -1
                        if trial_info['expVersion'] == 2:
                            type = data['type'][-1]
                            if type == 'Marker Block 1' or type == 'Marker Block 2' or type == 'Marker Block 3':
                                type = int(data['type'][-1])
                            else:
                                type = 1
                        markers[id].append({
                            'id': 'm'+str(mbidx),
                            'start': ts,
                            'end': 9999999,
                            'x': data['marker_x'],
                            'z': data['marker_z'],
                            'number': type,
                            'block_type': block_type,
                            'callsign': callsign,
                            'color': self.getPlayerColorFromCallsign(callsign)
                        })
                        mbidx = mbidx + 1

                        rsh.handle_marker_placed(data['marker_x'], data['marker_z'], data['type'], ts)

                    elif topic == 'observations/events/player/marker_removed':
                        id = str(data['marker_x']) + '_' + str(data['marker_z'])
                        ts = data['elapsed_milliseconds']
                        if ts < 0 and missionStartTime >= 0:
                            ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                        last_elapsed_time = ts
                        if id in markers.keys() and markers[id][-1]['end'] == 9999999:
                            markers[id][-1]['end'] = ts

                        rsh.handle_marker_removed(data['marker_x'], data['marker_z'], ts)

                    elif topic == 'score/change':
                        # print("SCORE_CHANGE!!" + str(data))
                        # "elapsed_milliseconds_global":
                        # "elapsed_milliseconds_global":
                        if 'teamScore' in data.keys() and 'elapsed_milliseconds' in data.keys():
                            if teamScore != data['teamScore']:
                                teamScore = data['teamScore']
                                ts = data['elapsed_milliseconds']
                                if ts < 0 and missionStartTime >= 0:
                                    ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                                last_elapsed_time = ts

                                #mark when the score is updated.
                                timings.append(['mission_score' + version_suffix, ts, -1, 'st', teamScore])
                                timeline['score'].append({'score': teamScore, 'st': ts})
                                if 'playerScores' in data.keys():
                                    for playername in data['playerScores']:
                                        score = data['playerScores'][playername]
                                        callsign = self.getPlayerCallsignFromNameOrId(playername, playername, trial_info['info']['client_info'])
                                        if callsign is not None and callsign in players.keys():
                                            player = players[callsign]
                                            timings.append(['m_' + callsign[0] + '_sc' + version_suffix, ts, -1, 'st', score])


                    elif topic == 'observations/events/scoreboard':
                        if 'scoreboard' in data.keys() and 'TeamScore' in data['scoreboard'].keys():
                            teamScore = data['scoreboard']['TeamScore']
                            ts = data['elapsed_milliseconds']
                            if ts < 0 and missionStartTime >= 0:
                                ts = int(1000 * (self.dateStringToTimestamp(msg['header']['timestamp']) - missionStartTime))
                            last_elapsed_time = ts
                            for playername in data['scoreboard']:
                                score = data['scoreboard'][playername]
                                callsign = self.getPlayerCallsignFromNameOrId(playername, playername, trial_info['info']['client_info'])
                                if callsign is not None and callsign in players.keys():
                                    player = players[callsign]
                                    timings.append(['mission_score' + version_suffix, ts, -1, 'st', teamScore])
                                    timings.append(['m_' + callsign[0] + '_sc' + version_suffix, ts, -1, 'st', score])

                                    #mark when the score is updated.
                                    timeline['score'].append({'score': teamScore, 'st': ts})

                    elif topic == 'agent/asr/intermediate':
                        if missionStartTime < 0 or trial_info['expVersion'] != 2:
                            continue
                        # get timestamp and playername/participant_id
                        participant_id = data['participant_id']
                        callsign = self.getPlayerCallsignFromNameOrId(participant_id, participant_id, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            if players[callsign]['asr_ts'] < 0:
                                ts = self.dateStringToTimestamp(msg['header']['timestamp'])
                                players[callsign]['asr_ts'] = ts - missionStartTime

                    elif topic == 'agent/asr/final':
                        if missionStartTime < 0:
                            continue
                        participant_id = data['participant_id']
                        callsign = self.getPlayerCallsignFromNameOrId(participant_id, participant_id, trial_info['info']['client_info'])
                        if callsign is not None and callsign in players.keys():
                            ts = players[callsign]['asr_ts']
                            if ts < 0:
                                if 'start_timestamp' in data:
                                    ts = self.dateStringToTimestamp(data['start_timestamp'])
                                else:
                                    ts = self.dateStringToTimestamp(msg['header']['timestamp'])
                                ts = ts - missionStartTime
                            # last_elapsed_time = ts
                            timings.append(['asr', int(ts*1000), -1, 'at', callsign, data['text'].strip()])
                            # players[callsign]['asr'].append([int(round(ts*1000,0)), callsign, data['text'].strip()])
                            players[callsign]['asr_ts'] = -1

                    elif topic.startswith("agent/intervention/") and topic.endswith("/chat"):
                        source = topic.split("/")[2]
                        ts = last_elapsed_time
                        receivers = []
                        for rec in data['receivers']:
                            receivers.append(self.getPlayerCallsignFromNameOrId(rec, rec, trial_info['info']['client_info']))

                        timings.append(['asr', int(ts), -1, 'at', "Int from " + source + " for " + str(receivers), data['content'].strip()])

                    elif topic == 'minecraft/chat':
                        source = data['sender']
                        ts = last_elapsed_time
                        addressees = []
                        for add in data['addressees']:
                            addressees.append(self.getPlayerCallsignFromNameOrId(add, add, trial_info['info']['client_info']))
                        chatMsg = ""
                        try:
                            chatMsg = json.loads(data['text'].strip())
                            timings.append(['asr', int(ts), -1, 'at', "Chat For " + str(addressees), "Color[" + chatMsg['color'] + "] Msg: '" + chatMsg['text'] + "'"])
                        except Exception as ex:
                            sender = "UNKNOWN"
                            if 'sender' in data:
                                sender = self.getPlayerCallsignFromNameOrId(data['sender'], data['sender'], trial_info['info']['client_info'])
                            chatMsg = data['text'].strip()
                            timings.append(['asr', int(ts), -1, 'at', sender + " - Chat to " + str(addressees), "Msg: '" + chatMsg + "'"])

                    # elif topic == 'observations/events/player/jag':
                    #     callsign = self.getPlayerCallsign(data, trial_info['info']['client_info'])
                    #     if callsign is not None and callsign in players.keys():
                    #         player = players[callsign]
                    #         ts = last_elapsed_time
                    #         sub_type = msg['msg']['sub_type']
                    #         if 'jag' in data:
                    #             if 'elapsed_milliseconds' in data['jag']:
                    #                 ts = data['jag']['elapsed_milliseconds']
                    #             else:
                    #                 data['jag']['elapsed_milliseconds'] = ts
                    #             data['jag']['sub_type'] = sub_type
                    #             print("Calling updatePlayerJag from the message processing loop for : " + data['jag']['id'] + " subtype: " + sub_type)
                    #             ASISTDataTools.updatePlayerJag(player, data['jag'])

                if generateSVG:
                    # generate SVG for the rubble
                    rubbleSVG, rubbleTimings = generateRubbleSVG(rubble, xtrans, ztrans)
                    timings.extend(rubbleTimings)
                    # generate SVG for the threats
                    threatSVG, threatTimings = generateThreatSVG(threats, xtrans, ztrans)
                    timings.extend(threatTimings)
                    # generate SVG for the markers
                    markerSVG, markerTimings = generateMarkerBlockSVG(markers, xtrans, ztrans)
                    timings.extend(markerTimings)
                    # generate SVG for the victims
                    victimsSVG, victimTimings = generateVictimSVG(victims, xtrans, ztrans)
                    timings.extend(victimTimings)
                    #generate SVG for the items
                    itemSVG, itemTimings = generateItemSVG(items, xtrans, ztrans)
                    timings.extend(itemTimings)
                    #generate SVG for the bombs
                    bombSVG, bombTimings = generateBombSVG(bombs, xtrans, ztrans)
                    timings.extend(bombTimings)
                    # generate SVG for room statuses (floor colors)
                    roomStatusSVG, roomStatusTimings = generateRoomStatusSVG(rsh.status_records, xtrans, ztrans)
                    timings.extend(roomStatusTimings)
                    # generate SVG for the players (path, location, role)
                    playerSVG = []
                    pathSVG = []

                    # print("{")
                    for player in players.values():
                        if player['callsign'] == 'server':
                            continue
                        # print('"' + player['callsign'] + '":')
                        # print(json.dumps(player['unprocessed_jags']))
                        # print(',')
                        pthSVG, plrSVG, plrTimings = generatePlayerSVG(player, xtrans, ztrans, trial_info['expVersion'])
                        timings.extend(plrTimings)
                        playerSVG.append(plrSVG)
                        pathSVG.append(pthSVG)
                    # print("}")

                    # print(sub_types)
                    # print(urns)

                    timings = sorted(timings, key=lambda d: d[1])
                    for timing in timings:
                        if timing[0] == 'mission_score':
                            timing[4] = str(timing[4]) + ' of ' + str(teamScore)

                # for callsign in ['red', 'green', 'blue']:  # ['team']:  # 
                #     print("=====================================")
                #     jah.player_summary (callsign)
                #     print("=====================================")

                # generate the base CSS, Groups, and timeline items
                newTimelineData = generateTimelineData(players, timeline, basemap, rubble, victims, trial_info['expVersion'], twentyMinutesAsMS, jah, jagData, planning_stop_time, markers, bombs)
                if 'Annotations' in timelineData.keys():
                    newTimelineData['Annotations'] = timelineData['Annotations']
                timelineData = newTimelineData

            if generateSVG:
                basemapSVG = ''
                if basemap is not None:
                    basemapSVG = BaseMap2SVG(basemap, xtrans, ztrans, trial_info['expVersion'])

                svg_map += semanticMapSVG + "\n"
                svg_map += roomStatusSVG + "\n"
                svg_map += basemapSVG + "\n"
                svg_map += rubbleSVG + "\n"
                svg_map += markerSVG + "\n"
                svg_map += threatSVG + "\n"
                svg_map += victimsSVG + "\n"
                svg_map += itemSVG + "\n"
                svg_map += bombSVG + "\n"
                for path in pathSVG:
                    svg_map += path + "\n"
                for player in playerSVG:
                    svg_map += player + "\n"
                        
                scale = 5

            # write out the generated info so it does not have to be generated again.
            # print("Saving MapTools data...")
            # trial_info['mapSearchTools'].initialize(basemap, rubble, victims)
            # trial_info['mapSearchTools'].save(trial_info['metadata-filename'] + '.maptools')

            if generateSVG:
                svg = '<svg height="' + str(height*scale) + '" width="' + str(width*scale) + '" xmlns="http://www.w3.org/2000/svg" ' + '>\n'+ \
                        '<g transform="scale(' + str(scale) + ')">\n' + \
                            '<rect id="x_' + str(xtrans) + '_z_' + str(ztrans) + '" x="0" y="0" width="' + str(width) + '" height="' + str(height) + '" style="fill:rgb(255,255,255)"></rect>\n' + \
                            svg_map + \
                    '</g></svg>'
                f = open(trial_info['metadata-filename'] + ".svg", "w")
                f.write(svg)
                f.close()

                f = open(trial_info['metadata-filename'] + ".timings", "w")
                f.write(json.dumps(timings))
                f.close()

            if len(timelineData) > 0:
                f = open(trial_info['metadata-filename'] + ".timeline", "w")
                f.write(json.dumps(timelineData))
                f.close()

        print("Finished Processing Trail Data. trial_info['expVersion']: " + str(trial_info['expVersion']) + " lastMsTime = " + str(lastMsTimeOfTrial))

        return width, height, xtrans, ztrans, svg_map, timings, timelineData, trial_info['expVersion'], lastMsTimeOfTrial

    # @staticmethod
    # def getChildJag(parent, id):
    #     if parent['id'] == id:
    #         return parent
    #     if 'children' in parent and len(parent['children']) > 0:
    #         for ch in parent['children']:
    #             child = ASISTDataTools.getChildJag(ch, id)
    #             if child is not None:
    #                 return child
    #     return None

    # @staticmethod
    # def updatePlayerJag(player, jag, alreadyProcessed = False):
    #     print("- Updating " + player['callsign'] + "'s jags with jagID: " + jag['id'])
    #     if 'unprocessed_jags' not in player:
    #         player['unprocessed_jags'] = []

    #     if jag['sub_type'] == 'Event:Discovered':
    #         print(" - Adding Discovery Event: " + jag['id'])
    #         player['jag'][jag['id']] = jag
    #         toRemove = []
    #         print("   - processing unprocessed jags...")
    #         for uj in player['unprocessed_jags']:
    #             if ASISTDataTools.updatePlayerJag(player, uj, True):
    #                 toRemove.append(uj)
    #         print("   - Removing processed jags from the unprocessed list...")
    #         for uj in toRemove:
    #             player['unprocessed_jags'].remove(uj)
    #         return True

    #     # find a discovery event which contains this jag's id:
    #     print(" - Not a Discovery event so looking for it's parent...")
    #     for deid in player['jag']:
    #         de = player['jag'][deid]
    #         child = ASISTDataTools.getChildJag(de, jag['id'])
    #         if child is not None:
    #             if 'jags' not in child:
    #                 child['jags'] = []
    #             child['jags'].append(jag)
    #             return True

    #     if not alreadyProcessed:
    #         # No corresponding Discovery event yet, so store this for later:
    #         print(" - No parent Discovery so adding to the unprocessed jags list. ")
    #         player['unprocessed_jags'].append(jag)

    #     return False

    #     # if sub_type not in player['jag']:
    #     #     player['jag'][sub_type] = {}
    #     # if data['jag']['id'] in player['jag'][sub_type]:
    #     #     print("** Id [" + data['jag']['id'] + "] already in subtype [" + sub_type + "]")
    #     # player['jag'][sub_type][data['jag']['id']] = data['jag']
        
    #     # if 'urn' in data['jag'] and data['jag']['urn'] not in urns:
    #     #     urns.append(data['jag']['urn'])
    #     # if sub_type not in sub_types:
    #     #     sub_types.append(sub_type)

    #     # ['Event:Discovered', 
    #     #       'Event:Awareness', 
    #     #       'Event:Completion', 
    #     #       'Event:Addressing', 
    #     #       'Event:Preparing']
    #     # ['urn:ihmc:asist:search-area', 
    #     # 'urn:ihmc:asist:get-in-range', 
    #     # 'urn:ihmc:asist:unlock-victim', 
    #     # 'urn:ihmc:asist:rescue-victim', 
    #     # 'urn:ihmc:asist:check-if-unlocked', 
    #     # 'urn:ihmc:asist:at-proper-triage-area', 
    #     # 'urn:ihmc:asist:stabilize', 
    #     # 'urn:ihmc:asist:diagnose', 
    #     # 'urn:ihmc:asist:pick-up-victim', 
    #     # 'urn:ihmc:asist:drop-off-victim', 
    #     # 'urn:ihmc:asist:clear-path']

    # def getShortestPath(self, trial_info, pt1, pt2, time):
    #     return trial_info['mapSearchTools'].shortestPath(pt1, pt2, time)

    # dtools = ASISTDataTools(maps_folder='maps', data_folder='data')
    # trials = dtools.getListOfTrials()

    # print(trials[-1]['name'])
    # s_svg_map, s_timings = dtools.loadTrial(trials[-1])

import argparse
from glob import glob
import os
from app.ASISTDataTools import ASISTDataTools

ap = argparse.ArgumentParser()
ap.add_argument("-f", "--file", help="path to metadata files to process", required=True, nargs='*')

# Parse the passed in arguments
args = ap.parse_args()

filenames = []
for fn in args.file:
    file = glob(fn)
    for f in file:
        if os.path.isdir(f):
            files = os.listdir(f)
            for fn2 in files:
                if not os.path.isdir(fn2) and fn2.lower().endswith(".metadata"):
                    filenames.append(os.path.join(f,fn2))
        elif f.lower().endswith(".metadata"):
            filenames.append(f)
filenames = sorted(filenames)

stats = []
for file in filenames:
    local_stats = {
        'team': 0,
        'trial': 0,
        'score': 0,
        'CriticalVictimTriaged': 0,
        'CriticalVictimEvacuated': 0,
        'CriticalVictimEvacuated_R': 0,
        'CriticalVictimEvacuated_G': 0,
        'CriticalVictimEvacuated_B': 0,
        'RegularVictimTriaged': 0,
        'RegularVictimEvacuated': 0,
        'RegularVictimEvacuated_R': 0,
        'RegularVictimEvacuated_G': 0,
        'RegularVictimEvacuated_B': 0,
        'RubbleDestroyed': 0,
        'ThreatTriggered': 0,
        'ThreatTriggered_R': 0,
        'ThreatTriggered_G': 0,
        'ThreatTriggered_B': 0,
        'TransportedVictims_R': 0,
        'TimeTransporting_R': 0,
        'TransportedVictims_G': 0,
        'TimeTransporting_G': 0,
        'TransportedVictims_B': 0,
        'TimeTransporting_B': 0,
        'MarkerBlocksPlaced': 0,
        'MarkerBlocksPlaced_R': 0,
        'MarkerBlocksPlaced_G': 0,
        'MarkerBlocksPlaced_B': 0
    }

    client_info = {}
    carryTime = {}
    # print(" - " + file)
    sorted_data = ASISTDataTools.loadAndSortMetadataFile(file)
    mission_started = False
    last_mission_time = -9999
    for msg in sorted_data:
        if msg['topic'] == 'trial':
            for client in msg['data']['client_info']:
                client_info[client['participant_id']] = client['callsign'].upper()[0]
            tmp = msg['data']['experiment_name']
            if tmp.lower().startswith('tm'):
                tmp = int(tmp.lower().split('tm')[-1])
            local_stats['team'] = tmp
            tmp = msg['data']['trial_number']
            if tmp.lower().startswith('t'):
                tmp = int(tmp.lower().split('t')[-1])
            local_stats['trial'] = tmp

        elif msg['topic'] == 'observations/events/mission':
            mission_started = True if msg['data']['mission_state'].lower() == 'start' else False

        elif msg['topic'] == 'observations/events/scoreboard':
            local_stats['score'] = msg['data']['scoreboard']['TeamScore']

        elif msg['topic'] == 'observations/state':
            if isinstance(msg['data']['elapsed_milliseconds'], int) and msg['data']['elapsed_milliseconds'] > 0:
                last_mission_time = msg['data']['elapsed_milliseconds']

        elif mission_started:
            if msg['topic'] == 'observations/events/player/triage':
                if msg['data']['triage_state'].lower() == 'successful':
                    if msg['data']['type'].lower()[-1] == 'c':
                        local_stats['CriticalVictimTriaged'] += 1
                    else:
                        local_stats['RegularVictimTriaged'] += 1

            elif msg['topic'] == 'observations/events/player/victim_evacuated' or \
                msg['topic'] == 'observations/events/server/victim_evacuated':

                if 'success' in msg['data'] and msg['data']['success']:
                    if msg['data']['type'].lower()[-1] == 'c':
                        local_stats['CriticalVictimEvacuated'] += 1
                        local_stats['CriticalVictimEvacuated_' + client_info[msg['data']['participant_id']]] += 1
                    else:
                        local_stats['RegularVictimEvacuated'] += 1
                        local_stats['RegularVictimEvacuated_' + client_info[msg['data']['participant_id']]] += 1

            elif msg['topic'] == 'observations/events/player/victim_picked_up':
                carryTime[msg['data']['participant_id']] = msg['data']['elapsed_milliseconds']
                local_stats['TransportedVictims_' + client_info[msg['data']['participant_id']]] += 1

            elif msg['topic'] == 'observations/events/player/victim_placed':
                local_stats['TimeTransporting_' + client_info[msg['data']['participant_id']]] += msg['data']['elapsed_milliseconds'] - carryTime[msg['data']['participant_id']]
                carryTime[msg['data']['participant_id']] = -1

            elif msg['topic'] == 'observations/events/player/rubble_collapse' or \
                 msg['topic'] == 'observations/events/server/rubble_collapse':
                local_stats['ThreatTriggered'] += 1
                local_stats['ThreatTriggered_' + client_info[msg['data']['participant_id']]] += 1

            elif msg['topic'] == 'observations/events/player/rubble_destroyed':
                local_stats['RubbleDestroyed'] += 1

            elif msg['topic'] == 'observations/events/player/marker_placed':
                local_stats['MarkerBlocksPlaced'] += 1
                local_stats['MarkerBlocksPlaced_' + client_info[msg['data']['participant_id']]] += 1
    
    # deal with computing the time spent carrying any victim after mission end.
    for pid in carryTime:
        if carryTime[pid] >= 0:
            local_stats['TimeTransporting_' + client_info[pid]] += last_mission_time - carryTime[pid]

    stats.append(local_stats)

#==============================================================
# Print the results...
#==============================================================
header = ''
keys = []
for key in stats[0]:
    header += key + ','
    keys.append(key)

print()
print(header)
for ls in stats:
    lss = ''
    for key in keys:
        lss += str(ls[key]) + ","
    print(lss)

# IHMC Visualizer (beta v0.2)

This is a beta version of the IHMC ASIST data visualizer.  It was written in Python and Javascript and uses the FLASK Python framework to display the interface in a web browser.  It can be run localy in a command shell or locally/remotely via a docker container. 

## Requirements:
- To run locally:
    - Python 3.7.3+ installed
    - A Web browser
- To create a Docker Image
    - Docker and Docker-Compose Installed


## Local Installation
1. In a command shell, goto the main folder of the cloned git repository which contains the `requirements.txt` file.
2. (suggestion) Create a virtual Python Environment by running the following commands in your shell. (This may be different on your machine!  If it does not work, look at how to install a virtual python env based on your system.):
    - `python3 -m venv env`
    - `source env/bin/activate`
3. Install the required python libraries by running this command in your shell:
    - `pip install -r requirements.txt`

## Docker Image Creation
1. In a command shell, goto the main foler of the cloned git repository which containes the `docker-compose.yml` file.
2. Build the docker image by running: `docker-compose build`


## Running Locally in a command shell
- In a command shell, goto the main folder of the cloned git repository which contains the `runit.sh` script.  (Sorry Windows users, just look at the contents of the file and you should get the idea.)
- In the comman shell run the following shell script:
    - `./runit.sh`
- Open a webrowser and goto the url: `http://0.0.0.0:9090`

## Running via Docker
- If you want to run the Docker container locally, just run the follwing command in a command shell in the folder which contains the `docker-compose.yml` file:
    - `docker-compose up`
- Open a webrowser and goto the url: `http://0.0.0.0:9090`

## Important Folders
The local install and the Docker Container both use the `./app/static` folder.  This folder contains the html, javascript files and is also where the `metadata` and `video` files are uploaded.

## Client Usage
- Uploading new data and videos
    - You can upload `*.metadata` and `*.mp4` files by dragging and dropping them into the 'Drop files here to upload' area on the bottom left.  When you do so the files will be uploaded and stored in the `./app/static/videos` folder.
    - There is a filter feature now which allows you to filter the list of trials shown in the selection dropdown.
    - Trial data and Video data are associated based on the filename.  The code is smart enought to select the highest version number of a file and use it.

- Once you have trials uploaded you can select a trial from the dropdown list on the top right.  This will tell the server to send over the appropriate data for the selected trial.  Please be patient after you select a trial for the first time.  It can take upto 20 seconds to process the data for the timeline.  The next time you select a previously opened trial it should only take a second or two to open.  You will know the trial is loaded when the video/map are updated.

- Once a trial is loaded you can do the following:
    1. Play the video by using the controls in the video itself.
    3. Hide/show different items in the map with the switches.
    4. Zoom In/Out of the map with the Zoom buttons.
    5. See the current Mission Name (From the data not the filename), Mission Time (In seconds for hackathon data and Minutes/seconds from v 1.0 data) also `(paused)` will be added to the end of the time value if the mission is in a paused state and finally the current location base on info from the Semantic Map.
    6. Delete the currently selected trial/video (There is a confirmation prompt for this.)
    7. View an annotatable timeline of events.  This timeline is pre-populated with data, and you can add to it by double clicking on it.

## Notice
This is beta software and you can use it however you want, so do not blame me if anything goes wrong.  I am happy to receive any suggestions and code updates/requests.

## Contact Info
Roger Carff\
Senior Research Associate\
IHMC\
rcarff@ihmc.org

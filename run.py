from flask import Flask
from flask_socketio import SocketIO
from app import socketio
from app import app

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=9090)